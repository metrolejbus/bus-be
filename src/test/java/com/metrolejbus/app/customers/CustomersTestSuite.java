package com.metrolejbus.app.customers;

import com.metrolejbus.app.customers.services.CustomersServiceTestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
        CustomersServiceTestSuite.class,
})
public class CustomersTestSuite {
}

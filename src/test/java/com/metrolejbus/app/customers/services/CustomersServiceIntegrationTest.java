package com.metrolejbus.app.customers.services;

import com.metrolejbus.app.ClearDatabaseRule;
import com.metrolejbus.app.authentication.models.Role;
import com.metrolejbus.app.authentication.models.User;
import com.metrolejbus.app.authentication.models.UserWithToken;
import com.metrolejbus.app.authentication.repositories.UserRepository;
import com.metrolejbus.app.authentication.services.AuthService;
import com.metrolejbus.app.common.exceptions.BadRequest;
import com.metrolejbus.app.customers.models.Customer;
import com.metrolejbus.app.customers.models.CustomerStatus;
import com.metrolejbus.app.customers.repositories.CustomersRepository;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.repositories.ZoneRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class CustomersServiceIntegrationTest {

    @Rule
    @Autowired
    public ClearDatabaseRule clearDatabaseRule;

    @Autowired
    public CustomersRepository customersRepository;

    @Autowired
    public CustomersService customersService;

    @Autowired
    public AuthService authService;

    @Autowired
    public UserRepository userRepository;

    @Autowired
    public ZoneRepository zoneRepository;

    @Before
    public void setUp() throws Exception {
        UserWithToken u = authService.signUp(
                "username",
                "password",
                "test@test.test",
                "First",
                "Last",
                "Md",
                45,
                CustomerStatus.ADULT
        );
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testFindByIdNotFound() {
        customersService.findById(78);
    }

    @Test
    public void testFindByIdSuccess() {
        Customer customer = customersService.findById(1L);
        assertNotNull(customer);
        assertEquals(1L, (long) customer.getId());
    }

    @Test
    public void updateCustomerSuccess() throws BadRequest {
        String firstName = "Milorad";
        String lastName = "Miloradovic";
        Integer age = 7;
        Long id = 1L;

        Customer customer = customersService.updateCustomer(id, firstName, lastName, age);

        assertNotNull(customer);
        assertEquals(1L, (long) customer.getId());
        assertEquals(firstName, customer.getFirstName());
        assertEquals(lastName, customer.getLastName());
        assertEquals(age, new Integer(customer.getAge()));
    }

    @Test(expected = ResourceNotFoundException.class)
    public void updateCustomerNotFound() throws BadRequest {
        customersService.updateCustomer(78L, "Doesn't", "Matter", 78);
    }

    @Test(expected = BadRequest.class)
    public void updateCustomerNoName() throws BadRequest {
        customersService.updateCustomer(1L, null, "Last", 45);
    }

    @Test(expected = BadRequest.class)
    public void updateCustomerNoLast() throws BadRequest {
        customersService.updateCustomer(1L, "First", null, 67);
    }

    @Test(expected = BadRequest.class)
    public void updateCustomerNoAge() throws BadRequest {
        customersService.updateCustomer(1L, "First", "Last", null);
    }

    @Test(expected = BadRequest.class)
    public void createCustomerNoMiddleName() throws BadRequest {
        User u = userRepository.findByUsername("username");
        customersService.createCustomer("First", "Last", null, 56, u);
    }

    @Test(expected = BadRequest.class)
    public void createCustomerNoFirstName() throws BadRequest {
        User u = userRepository.findByUsername("username");
        customersService.createCustomer(null, "Last", "Mid", 56, u);
    }

    @Test(expected = BadRequest.class)
    public void createCustomerNoLastName() throws BadRequest {
        User u = userRepository.findByUsername("username");
        customersService.createCustomer("First", null, "Mid", 56, u);
    }

    @Test(expected = BadRequest.class)
    public void createCustomerNoAge() throws BadRequest {
        User u = userRepository.findByUsername("username");
        customersService.createCustomer("First", "Last", "Mid", null, u);
    }


    @Test
    public void createCustomerSuccess() throws BadRequest {
        User u = new User();
        u.setPassword("pass");
        u.setEmail("arst@neio.com");
        u.setUsername("newGuy");
        Customer c = customersService.createCustomer("First", "Last", "Mid", 43, u);

        assertNotNull(c);
        assertEquals(c.getAge(), 43);
        assertEquals(c.getFirstName(), "First");
        assertEquals(c.getLastName(), "Last");
        assertEquals(c.getMiddleName(), "Mid");
    }
}

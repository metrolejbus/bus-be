package com.metrolejbus.app.customers.services;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
        CustomersServiceTest.class,
        CustomersServiceIntegrationTest.class
})
public class CustomersServiceTestSuite {
}

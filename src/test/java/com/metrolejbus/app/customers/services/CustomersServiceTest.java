package com.metrolejbus.app.customers.services;

import com.metrolejbus.app.authentication.models.User;
import com.metrolejbus.app.common.exceptions.BadRequest;
import com.metrolejbus.app.customers.forms.UpdateCustomerForm;
import com.metrolejbus.app.customers.models.Customer;
import com.metrolejbus.app.customers.models.CustomerStatus;
import com.metrolejbus.app.customers.repositories.CustomersRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.management.InvalidAttributeValueException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class CustomersServiceTest {

    @MockBean
    public CustomersRepository customersRepository;

    @Autowired
    public CustomersService customersService;

    private Map<Long, Customer> mockedCustomers = new HashMap<>();


    @Before
    public void setUp() throws Exception {

        when(customersRepository.saveAndFlush(any(Customer.class))).thenAnswer(invocation -> {
            Customer l = invocation.getArgument(0);

            if (l.getId() == null) {
                long key = mockedCustomers.keySet().size();
                l.setId(++key);
                mockedCustomers.put(++key, l);
            } else {
                mockedCustomers.put(l.getId(), l);
            }

            return l;
        });

        when(customersRepository.findById(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            Customer l = mockedCustomers.getOrDefault(key, null);
            return Optional.ofNullable(l);
        });

        Customer customer = new Customer(new User(), "Saint", "Le", "Tropez", 1, CustomerStatus.ADULT);
        customer.setId(1L);
        mockedCustomers.put(customer.getId(), customer);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testFindByIdNotFound() {
        customersService.findById(78);
    }

    @Test
    public void testFindByIdSuccess() {
        Customer customer = customersService.findById(1);
        assertNotNull(customer);
        assertEquals(1L, (long) customer.getId());

        verify(customersRepository, times(1)).findById(anyLong());
        verifyNoMoreInteractions(customersRepository);
    }

    @Test
    public void updateCustomerSuccess() throws BadRequest {
        String firstName = "Milorad";
        String lastName = "Miloradovic";
        Integer age = 7;

        Customer customer = customersService.updateCustomer(1L, "Milorad", "Miloradovic", 7);

        assertNotNull(customer);
        assertEquals(1L, (long) customer.getId());
        assertEquals(firstName, customer.getFirstName());
        assertEquals(lastName, customer.getLastName());
        assertEquals(age, new Integer(customer.getAge()));

        verify(customersRepository, times(1)).findById(anyLong());
        verify(customersRepository, times(1)).saveAndFlush(any(Customer.class));
    }

    @Test(expected = ResourceNotFoundException.class)
    public void updateCustomerNotFound() throws InvalidAttributeValueException, BadRequest {
        customersService.updateCustomer(78L, "First", "Last", 43);
    }

    @Test(expected = BadRequest.class)
    public void updateCustomer() throws InvalidAttributeValueException, BadRequest {
        customersService.updateCustomer(1L, null, null, null);
    }

}

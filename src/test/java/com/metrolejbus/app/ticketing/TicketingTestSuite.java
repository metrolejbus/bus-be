package com.metrolejbus.app.ticketing;

import com.metrolejbus.app.ticketing.controllers.TicketingControllersTestSuite;
import com.metrolejbus.app.ticketing.services.TicketingServicesTestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
    TicketingControllersTestSuite.class,
    TicketingServicesTestSuite.class
})
public class TicketingTestSuite {}

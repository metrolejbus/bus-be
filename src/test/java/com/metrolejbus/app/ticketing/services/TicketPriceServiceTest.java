package com.metrolejbus.app.ticketing.services;


import com.metrolejbus.app.common.exceptions.NotFound;
import com.metrolejbus.app.common.models.ValidityPeriod;
import com.metrolejbus.app.customers.models.CustomerStatus;
import com.metrolejbus.app.ticketing.forms.CreateTicketPriceForm;
import com.metrolejbus.app.ticketing.forms.UpdateTicketPriceForm;
import com.metrolejbus.app.ticketing.models.TicketDuration;
import com.metrolejbus.app.ticketing.models.TicketPrice;
import com.metrolejbus.app.ticketing.repositories.TicketPriceRepository;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.services.ZoneService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.management.InvalidAttributeValueException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class TicketPriceServiceTest {

    @Autowired
    private TicketPriceService ticketPriceService;

    @MockBean
    private TicketPriceRepository ticketPriceRepositoryMocked;

    @MockBean
    private ZoneService zoneServiceMocked;

    @Test
    public void testFindByIdSuccess() throws ZoneNotFound {
        long zoneId = 1L;
        String duration = "month";
        String customer = "student";

        Zone zone = new Zone("zone 1");
        zone.setId(1L);
        Mockito.when(zoneServiceMocked.get(zoneId)).thenReturn(zone);

        TicketPrice ticketPrice = new TicketPrice(
                CustomerStatus.valueOf(customer.toUpperCase()),
                TicketDuration.valueOf(duration.toUpperCase()),
                zone
        );
        ticketPrice.setActive(true);
        Mockito.when(ticketPriceRepositoryMocked.findTicketPriceByIdEquals(ticketPrice.getId())).thenReturn(ticketPrice);

        TicketPrice foundTicketPrice = ticketPriceService.findById(zoneId, duration, customer);
        assertNotNull(foundTicketPrice);
        assertEquals(zoneId, foundTicketPrice.getZone().getId(), 0.0);
        assertEquals(duration.toUpperCase(), foundTicketPrice.getTicketDuration().name());
        assertEquals(customer.toUpperCase(), foundTicketPrice.getCustomerStatus().name());
        verify(zoneServiceMocked, atMost(1)).get(zoneId);
        verify(ticketPriceRepositoryMocked, atMost(1)).findTicketPriceByIdEquals(ticketPrice.getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindByIdIllegalCustomerEx() throws ZoneNotFound {
        long zoneId = 1L;
        String duration = "month";
        String customer = "asd";

        ticketPriceService.findById(zoneId, duration, customer);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindByIdIllegalDurationEx() throws ZoneNotFound {
        long zoneId = 1L;
        String duration = "asd";
        String customer = "student";

        ticketPriceService.findById(zoneId, duration, customer);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testFindByIdResourceZoneNotFoundEx()
        throws ZoneNotFound
    {
        long zoneId = 1L;
        String duration = "month";
        String customer = "student";

        Mockito.when(zoneServiceMocked.get(zoneId)).thenThrow(ResourceNotFoundException.class);

        ticketPriceService.findById(zoneId, duration, customer);
        verify(zoneServiceMocked, atMost(1)).get(zoneId);
    }

//    @Test(expected = TicketPriceNotFound.class)
//    public void testFindByIdResourceTicketPriceNotFoundEx()
//        throws ZoneNotFound
//    {
//        long zoneId = 1L;
//        String duration = "month";
//        String customer = "student";
//
//        Zone zone = new Zone("zone 1");
//        zone.setId(1L);
//        Mockito.when(zoneServiceMocked.get(zoneId)).thenReturn(zone);
//
//        TicketPrice.TicketPriceKey id = new TicketPrice.TicketPriceKey(
//                CustomerStatus.valueOf(customer.toUpperCase()),
//                TicketDuration.valueOf(duration.toUpperCase()),
//                zone
//        );
//        Mockito.when(ticketPriceRepositoryMocked.findTicketPriceByIdEquals(id)).thenReturn(null);
//
//        ticketPriceService.findById(zoneId, duration, customer);
//        verify(zoneServiceMocked, atMost(1)).get(zoneId);
//        verify(ticketPriceRepositoryMocked, atMost(1)).findTicketPriceByIdEquals(id);
//    }

    @Test
    public void testCreateSuccess()
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.customerStatus = "student";
        data.startDate = "25.12.2019.";
        data.endDate = "25.12.2020.";
        data.ticketDuration = "month";
        data.value = (float)10;
        data.zoneId = 1L;

        Zone zone = new Zone("zone 1");
        zone.setId(1L);
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(zone);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
        TicketPrice ticketPrice = new TicketPrice(
                CustomerStatus.valueOf(data.customerStatus.toUpperCase()),
                TicketDuration.valueOf(data.ticketDuration.toUpperCase()),
                zone,
                data.value,
                new ValidityPeriod(LocalDate.parse(data.startDate, formatter), LocalDate.parse(data.endDate, formatter))
        );
        ticketPrice.setActive(true);
        Mockito.when(ticketPriceRepositoryMocked.save(ticketPrice)).thenReturn(ticketPrice);

        TicketPrice createdTicketPrice = ticketPriceService.create(data);
        assertNotNull(createdTicketPrice);
        assertEquals(data.zoneId, createdTicketPrice.getZone().getId());
        assertEquals(data.customerStatus.toUpperCase(), createdTicketPrice.getCustomerStatus().name());
        assertEquals(data.startDate, createdTicketPrice.getValidityPeriod().getStartDate().format(formatter));
        assertEquals(data.endDate, createdTicketPrice.getValidityPeriod().getEndDate().format(formatter));
        assertEquals(data.ticketDuration.toUpperCase(), createdTicketPrice.getTicketDuration().name());
        verify(ticketPriceRepositoryMocked, atMost(1)).save(ticketPrice);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testCreateResourceZoneNotFoundEx()
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.customerStatus = "student";
        data.startDate = "25.12.2019.";
        data.endDate = "25.12.2020.";
        data.ticketDuration = "month";
        data.value = (float)10;
        data.zoneId = 1L;

        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenThrow(ResourceNotFoundException.class);

        ticketPriceService.create(data);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test(expected = DateTimeParseException.class)
    public void testCreateDateTimeParseEx()
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.customerStatus = "student";
        data.startDate = "25.12.2019";
        data.endDate = "25.12.2020.";
        data.ticketDuration = "month";
        data.value = (float)10;
        data.zoneId = 1L;

        Zone zone = new Zone("zone 1");
        zone.setId(1L);
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(zone);

        ticketPriceService.create(data);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testCreateDateInPastEx()
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.customerStatus = "student";
        data.startDate = "25.12.2017.";
        data.endDate = "25.12.2020.";
        data.ticketDuration = "month";
        data.value = (float)10;
        data.zoneId = 1L;

        Zone zone = new Zone("zone 1");
        zone.setId(1L);
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(zone);

        ticketPriceService.create(data);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testCreateEndDateBeforeStartDateEx()
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.customerStatus = "student";
        data.startDate = "25.12.2020.";
        data.endDate = "25.12.2019.";
        data.ticketDuration = "month";
        data.value = (float)10;
        data.zoneId = 1L;

        Zone zone = new Zone("zone 1");
        zone.setId(1L);
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(zone);

        ticketPriceService.create(data);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testCreateInvalidValueEx()
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.customerStatus = "student";
        data.startDate = "25.12.2019.";
        data.endDate = "25.12.2020.";
        data.ticketDuration = "month";
        data.value = (float)-10;
        data.zoneId = 1L;

        Zone zone = new Zone("zone 1");
        zone.setId(1L);
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(zone);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
        new TicketPrice(
            CustomerStatus.valueOf(data.customerStatus.toUpperCase()),
            TicketDuration.valueOf(data.ticketDuration.toUpperCase()),
            zone,
            data.value,
            new ValidityPeriod(LocalDate.parse(data.startDate, formatter), LocalDate.parse(data.endDate, formatter))
        );

        ticketPriceService.create(data);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateIllegalCustomerStatusEx()
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.customerStatus = "asd";
        data.startDate = "25.12.2019.";
        data.endDate = "25.12.2020.";
        data.ticketDuration = "month";
        data.value = (float)10;
        data.zoneId = 1L;

        Zone zone = new Zone("zone 1");
        zone.setId(1L);
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(zone);

        ticketPriceService.create(data);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateIllegalDurationEx()
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.customerStatus = "student";
        data.startDate = "25.12.2019.";
        data.endDate = "25.12.2020.";
        data.ticketDuration = "asd";
        data.value = (float)10;
        data.zoneId = 1L;

        Zone zone = new Zone("zone 1");
        zone.setId(1L);
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(zone);

        ticketPriceService.create(data);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test
    public void testUpdateValueOnlySuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "month";
        data.zoneId = 1L;
        data.value = (float)10;

        Zone zone = new Zone("zone 1");
        zone.setId(1L);
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(zone);

        float oldValue = 5;

        TicketPrice ticketPrice = new TicketPrice(
                CustomerStatus.valueOf(data.customerStatus.toUpperCase()),
                TicketDuration.valueOf(data.ticketDuration.toUpperCase()),
                zone
        );
        ticketPrice.setValue(oldValue);
        ticketPrice.setActive(true);
        ticketPrice.setValidityPeriod(new ValidityPeriod(LocalDate.of(2019, 1, 31), LocalDate.of(2020, 1, 31)));
        Mockito.when(ticketPriceRepositoryMocked.findTicketPriceByIdEquals(ticketPrice.getId())).thenReturn(ticketPrice);

        TicketPrice updatedTicketPrice= new TicketPrice(
                ticketPrice.getCustomerStatus(),
                ticketPrice.getTicketDuration(),
                ticketPrice.getZone(),
                data.value,
                ticketPrice.getValidityPeriod()
        );
        ticketPrice.setActive(true);

        Mockito.when(ticketPriceRepositoryMocked.save(updatedTicketPrice)).thenReturn(updatedTicketPrice);

        TicketPrice ticketPriceFinal = ticketPriceService.update(data);
        assertNotNull(ticketPriceFinal);
        assertEquals(data.value, ticketPriceFinal.getValue(), 0.0);
        assertEquals(CustomerStatus.valueOf(data.customerStatus.toUpperCase()), ticketPriceFinal.getCustomerStatus());
        assertEquals(TicketDuration.valueOf(data.ticketDuration.toUpperCase()), ticketPriceFinal.getTicketDuration());
        assertEquals(zone, ticketPriceFinal.getZone());
        verify(ticketPriceRepositoryMocked, atMost(1)).findTicketPriceByIdEquals(ticketPrice.getId());
        verify(ticketPriceRepositoryMocked, atMost(1)).save(updatedTicketPrice);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test
    public void testUpdateStartDateOnlySuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "month";
        data.zoneId = 1L;
        data.startDate = "25.11.2019.";

        Zone zone = new Zone("zone 1");
        zone.setId(1L);
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(zone);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
        TicketPrice ticketPrice = new TicketPrice(
                CustomerStatus.valueOf(data.customerStatus.toUpperCase()),
                TicketDuration.valueOf(data.ticketDuration.toUpperCase()),
                zone,
                5,
                new ValidityPeriod(LocalDate.parse("28.11.2019.", formatter), LocalDate.parse("28.11.2020.", formatter))
        );
        ticketPrice.setActive(true);
        Mockito.when(ticketPriceRepositoryMocked.findTicketPriceByIdEquals(ticketPrice.getId())).thenReturn(ticketPrice);

        TicketPrice updatedTicketPrice= new TicketPrice(
                ticketPrice.getCustomerStatus(),
                ticketPrice.getTicketDuration(),
                ticketPrice.getZone(),
                ticketPrice.getValue(),
                ticketPrice.getValidityPeriod()
        );
        ticketPrice.getValidityPeriod().setStartDate( LocalDate.parse(data.startDate, formatter));
        ticketPrice.setActive(true);

        Mockito.when(ticketPriceRepositoryMocked.save(updatedTicketPrice)).thenReturn(updatedTicketPrice);

        TicketPrice ticketPriceFinal = ticketPriceService.update(data);
        assertNotNull(ticketPriceFinal);
        assertEquals(LocalDate.parse(data.startDate, formatter), ticketPriceFinal.getValidityPeriod().getStartDate());
        assertEquals(CustomerStatus.valueOf(data.customerStatus.toUpperCase()), ticketPriceFinal.getCustomerStatus());
        assertEquals(TicketDuration.valueOf(data.ticketDuration.toUpperCase()), ticketPriceFinal.getTicketDuration());
        assertEquals(zone, ticketPriceFinal.getZone());
        verify(ticketPriceRepositoryMocked, atMost(1)).findTicketPriceByIdEquals(ticketPrice.getId());
        verify(ticketPriceRepositoryMocked, atMost(1)).save(updatedTicketPrice);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test
    public void testUpdateEndDateOnlySuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "month";
        data.zoneId = 1L;
        data.endDate = "25.11.2020.";

        Zone zone = new Zone("zone 1");
        zone.setId(1L);
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(zone);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
        TicketPrice ticketPrice = new TicketPrice(
                CustomerStatus.valueOf(data.customerStatus.toUpperCase()),
                TicketDuration.valueOf(data.ticketDuration.toUpperCase()),
                zone,
                5,
                new ValidityPeriod(LocalDate.parse("28.11.2019.", formatter), LocalDate.parse("28.11.2020.", formatter))
        );
        ticketPrice.setActive(true);
        Mockito.when(ticketPriceRepositoryMocked.findTicketPriceByIdEquals(ticketPrice.getId())).thenReturn(ticketPrice);

        TicketPrice updatedTicketPrice= new TicketPrice(
                ticketPrice.getCustomerStatus(),
                ticketPrice.getTicketDuration(),
                ticketPrice.getZone(),
                ticketPrice.getValue(),
                ticketPrice.getValidityPeriod()
        );
        ticketPrice.getValidityPeriod().setEndDate( LocalDate.parse(data.endDate, formatter));
        ticketPrice.setActive(true);

        Mockito.when(ticketPriceRepositoryMocked.save(updatedTicketPrice)).thenReturn(updatedTicketPrice);

        TicketPrice ticketPriceFinal = ticketPriceService.update(data);
        assertNotNull(ticketPriceFinal);
        assertEquals(LocalDate.parse(data.endDate, formatter), ticketPriceFinal.getValidityPeriod().getEndDate());
        assertEquals(CustomerStatus.valueOf(data.customerStatus.toUpperCase()), ticketPriceFinal.getCustomerStatus());
        assertEquals(TicketDuration.valueOf(data.ticketDuration.toUpperCase()), ticketPriceFinal.getTicketDuration());
        assertEquals(zone, ticketPriceFinal.getZone());
        verify(ticketPriceRepositoryMocked, atMost(1)).findTicketPriceByIdEquals(ticketPrice.getId());
        verify(ticketPriceRepositoryMocked, atMost(1)).save(updatedTicketPrice);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test
    public void testUpdateNoChange()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "month";
        data.zoneId = 1L;

        Zone zone = new Zone("zone 1");
        zone.setId(1L);
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(zone);

        TicketPrice ticketPrice = new TicketPrice(
                CustomerStatus.valueOf(data.customerStatus.toUpperCase()),
                TicketDuration.valueOf(data.ticketDuration.toUpperCase()),
                zone
        );
        ticketPrice.setActive(true);
        Mockito.when(ticketPriceRepositoryMocked.findTicketPriceByIdEquals(ticketPrice.getId())).thenReturn(ticketPrice);

        TicketPrice updatedTicketPrice= new TicketPrice(
                ticketPrice.getCustomerStatus(),
                ticketPrice.getTicketDuration(),
                ticketPrice.getZone()
        );
        ticketPrice.setActive(true);

        Mockito.when(ticketPriceRepositoryMocked.save(updatedTicketPrice)).thenReturn(updatedTicketPrice);

        TicketPrice ticketPriceFinal = ticketPriceService.update(data);
        assertNotNull(ticketPriceFinal);
        assertEquals(ticketPrice.getCustomerStatus(), ticketPriceFinal.getCustomerStatus());
        assertEquals(ticketPrice.getTicketDuration(), ticketPriceFinal.getTicketDuration());
        assertEquals(ticketPrice.getZone().getId(), ticketPriceFinal.getZone().getId());
        verify(ticketPriceRepositoryMocked, atMost(1)).findTicketPriceByIdEquals(ticketPrice.getId());
        verify(ticketPriceRepositoryMocked, atMost(1)).save(updatedTicketPrice);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateIllegalCustomerStatusEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "ashs";
        data.ticketDuration = "month";
        data.zoneId = 1L;

        ticketPriceService.update(data);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateIllegalDurationEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "asd";
        data.zoneId = 1L;

        ticketPriceService.update(data);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testUpdateResourceZoneNotFoundEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "year";
        data.zoneId = 1L;

        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenThrow(ResourceNotFoundException.class);
        ticketPriceService.update(data);
    }

//    @Test(expected = TicketPriceNotFound.class)
//    public void testUpdateResourceTicketPriceNotFoundEx()
//        throws InvalidAttributeValueException, ZoneNotFound
//    {
//        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
//        data.customerStatus = "student";
//        data.ticketDuration = "month";
//        data.zoneId = 1L;
//
//        Zone zone = new Zone("zone 1");
//        zone.setId(1L);
//        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(zone);
//
//        TicketPrice.TicketPriceKey id = new TicketPrice.TicketPriceKey(
//                CustomerStatus.valueOf(data.customerStatus.toUpperCase()),
//                TicketDuration.valueOf(data.ticketDuration.toUpperCase()),
//                zone
//        );
//        Mockito.when(ticketPriceRepositoryMocked.findTicketPriceByIdEquals(id)).thenReturn(null);
//
//        ticketPriceService.update(data);
//    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testUpdateInvalidValueEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "month";
        data.zoneId = 1L;
        data.value = (float)-10;

        Zone zone = new Zone("zone 1");
        zone.setId(1L);
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(zone);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
        TicketPrice ticketPrice = new TicketPrice(
                CustomerStatus.valueOf(data.customerStatus.toUpperCase()),
                TicketDuration.valueOf(data.ticketDuration.toUpperCase()),
                zone,
                10,
                new ValidityPeriod(LocalDate.parse("28.11.2019.", formatter), LocalDate.parse("28.11.2020.", formatter))
        );
        ticketPrice.setActive(true);
        Mockito.when(ticketPriceRepositoryMocked.findTicketPriceByIdEquals(ticketPrice.getId())).thenReturn(ticketPrice);

        ticketPriceService.update(data);
    }

    @Test(expected = DateTimeParseException.class)
    public void testUpdateDateTimeParseEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "month";
        data.zoneId = 1L;
        data.startDate = "25.11.2019";

        Zone zone = new Zone("zone 1");
        zone.setId(1L);
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(zone);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
        TicketPrice ticketPrice = new TicketPrice(
                CustomerStatus.valueOf(data.customerStatus.toUpperCase()),
                TicketDuration.valueOf(data.ticketDuration.toUpperCase()),
                zone,
                10,
                new ValidityPeriod(LocalDate.parse("28.11.2019.", formatter), LocalDate.parse("28.11.2020.", formatter))
        );
        ticketPrice.setActive(true);
        Mockito.when(ticketPriceRepositoryMocked.findTicketPriceByIdEquals(ticketPrice.getId())).thenReturn(ticketPrice);

        ticketPriceService.update(data);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testUpdateStartDateInPastEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "month";
        data.zoneId = 1L;
        data.startDate = "25.11.2017.";

        Zone zone = new Zone("zone 1");
        zone.setId(1L);
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(zone);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
        TicketPrice ticketPrice = new TicketPrice(
                CustomerStatus.valueOf(data.customerStatus.toUpperCase()),
                TicketDuration.valueOf(data.ticketDuration.toUpperCase()),
                zone,
                10,
                new ValidityPeriod(LocalDate.parse("28.11.2019.", formatter), LocalDate.parse("28.11.2020.", formatter))
        );
        ticketPrice.setActive(true);
        Mockito.when(ticketPriceRepositoryMocked.findTicketPriceByIdEquals(ticketPrice.getId())).thenReturn(ticketPrice);

        ticketPriceService.update(data);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testUpdateStartDateAfterEndDateEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "month";
        data.zoneId = 1L;
        data.startDate = "25.12.2020.";

        Zone zone = new Zone("zone 1");
        zone.setId(1L);
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(zone);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
        TicketPrice ticketPrice = new TicketPrice(
                CustomerStatus.valueOf(data.customerStatus.toUpperCase()),
                TicketDuration.valueOf(data.ticketDuration.toUpperCase()),
                zone,
                10,
                new ValidityPeriod(LocalDate.parse("28.11.2019.", formatter), LocalDate.parse("28.11.2020.", formatter))
        );
        ticketPrice.setActive(true);
        Mockito.when(ticketPriceRepositoryMocked.findTicketPriceByIdEquals(ticketPrice.getId())).thenReturn(ticketPrice);

        ticketPriceService.update(data);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testUpdateEndDateBeforeStartDateEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "month";
        data.zoneId = 1L;
        data.endDate = "27.11.2019.";

        Zone zone = new Zone("zone 1");
        zone.setId(1L);
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(zone);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
        TicketPrice ticketPrice = new TicketPrice(
                CustomerStatus.valueOf(data.customerStatus.toUpperCase()),
                TicketDuration.valueOf(data.ticketDuration.toUpperCase()),
                zone,
                10,
                new ValidityPeriod(LocalDate.parse("28.11.2019.", formatter), LocalDate.parse("28.11.2020.", formatter))
        );
        ticketPrice.setActive(true);
        Mockito.when(ticketPriceRepositoryMocked.findTicketPriceByIdEquals(ticketPrice.getId())).thenReturn(ticketPrice);

        ticketPriceService.update(data);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testUpdateZoneIdNull()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "month";

        ticketPriceService.update(data);
    }

    @Test
    public void testDeleteSuccess() throws ZoneNotFound {
        long zoneId = 1L;
        String duration = "month";
        String customer = "student";

        Zone zone = new Zone("zone 1");
        zone.setId(1L);
        Mockito.when(zoneServiceMocked.get(zoneId)).thenReturn(zone);

        TicketPrice ticketPrice = new TicketPrice(
                CustomerStatus.valueOf(customer.toUpperCase()),
                TicketDuration.valueOf(duration.toUpperCase()),
                zone
        );
        ticketPrice.setActive(true);
        Mockito.when(ticketPriceRepositoryMocked.findTicketPriceByIdEquals(ticketPrice.getId())).thenReturn(ticketPrice);

        TicketPrice deletedTicketPrice = new TicketPrice(
                ticketPrice.getCustomerStatus(),
                ticketPrice.getTicketDuration(),
                ticketPrice.getZone()
        );
        deletedTicketPrice.setActive(false);
        Mockito.when(ticketPriceRepositoryMocked.save(deletedTicketPrice)).thenReturn(deletedTicketPrice);

        TicketPrice ticketPriceFinal = ticketPriceService.deleteById(zoneId, duration, customer);
        assertNotNull(ticketPriceFinal);
        assertFalse(ticketPriceFinal.isActive());
        assertEquals(zoneId, ticketPriceFinal.getZone().getId(), 0.0);
        assertEquals(duration.toUpperCase(), ticketPriceFinal.getTicketDuration().name());
        assertEquals(customer.toUpperCase(), ticketPriceFinal.getCustomerStatus().name());
        verify(zoneServiceMocked, atMost(1)).get(zoneId);
        verify(ticketPriceRepositoryMocked, atMost(1)).findTicketPriceByIdEquals(ticketPrice.getId());
        verify(ticketPriceRepositoryMocked, atMost(1)).save(deletedTicketPrice);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteIllegalCustomerStatusEx()
        throws ZoneNotFound
    {
        long zoneId = 1L;
        String customer = "asd";
        String duration = "month";

        ticketPriceService.deleteById(zoneId, duration, customer);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteIllegalDurationEx() throws ZoneNotFound {
        long zoneId = 1L;
        String customer = "student";
        String duration = "asd";

        ticketPriceService.deleteById(zoneId, duration, customer);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testDeleteResourceZoneNotFoundEx() throws ZoneNotFound {
        long zoneId = 1L;
        String customer = "student";
        String duration = "month";

        Mockito.when(zoneServiceMocked.get(zoneId)).thenThrow(ResourceNotFoundException.class);
        ticketPriceService.deleteById(zoneId, duration, customer);
        verify(zoneServiceMocked, atMost(1)).get(zoneId);
    }

//    @Test(expected = TicketPriceNotFound.class)
//    public void testDeleteResourceTicketNotFoundEx()
//        throws ZoneNotFound
//    {
//        long zoneId = 1L;
//        String customer = "student";
//        String duration = "month";
//
//        Zone zone = new Zone("zone 1");
//        zone.setId(1L);
//        Mockito.when(zoneServiceMocked.get(zoneId)).thenReturn(zone);
//
//        TicketPrice.TicketPriceKey id = new TicketPrice.TicketPriceKey(
//                CustomerStatus.valueOf(customer.toUpperCase()),
//                TicketDuration.valueOf(duration.toUpperCase()),
//                zone
//        );
//        Mockito.when(ticketPriceRepositoryMocked.findTicketPriceByIdEquals(id)).thenReturn(null);
//
//        ticketPriceService.deleteById(zoneId, duration, customer);
//        verify(zoneServiceMocked, atMost(1)).get(zoneId);
//        verify(ticketPriceRepositoryMocked, atMost(1)).findTicketPriceByIdEquals(id);
//    }

}

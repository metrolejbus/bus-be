package com.metrolejbus.app.ticketing.services;

import com.metrolejbus.app.ClearDatabaseRule;
import com.metrolejbus.app.ticketing.forms.CreateDailyTicketForm;
import com.metrolejbus.app.ticketing.forms.UpdateDailyTicketForm;
import com.metrolejbus.app.ticketing.models.DailyTicket;
import com.metrolejbus.app.ticketing.repositories.DailyTicketRepository;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.repositories.ZoneRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.management.InvalidAttributeValueException;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test, controller-test")
public class DailyTicketServiceIntegrationTest {

    @Rule
    @Autowired
    public ClearDatabaseRule clearDatabaseRule;

    @Autowired
    private DailyTicketService dailyTicketService;

    @Autowired
    private ZoneRepository zoneRepository;

    @Autowired
    private DailyTicketRepository ticketRepository;

    @Before
    public void setUp() throws InvalidAttributeValueException {
        Zone zone = new Zone("zone 1");
        Zone zone2 = new Zone("zone 2");
        zoneRepository.save(zone);
        zoneRepository.save(zone2);

        DailyTicket ticket = new DailyTicket(zone, LocalDate.parse("25.12.2019.", DateTimeFormatter.ofPattern("d.M.yyyy.")));
        ticket.setActive(true);
        ticketRepository.save(ticket);
        DailyTicket ticket2 = new DailyTicket(zone, LocalDate.parse("25.12.2019.", DateTimeFormatter.ofPattern("d.M.yyyy.")));
        ticket2.setActive(true);
        ticketRepository.save(ticket2);


        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        manager.createUser(User
                .withUsername("user")
                .password("password")
                .authorities("USER").build());
        UserDetails userDetails = manager.loadUserByUsername("user");

        if (userDetails.getPassword().equals("password")) {
            Collection roles = userDetails.getAuthorities();
            Authentication auth =
                    new UsernamePasswordAuthenticationToken(
                            "user", "", roles
                    );
            SecurityContextHolder.getContext()
                    .setAuthentication(auth);
        }
    }

//    @Test
//    public void testCreateSuccess()
//        throws InvalidAttributeValueException, ZoneNotFound
//    {
//        CreateDailyTicketForm data = new CreateDailyTicketForm();
//        data.zoneId = zoneRepository.findByName("zone 1").getId();
//        data.date = "25.12.2019.";
//
//        DailyTicket createdTicket = dailyTicketService.create(data);
//
//        assertNotNull(createdTicket);
//        assertEquals(data.zoneId, createdTicket.getZone().getId());
//        assertEquals(data.date, createdTicket.getDate().format(DateTimeFormatter.ofPattern("d.M.yyyy.")));
//        assertTrue(createdTicket.isActive());
//    }

    @Test(expected = DateTimeParseException.class)
    public void testCreateDateTimeParseEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreateDailyTicketForm data = new CreateDailyTicketForm();
        data.zoneId = zoneRepository.findByName("zone 1").getId();
        data.date = "25.12.2018";

        dailyTicketService.create(data);
    }

    @Test(expected = ZoneNotFound.class)
    public void testCreateResourceZoneNotFoundEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreateDailyTicketForm data = new CreateDailyTicketForm();
        data.zoneId = 123L;
        data.date = "25.12.2019.";

        dailyTicketService.create(data);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testCreateDateInPastEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreateDailyTicketForm data = new CreateDailyTicketForm();
        data.zoneId = zoneRepository.findByName("zone 1").getId();
        data.date = "25.11.2017.";

        dailyTicketService.create(data);
    }

    @Test
    public void testUpdateOnlyDateSuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateDailyTicketForm data = new UpdateDailyTicketForm();
        data.id = 1;
        data.date = "28.12.2019.";

        DailyTicket ticketAfterUpdate = dailyTicketService.update(data);

        assertNotNull(ticketAfterUpdate);
        assertEquals(data.date, ticketAfterUpdate.getDate().format(DateTimeFormatter.ofPattern("d.M.yyyy.")));
    }

    @Test
    public void testUpdateOnlyZoneSuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateDailyTicketForm data = new UpdateDailyTicketForm();
        data.id = 1;
        data.zoneId = zoneRepository.findByName("zone 2").getId();

        DailyTicket ticketAfterUpdate = dailyTicketService.update(data);

        assertNotNull(ticketAfterUpdate);
        assertEquals(data.id, ticketAfterUpdate.getId(), 0.0);
        assertEquals(data.zoneId, ticketAfterUpdate.getZone().getId());
    }

    @Test
    public void testUpdateDateAndZoneSuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateDailyTicketForm data = new UpdateDailyTicketForm();
        data.id = 1;
        data.date = "28.12.2019.";
        data.zoneId = zoneRepository.findByName("zone 2").getId();

        DailyTicket ticketAfterUpdate = dailyTicketService.update(data);

        assertNotNull(ticketAfterUpdate);
        assertEquals(data.date, ticketAfterUpdate.getDate().format(DateTimeFormatter.ofPattern("d.M.yyyy.")));
        assertEquals(data.zoneId, ticketAfterUpdate.getZone().getId());
        assertEquals(data.id, ticketAfterUpdate.getId(), 0.0);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testUpdateResourceTicketNotFoundEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateDailyTicketForm data = new UpdateDailyTicketForm();
        data.id = 123;

        dailyTicketService.update(data);
    }

    @Test(expected = ZoneNotFound.class)
    public void testUpdateResourceZoneNotFoundEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateDailyTicketForm data = new UpdateDailyTicketForm();
        data.id = 1;
        data.zoneId = 123L;

        dailyTicketService.update(data);
    }

    @Test(expected = DateTimeParseException.class)
    public void testUpdateDateTimeParseEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateDailyTicketForm data = new UpdateDailyTicketForm();
        data.zoneId = zoneRepository.findByName("zone 2").getId();
        data.id = 1;
        data.date = "25.12.2018";

        dailyTicketService.update(data);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testUpdateDateInPastEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateDailyTicketForm data = new UpdateDailyTicketForm();
        data.id = 1;
        data.zoneId = zoneRepository.findByName("zone 2").getId();
        data.date = "25.11.2017.";

        dailyTicketService.update(data);
    }

    @Test
    @Transactional
    @Rollback
    public void testDeleteSuccess() {
        long id = 1;

        DailyTicket ticketFinal = dailyTicketService.deleteById(id);
        assertNotNull(ticketFinal);
        assertFalse(ticketFinal.isActive());
        assertEquals(id, ticketFinal.getId(), 0.0);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testDeleteResourceTicketNotFoundEx() {
        long id = 123;
        dailyTicketService.deleteById(id);
    }
}

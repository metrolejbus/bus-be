package com.metrolejbus.app.ticketing.services;


import com.metrolejbus.app.ClearDatabaseRule;
import com.metrolejbus.app.common.exceptions.NotFound;
import com.metrolejbus.app.common.models.ValidityPeriod;
import com.metrolejbus.app.customers.models.CustomerStatus;
import com.metrolejbus.app.ticketing.exceptions.TicketPriceNotFound;
import com.metrolejbus.app.ticketing.forms.CreateTicketPriceForm;
import com.metrolejbus.app.ticketing.forms.UpdateTicketPriceForm;
import com.metrolejbus.app.ticketing.models.TicketDuration;
import com.metrolejbus.app.ticketing.models.TicketPrice;
import com.metrolejbus.app.ticketing.repositories.TicketPriceRepository;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.repositories.ZoneRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.management.InvalidAttributeValueException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class TicketPriceServiceIntegrationTest {

    @Rule
    @Autowired
    public ClearDatabaseRule clearDatabaseRule;

    @Autowired
    private TicketPriceService ticketPriceService;

    @Autowired
    private TicketPriceRepository ticketPriceRepository;

    @Autowired
    private ZoneRepository zoneRepository;

    @Before
    public void setUp() throws InvalidAttributeValueException {
        Zone zone = new Zone("zone01");
        Zone zone2 = new Zone("zone02");
        zoneRepository.save(zone);
        zoneRepository.save(zone2);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
        ValidityPeriod validityPeriod = new ValidityPeriod(LocalDate.parse("25.10.2019.", formatter), LocalDate.parse("25.10.2020.", formatter));
        TicketPrice ticketPrice = new TicketPrice(CustomerStatus.STUDENT, TicketDuration.MONTH, zone, 10, validityPeriod);
        ticketPrice.setActive(true);
        TicketPrice ticketPrice2 = new TicketPrice(CustomerStatus.STUDENT, TicketDuration.YEAR, zone2, 10, validityPeriod);
        ticketPrice2.setActive(true);
        TicketPrice ticketPrice3 = new TicketPrice(CustomerStatus.STUDENT, TicketDuration.DAY, zone, 10, validityPeriod);
        ticketPrice3.setActive(true);
        ticketPriceRepository.save(ticketPrice);
        ticketPriceRepository.save(ticketPrice2);
        ticketPriceRepository.save(ticketPrice3);
    }

    @Test
    public void testFindByIdSuccess() throws ZoneNotFound {
        long zoneId = zoneRepository.findByName("zone01").getId();
        String duration = "day";
        String customer = "student";

        TicketPrice foundTicketPrice = ticketPriceService.findById(zoneId, duration, customer);
        assertNotNull(foundTicketPrice);
        assertEquals(zoneId, foundTicketPrice.getZone().getId(), 0.0);
        assertEquals(duration.toUpperCase(), foundTicketPrice.getTicketDuration().name());
        assertEquals(customer.toUpperCase(), foundTicketPrice.getCustomerStatus().name());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindByIdIllegalCustomerEx() throws ZoneNotFound {
        long zoneId = zoneRepository.findByName("zone01").getId();
        String duration = "month";
        String customer = "asd";

        ticketPriceService.findById(zoneId, duration, customer);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindByIdIllegalDurationEx() throws ZoneNotFound {
        long zoneId = zoneRepository.findByName("zone01").getId();
        String duration = "asd";
        String customer = "student";

        ticketPriceService.findById(zoneId, duration, customer);
    }

    @Test(expected = ZoneNotFound.class)
    public void testFindByIdResourceZoneNotFoundEx()
        throws ZoneNotFound
    {
        long zoneId = 321;
        String duration = "month";
        String customer = "student";

        ticketPriceService.findById(zoneId, duration, customer);
    }

//    @Test(expected = TicketPriceNotFound.class)
//    public void testFindByIdResourceTicketPriceNotFoundEx()
//        throws ZoneNotFound
//    {
//        long zoneId = zoneRepository.findByName("zone01").getId();
//        String duration = "single_use";
//        String customer = "student";
//
//        ticketPriceService.findById(zoneId, duration, customer);
//    }

    @Test
    public void testCreateSuccess()
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.customerStatus = "adult";
        data.startDate = "25.12.2019.";
        data.endDate = "25.12.2020.";
        data.ticketDuration = "month";
        data.value = (float)10;
        data.zoneId = zoneRepository.findByName("zone01").getId();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");

        TicketPrice createdTicketPrice = ticketPriceService.create(data);
        assertNotNull(createdTicketPrice);
        assertEquals(data.zoneId, createdTicketPrice.getZone().getId());
        assertEquals(data.customerStatus.toUpperCase(), createdTicketPrice.getCustomerStatus().name());
        assertEquals(data.startDate, createdTicketPrice.getValidityPeriod().getStartDate().format(formatter));
        assertEquals(data.endDate, createdTicketPrice.getValidityPeriod().getEndDate().format(formatter));
        assertEquals(data.ticketDuration.toUpperCase(), createdTicketPrice.getTicketDuration().name());
    }

    @Test(expected = ZoneNotFound.class)
    public void testCreateResourceZoneNotFoundEx()
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.customerStatus = "student";
        data.startDate = "25.12.2019.";
        data.endDate = "25.12.2020.";
        data.ticketDuration = "month";
        data.value = (float)10;
        data.zoneId = 123L;

        ticketPriceService.create(data);
    }

    @Test(expected = DateTimeParseException.class)
    public void testCreateDateTimeParseEx()
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.customerStatus = "student";
        data.startDate = "25.12.2019";
        data.endDate = "25.12.2020.";
        data.ticketDuration = "month";
        data.value = (float)10;
        data.zoneId = zoneRepository.findByName("zone01").getId();

        ticketPriceService.create(data);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testCreateDateInPastEx()
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.customerStatus = "student";
        data.startDate = "25.12.2017.";
        data.endDate = "25.12.2020.";
        data.ticketDuration = "month";
        data.value = (float)10;
        data.zoneId = zoneRepository.findByName("zone01").getId();

        ticketPriceService.create(data);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testCreateEndDateBeforeStartDateEx()
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.customerStatus = "student";
        data.startDate = "25.12.2020.";
        data.endDate = "25.12.2019.";
        data.ticketDuration = "month";
        data.value = (float)10;
        data.zoneId = zoneRepository.findByName("zone01").getId();

        ticketPriceService.create(data);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testCreateInvalidValueEx()
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.customerStatus = "student";
        data.startDate = "25.12.2019.";
        data.endDate = "25.12.2020.";
        data.ticketDuration = "month";
        data.value = (float)-10;
        data.zoneId = zoneRepository.findByName("zone01").getId();

        ticketPriceService.create(data);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateIllegalCustomerStatusEx()
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.customerStatus = "asd";
        data.startDate = "25.12.2019.";
        data.endDate = "25.12.2020.";
        data.ticketDuration = "month";
        data.value = (float)10;
        data.zoneId = zoneRepository.findByName("zone01").getId();

        ticketPriceService.create(data);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateIllegalDurationEx()
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.customerStatus = "student";
        data.startDate = "25.12.2019.";
        data.endDate = "25.12.2020.";
        data.ticketDuration = "asd";
        data.value = (float)10;
        data.zoneId = zoneRepository.findByName("zone01").getId();

        ticketPriceService.create(data);
    }

    @Test
    public void testUpdateValueOnlySuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "month";
        data.zoneId = zoneRepository.findByName("zone01").getId();
        data.value = (float)20;

        TicketPrice ticketPriceFinal = ticketPriceService.update(data);
        assertNotNull(ticketPriceFinal);
        assertEquals(data.value, ticketPriceFinal.getValue(), 0.0);
        assertEquals(CustomerStatus.valueOf(data.customerStatus.toUpperCase()), ticketPriceFinal.getCustomerStatus());
        assertEquals(TicketDuration.valueOf(data.ticketDuration.toUpperCase()), ticketPriceFinal.getTicketDuration());
        assertEquals(data.zoneId, ticketPriceFinal.getZone().getId());
    }

    @Test
    public void testUpdateStartDateOnlySuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "month";
        data.zoneId = zoneRepository.findByName("zone01").getId();
        data.startDate = "25.11.2019.";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");

        TicketPrice ticketPriceFinal = ticketPriceService.update(data);
        assertNotNull(ticketPriceFinal);
        assertEquals(LocalDate.parse(data.startDate, formatter), ticketPriceFinal.getValidityPeriod().getStartDate());
        assertEquals(CustomerStatus.valueOf(data.customerStatus.toUpperCase()), ticketPriceFinal.getCustomerStatus());
        assertEquals(TicketDuration.valueOf(data.ticketDuration.toUpperCase()), ticketPriceFinal.getTicketDuration());
        assertEquals(data.zoneId, ticketPriceFinal.getZone().getId());
    }

    @Test
    public void testUpdateEndDateOnlySuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "month";
        data.zoneId = zoneRepository.findByName("zone01").getId();
        data.endDate = "25.11.2020.";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");

        TicketPrice ticketPriceFinal = ticketPriceService.update(data);
        assertNotNull(ticketPriceFinal);
        assertEquals(LocalDate.parse(data.endDate, formatter), ticketPriceFinal.getValidityPeriod().getEndDate());
        assertEquals(CustomerStatus.valueOf(data.customerStatus.toUpperCase()), ticketPriceFinal.getCustomerStatus());
        assertEquals(TicketDuration.valueOf(data.ticketDuration.toUpperCase()), ticketPriceFinal.getTicketDuration());
        assertEquals(data.zoneId, ticketPriceFinal.getZone().getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateIllegalCustomerStatusEx()
        throws NotFound, InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "ashs";
        data.ticketDuration = "year";
        data.zoneId = zoneRepository.findByName("zone01").getId();

        ticketPriceService.update(data);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateIllegalDurationEx()
        throws NotFound, InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "adult";
        data.ticketDuration = "asd";
        data.zoneId = zoneRepository.findByName("zone01").getId();

        ticketPriceService.update(data);
    }

    @Test(expected = ZoneNotFound.class)
    public void testUpdateResourceZoneNotFoundEx()
        throws NotFound, InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "year";
        data.zoneId = 123L;

        ticketPriceService.update(data);
    }

//    @Test(expected = TicketPriceNotFound.class)
//    public void testUpdateResourceTicketPriceNotFoundEx()
//        throws InvalidAttributeValueException, ZoneNotFound
//    {
//        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
//        data.customerStatus = "adult";
//        data.ticketDuration = "day";
//        data.zoneId = zoneRepository.findByName("zone01").getId();
//
//        ticketPriceService.update(data);
//    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testUpdateInvalidValueEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "month";
        data.zoneId = zoneRepository.findByName("zone01").getId();
        data.value = (float)-10;

        ticketPriceService.update(data);
    }

    @Test(expected = DateTimeParseException.class)
    public void testUpdateDateTimeParseEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "month";
        data.zoneId = zoneRepository.findByName("zone01").getId();
        data.startDate = "25.11.2019";

        ticketPriceService.update(data);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testUpdateStartDateInPastEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "month";
        data.zoneId = zoneRepository.findByName("zone01").getId();
        data.startDate = "25.11.2017.";

        ticketPriceService.update(data);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testUpdateStartDateAfterEndDateEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "month";
        data.zoneId = zoneRepository.findByName("zone01").getId();
        data.startDate = "25.12.2021.";

        ticketPriceService.update(data);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testUpdateEndDateBeforeStartDateEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "month";
        data.zoneId = zoneRepository.findByName("zone01").getId();
        data.endDate = "01.01.2019.";

        ticketPriceService.update(data);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testUpdateZoneIdNull()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.customerStatus = "student";
        data.ticketDuration = "month";

        ticketPriceService.update(data);
    }

    @Test
    public void testDeleteSuccess() throws ZoneNotFound {
        long zoneId = zoneRepository.findByName("zone01").getId();
        String duration = "day";
        String customer = "student";

        TicketPrice ticketPriceFinal = ticketPriceService.deleteById(zoneId, duration, customer);
        assertNotNull(ticketPriceFinal);
        assertFalse(ticketPriceFinal.isActive());
        assertEquals(zoneId, ticketPriceFinal.getZone().getId(), 0.0);
        assertEquals(duration.toUpperCase(), ticketPriceFinal.getTicketDuration().name());
        assertEquals(customer.toUpperCase(), ticketPriceFinal.getCustomerStatus().name());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteIllegalCustomerStatusEx()
        throws ZoneNotFound
    {
        long zoneId = zoneRepository.findByName("zone01").getId();
        String customer = "asd";
        String duration = "month";

        ticketPriceService.deleteById(zoneId, duration, customer);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteIllegalDurationEx() throws ZoneNotFound {
        long zoneId = zoneRepository.findByName("zone01").getId();
        String customer = "student";
        String duration = "asd";

        ticketPriceService.deleteById(zoneId, duration, customer);
    }

    @Test(expected = ZoneNotFound.class)
    public void testDeleteResourceZoneNotFoundEx() throws ZoneNotFound {
        long zoneId = 123L;
        String customer = "student";
        String duration = "day";

        ticketPriceService.deleteById(zoneId, duration, customer);
    }

    @Test(expected = NullPointerException.class)
    public void testDeleteResourceTicketNotFoundEx()
        throws ZoneNotFound
    {
        long zoneId = zoneRepository.findByName("zone01").getId();
        String customer = "senior";
        String duration = "month";

        ticketPriceService.deleteById(zoneId, duration, customer);
    }

}

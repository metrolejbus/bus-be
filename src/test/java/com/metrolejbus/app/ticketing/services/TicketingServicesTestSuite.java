package com.metrolejbus.app.ticketing.services;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
    DailyTicketServiceIntegrationTest.class,
    DailyTicketServiceTest.class,
    PeriodicTicketServiceIntegrationTest.class,
    PeriodicTicketServiceTest.class,
    SingleUseTicketServiceIntegrationTest.class,
    SingleUseTicketServiceTest.class,
    TicketPriceServiceIntegrationTest.class,
    TicketPriceServiceTest.class
})
public class TicketingServicesTestSuite {}

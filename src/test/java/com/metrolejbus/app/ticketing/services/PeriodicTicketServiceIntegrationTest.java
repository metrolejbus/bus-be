package com.metrolejbus.app.ticketing.services;

import com.metrolejbus.app.ClearDatabaseRule;
import com.metrolejbus.app.ticketing.forms.CreatePeriodicTicketForm;
import com.metrolejbus.app.ticketing.forms.UpdatePeriodicTicketForm;
import com.metrolejbus.app.ticketing.models.PeriodicTicket;
import com.metrolejbus.app.ticketing.models.PeriodicTicketState;
import com.metrolejbus.app.ticketing.models.TicketDuration;
import com.metrolejbus.app.ticketing.repositories.PeriodicTicketRepository;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.repositories.ZoneRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.management.InvalidAttributeValueException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class PeriodicTicketServiceIntegrationTest {

    @Rule
    @Autowired
    public ClearDatabaseRule clearDatabaseRule;

    @Autowired
    private PeriodicTicketService periodicTicketService;

    @Autowired
    private PeriodicTicketRepository periodicTicketRepository;

    @Autowired
    private ZoneRepository zoneRepository;

    @Before
    public void setUp() throws InvalidAttributeValueException {
        Zone zone = new Zone("zone1");
        Zone zone2 = new Zone("zone2");
        zoneRepository.save(zone);
        zoneRepository.save(zone2);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
        PeriodicTicket ticket = new PeriodicTicket(zone, TicketDuration.MONTH, LocalDate.parse("25.11.2019.", formatter));
        ticket.setActive(true);
        PeriodicTicket ticket2 = new PeriodicTicket(zone, TicketDuration.MONTH, LocalDate.parse("25.11.2019.", formatter));
        ticket2.setActive(true);
        periodicTicketRepository.save(ticket);
        periodicTicketRepository.save(ticket2);
    }

//    @Test
//    public void testCreateSuccess()
//        throws InvalidAttributeValueException, ZoneNotFound
//    {
//        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
//        data.zoneId = zoneRepository.findByName("zone1").getId();
//        data.duration = "month";
//        data.validFrom = "25.10.2019.";
//
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
//
//        PeriodicTicket createdTicket = periodicTicketService.create(data);
//        assertNotNull(createdTicket);
//        assertTrue(createdTicket.isActive());
//        assertEquals(data.duration.toUpperCase(), createdTicket.getDuration().name());
//        assertEquals(data.zoneId, createdTicket.getZone().getId());
//        assertEquals(data.validFrom, createdTicket.getValidFrom().format(formatter));
//        assertEquals(LocalDate.parse(data.validFrom, formatter).plusMonths(1), createdTicket.getValidTo());
//        assertEquals(PeriodicTicketState.CREATED, createdTicket.getState());
//    }

    @Test(expected = DateTimeParseException.class)
    public void testCreateDateTimeParseEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
        data.zoneId = zoneRepository.findByName("zone1").getId();
        data.duration = "year";
        data.validFrom = "25.11.2019";

        periodicTicketService.create(data);
    }

    @Test(expected = ZoneNotFound.class)
    public void testCreateResourceZoneNotFoundEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
        data.zoneId = 123L;
        data.duration = "month";
        data.validFrom = "25.11.2019.";

        periodicTicketService.create(data);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testCreateInvalidDurationEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
        data.zoneId = zoneRepository.findByName("zone1").getId();
        data.duration = "day";
        data.validFrom = "25.11.2019.";

        periodicTicketService.create(data);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateIllegalDurationEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
        data.zoneId = zoneRepository.findByName("zone1").getId();
        data.duration = "askds";
        data.validFrom = "25.11.2019.";

        periodicTicketService.create(data);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testCreateDateInPastEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
        data.zoneId = zoneRepository.findByName("zone1").getId();
        data.duration = "month";
        data.validFrom = "25.11.2017.";

        periodicTicketService.create(data);
    }

    @Test
    public void testUpdateOnlyZoneSuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.id = 1;
        data.zoneId = zoneRepository.findByName("zone2").getId();

        PeriodicTicket ticketFinal = periodicTicketService.update(data);
        assertNotNull(ticketFinal);
        assertEquals(data.id, ticketFinal.getId(), 0.0);
        assertEquals(data.zoneId, ticketFinal.getZone().getId());
    }

    @Test
    public void testUpdateOnlyValidityPeriodSuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.id = 1;
        data.validFrom = "25.10.2019.";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");

        PeriodicTicket ticketFinal = periodicTicketService.update(data);
        assertNotNull(ticketFinal);
        assertEquals(data.id, ticketFinal.getId(), 0.0);
        assertEquals(data.validFrom, ticketFinal.getValidFrom().format(formatter));
        if (ticketFinal.getDuration().name().equals("MONTH")) {
            assertEquals(LocalDate.parse(data.validFrom, formatter).plusMonths(1), ticketFinal.getValidTo());
        } else {
            assertEquals(LocalDate.parse(data.validFrom, formatter).plusYears(1), ticketFinal.getValidTo());
        }
    }

    @Test
    public void testUpdateOnlyDurationSuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.id = 1;
        data.duration = "year";

        PeriodicTicket ticketFinal = periodicTicketService.update(data);
        assertNotNull(ticketFinal);
        assertEquals(data.duration.toUpperCase(), ticketFinal.getDuration().name());
        assertEquals(data.id, ticketFinal.getId(), 0.0);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testUpdateResourceTicketNotFoundEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.id = 123;

        periodicTicketService.update(data);
    }

    @Test(expected = ZoneNotFound.class)
    public void testUpdateResourceZoneNotFoundEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.id = 1;
        data.zoneId = 123L;

        periodicTicketService.update(data);
    }

    @Test(expected = DateTimeParseException.class)
    public void testUpdateDateTimeParseEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.id = 1;
        data.validFrom = "25.12.2019";

        periodicTicketService.update(data);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testUpdateInvalidDurationEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.id = 1;
        data.duration = "day";

        periodicTicketService.update(data);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateIllegalDurationEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.id = 1;
        data.duration = "asds";

        periodicTicketService.update(data);
    }

    @Test
    public void testDeleteSuccess() {
        long id = 2;

        PeriodicTicket ticketFinal = periodicTicketService.deleteById(id);
        assertNotNull(ticketFinal);
        assertFalse(ticketFinal.isActive());
        assertEquals(id, ticketFinal.getId(), 0.0);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testDeleteResourceTicketNotFoundEx() {
        long id = 123;

        periodicTicketService.deleteById(id);
    }
}

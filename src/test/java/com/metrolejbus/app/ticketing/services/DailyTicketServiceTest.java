package com.metrolejbus.app.ticketing.services;

import com.metrolejbus.app.ticketing.forms.CreateDailyTicketForm;
import com.metrolejbus.app.ticketing.forms.UpdateDailyTicketForm;
import com.metrolejbus.app.ticketing.models.DailyTicket;
import com.metrolejbus.app.ticketing.repositories.DailyTicketRepository;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.services.ZoneService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.management.InvalidAttributeValueException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class DailyTicketServiceTest {

    @Autowired
    private DailyTicketService dailyTicketService;

    @MockBean
    private DailyTicketRepository dailyTicketRepositoryMocked;

    @MockBean
    private ZoneService zoneServiceMocked;

//    @Test
//    public void testCreateSuccess()
//        throws InvalidAttributeValueException, ZoneNotFound
//    {
//        Zone zone = new Zone("zone 1");
//        zone.setId(1L);
//
//        CreateDailyTicketForm data = new CreateDailyTicketForm();
//        data.zoneId = 1L;
//        data.date = "25.12.2019.";
//
//        DailyTicket ticket = new DailyTicket(zone, LocalDate.parse(data.date, DateTimeFormatter.ofPattern("d.M.yyyy.")));
//        ticket.setActive(true);
//
//        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(zone);
//        Mockito.when(dailyTicketRepositoryMocked.save(ticket)).thenReturn(ticket);
//
//        DailyTicket createdTicket = dailyTicketService.create(data);
//
//        assertNotNull(createdTicket);
//        assertEquals(data.zoneId, createdTicket.getZone().getId());
//        assertEquals(zone.getName(), createdTicket.getZone().getName());
//        assertEquals(data.date, createdTicket.getDate().format(DateTimeFormatter.ofPattern("d.M.yyyy.")));
//        assertTrue(createdTicket.isActive());
//        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
//        verify(dailyTicketRepositoryMocked, atMost(1)).save(ticket);
//    }

    @Test(expected = DateTimeParseException.class)
    public void testCreateDateTimeParseEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreateDailyTicketForm data = new CreateDailyTicketForm();
        data.zoneId = 1L;
        data.date = "25.12.2018";

        dailyTicketService.create(data);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testCreateResourceZoneNotFoundEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreateDailyTicketForm data = new CreateDailyTicketForm();
        data.zoneId = 2L;
        data.date = "25.12.2019.";

        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenThrow(ResourceNotFoundException.class);

        dailyTicketService.create(data);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testCreateDateInPastEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        Zone zone = new Zone("zone 1");
        zone.setId(1L);

        CreateDailyTicketForm data = new CreateDailyTicketForm();
        data.zoneId = 1L;
        data.date = "25.11.2018.";

        new DailyTicket(zone, LocalDate.parse(data.date, DateTimeFormatter.ofPattern("d.M.yyyy.")));

        dailyTicketService.create(data);
    }

    @Test
    public void testUpdateOnlyDateSuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateDailyTicketForm data = new UpdateDailyTicketForm();
        data.id = 1L;
        data.date = "28.12.2019.";

        Zone zone = new Zone("zone 1");
        zone.setId(1L);

        DailyTicket ticketToUpdate = new DailyTicket();
        ticketToUpdate.setId(1L);
        ticketToUpdate.setDate(LocalDate.parse("26.12.2019.", DateTimeFormatter.ofPattern("d.M.yyyy.")));
        ticketToUpdate.setZone(zone);

        DailyTicket updatedTicket = new DailyTicket();
        updatedTicket.setId(ticketToUpdate.getId());
        updatedTicket.setDate(LocalDate.parse(data.date, DateTimeFormatter.ofPattern("d.M.yyyy.")));
        updatedTicket.setZone(ticketToUpdate.getZone());

        Mockito.when(dailyTicketRepositoryMocked.findById(data.id)).thenReturn(Optional.of(ticketToUpdate));
        Mockito.when(dailyTicketRepositoryMocked.save(updatedTicket)).thenReturn(updatedTicket);

        DailyTicket ticketAfterUpdate = dailyTicketService.update(data);

        assertNotNull(ticketAfterUpdate);
        assertEquals(data.date, ticketAfterUpdate.getDate().format(DateTimeFormatter.ofPattern("d.M.yyyy.")));
        assertEquals(ticketToUpdate.getZone().getName(), ticketAfterUpdate.getZone().getName());
        assertEquals(ticketToUpdate.getId(), ticketAfterUpdate.getId());
        verify(dailyTicketRepositoryMocked, atMost(1)).findById(data.id);
        verify(dailyTicketRepositoryMocked, atMost(1)).save(updatedTicket);
    }

    @Test
    public void testUpdateOnlyZoneSuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateDailyTicketForm data = new UpdateDailyTicketForm();
        data.id = 1L;
        data.zoneId = 2L;

        Zone oldZone = new Zone("zone 1");
        oldZone.setId(1L);

        Zone newZone = new Zone("zone 2");
        newZone.setId(2L);

        DailyTicket ticketToUpdate = new DailyTicket();
        ticketToUpdate.setId(1L);
        ticketToUpdate.setDate(LocalDate.parse("28.12.2019.", DateTimeFormatter.ofPattern("d.M.yyyy.")));
        ticketToUpdate.setZone(oldZone);

        DailyTicket updatedTicket = new DailyTicket();
        updatedTicket.setId(ticketToUpdate.getId());
        updatedTicket.setDate(ticketToUpdate.getDate());
        updatedTicket.setZone(newZone);

        Mockito.when(dailyTicketRepositoryMocked.findById(data.id)).thenReturn(Optional.of(ticketToUpdate));
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(newZone);
        Mockito.when(dailyTicketRepositoryMocked.save(updatedTicket)).thenReturn(updatedTicket);

        DailyTicket ticketAfterUpdate = dailyTicketService.update(data);

        assertNotNull(ticketAfterUpdate);
        assertEquals(ticketToUpdate.getDate(), ticketAfterUpdate.getDate());
        assertEquals(data.zoneId, ticketAfterUpdate.getZone().getId());
        assertEquals(ticketToUpdate.getId(), ticketAfterUpdate.getId());
        verify(dailyTicketRepositoryMocked, atMost(1)).findById(data.id);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
        verify(dailyTicketRepositoryMocked, atMost(1)).save(updatedTicket);
    }

    @Test
    public void testUpdateDateAndZoneSuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateDailyTicketForm data = new UpdateDailyTicketForm();
        data.id = 1L;
        data.date = "28.12.2019.";
        data.zoneId = 2L;

        Zone oldZone = new Zone("zone 1");
        oldZone.setId(1L);

        Zone newZone = new Zone("zone 2");
        newZone.setId(2L);

        DailyTicket ticketToUpdate = new DailyTicket();
        ticketToUpdate.setId(1L);
        ticketToUpdate.setDate(LocalDate.parse("26.12.2019.", DateTimeFormatter.ofPattern("d.M.yyyy.")));
        ticketToUpdate.setZone(oldZone);

        DailyTicket updatedTicket = new DailyTicket();
        updatedTicket.setId(ticketToUpdate.getId());
        updatedTicket.setDate((LocalDate.parse(data.date, DateTimeFormatter.ofPattern("d.M.yyyy."))));
        updatedTicket.setZone(newZone);

        Mockito.when(dailyTicketRepositoryMocked.findById(data.id)).thenReturn(Optional.of(ticketToUpdate));
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(newZone);
        Mockito.when(dailyTicketRepositoryMocked.save(updatedTicket)).thenReturn(updatedTicket);

        DailyTicket ticketAfterUpdate = dailyTicketService.update(data);

        assertNotNull(ticketAfterUpdate);
        assertEquals(data.date, ticketAfterUpdate.getDate().format(DateTimeFormatter.ofPattern("d.M.yyyy.")));
        assertEquals(data.zoneId, ticketAfterUpdate.getZone().getId());
        assertEquals(ticketToUpdate.getId(), ticketAfterUpdate.getId());
        verify(dailyTicketRepositoryMocked, atMost(1)).findById(data.id);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
        verify(dailyTicketRepositoryMocked, atMost(1)).save(updatedTicket);
    }

    @Test
    public void testUpdateNoChange()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateDailyTicketForm data = new UpdateDailyTicketForm();
        data.id = 1L;

        DailyTicket ticket = new DailyTicket();
        ticket.setId(1L);
        ticket.setDate(LocalDate.parse("28.12.2019.", DateTimeFormatter.ofPattern("d.M.yyyy.")));
        ticket.setZone(new Zone("zone 1"));

        Mockito.when(dailyTicketRepositoryMocked.findById(data.id)).thenReturn(Optional.of(ticket));
        Mockito.when(dailyTicketRepositoryMocked.save(ticket)).thenReturn(ticket);

        DailyTicket updatedTicket = dailyTicketService.update(data);
        assertEquals(ticket.getId(), updatedTicket.getId());
        assertEquals(ticket.getDate(), updatedTicket.getDate());
        assertEquals(ticket.getZone().getName(), updatedTicket.getZone().getName());
        verify(dailyTicketRepositoryMocked, atMost(1)).findById(data.id);
        verify(dailyTicketRepositoryMocked, atMost(1)).save(ticket);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testUpdateResourceTicketNotFoundEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateDailyTicketForm data = new UpdateDailyTicketForm();
        data.id = 1L;

        Mockito.when(dailyTicketRepositoryMocked.findById(data.id)).thenThrow(ResourceNotFoundException.class);
        dailyTicketService.update(data);
        verify(dailyTicketRepositoryMocked, atMost(1)).findById(data.id);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testUpdateResourceZoneNotFoundEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateDailyTicketForm data = new UpdateDailyTicketForm();
        data.id = 1L;
        data.zoneId = 1L;

        DailyTicket ticket = new DailyTicket();

        Mockito.when(dailyTicketRepositoryMocked.findById(data.id)).thenReturn(Optional.of(ticket));
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenThrow(ResourceNotFoundException.class);

        dailyTicketService.update(data);
        verify(dailyTicketRepositoryMocked, atMost(1)).findById(data.id);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test(expected = DateTimeParseException.class)
    public void testUpdateDateTimeParseEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateDailyTicketForm data = new UpdateDailyTicketForm();
        data.zoneId = 1L;
        data.date = "25.12.2018";

        Mockito.when(dailyTicketRepositoryMocked.findById(data.id)).thenReturn(Optional.of(new DailyTicket()));
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(new Zone("zone 1"));

        dailyTicketService.update(data);
        verify(dailyTicketRepositoryMocked, atMost(1)).findById(data.id);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testUpdateDateInPastEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateDailyTicketForm data = new UpdateDailyTicketForm();
        data.zoneId = 1L;
        data.date = "25.11.2018.";

        Mockito.when(dailyTicketRepositoryMocked.findById(data.id)).thenReturn(Optional.of(new DailyTicket()));
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(new Zone("zone 1"));

        dailyTicketService.update(data);
        verify(dailyTicketRepositoryMocked, atMost(1)).findById(data.id);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test
    public void testDeleteSuccess() {
        long id = 1L;

        DailyTicket ticket = new DailyTicket();
        ticket.setId(id);
        ticket.setActive(true);

        Mockito.when(dailyTicketRepositoryMocked.findById(id)).thenReturn(Optional.of(ticket));

        DailyTicket deletedTicket = new DailyTicket();
        deletedTicket.setId(ticket.getId());
        deletedTicket.setActive(false);

        Mockito.when(dailyTicketRepositoryMocked.save(deletedTicket)).thenReturn(deletedTicket);

        DailyTicket ticketFinal = dailyTicketService.deleteById(id);
        assertNotNull(ticketFinal);
        assertFalse(ticketFinal.isActive());
        assertEquals(ticket.getId(), ticketFinal.getId());
        verify(dailyTicketRepositoryMocked, atMost(1)).findById(id);
        verify(dailyTicketRepositoryMocked, atMost(1)).save(deletedTicket);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testDeleteResourceTicketNotFoundEx() {
        long id = 1L;

        Mockito.when(dailyTicketRepositoryMocked.findById(id)).thenThrow(ResourceNotFoundException.class);
        dailyTicketService.deleteById(id);
        verify(dailyTicketRepositoryMocked, atMost(1)).findById(id);
    }

}

package com.metrolejbus.app.ticketing.services;

import com.metrolejbus.app.ClearDatabaseRule;
import com.metrolejbus.app.ticketing.forms.CreateSingleUseTicketForm;
import com.metrolejbus.app.ticketing.forms.UpdateSingleUseTicketForm;
import com.metrolejbus.app.ticketing.models.SingleUseTicket;
import com.metrolejbus.app.ticketing.repositories.SingleUseTicketRepository;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.repositories.ZoneRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class SingleUseTicketServiceIntegrationTest {

    @Rule
    @Autowired
    public ClearDatabaseRule clearDatabaseRule;

    @Autowired
    private SingleUseTicketService singleUseTicketService;

    @Autowired
    private SingleUseTicketRepository singleUseTicketRepository;

    @Autowired
    private ZoneRepository zoneRepository;

    @Before
    public void setUp() {
        Zone zone = new Zone("z1");
        Zone zone2 = new Zone("z2");
        zoneRepository.save(zone);
        zoneRepository.save(zone2);

        SingleUseTicket ticket = new SingleUseTicket(zone);
        ticket.setActive(true);
        SingleUseTicket ticket2 = new SingleUseTicket(zone);
        ticket2.setActive(true);
        singleUseTicketRepository.save(ticket);
        singleUseTicketRepository.save(ticket2);
    }

//    @Test
//    public void testCreateSuccess() throws ZoneNotFound {
//        CreateSingleUseTicketForm data = new CreateSingleUseTicketForm();
//        data.zoneId = zoneRepository.findByName("z1").getId();
//
//        SingleUseTicket createdTicket = singleUseTicketService.create(data);
//        assertNotNull(createdTicket);
//        assertTrue(createdTicket.isActive());
//        assertEquals(data.zoneId, createdTicket.getZone().getId());
//    }

    @Test(expected = ZoneNotFound.class)
    public void testCreateResourceZoneNotFoundEx() throws ZoneNotFound {
        CreateSingleUseTicketForm data = new CreateSingleUseTicketForm();
        data.zoneId = 123L;

        singleUseTicketService.create(data);
    }

    @Test
    public void testUpdateSuccess() throws ZoneNotFound {
        UpdateSingleUseTicketForm data = new UpdateSingleUseTicketForm();
        data.id = 1L;
        data.zoneId = zoneRepository.findByName("z2").getId();

        SingleUseTicket ticketFinal = singleUseTicketService.update(data);
        assertNotNull(ticketFinal);
        assertEquals(data.zoneId, ticketFinal.getZone().getId());
        assertEquals(data.id, ticketFinal.getId());
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testUpdateResourceTicketNotFoundEx() throws ZoneNotFound {
        UpdateSingleUseTicketForm data = new UpdateSingleUseTicketForm();
        data.id = 123L;

        singleUseTicketService.update(data);
    }

    @Test(expected = ZoneNotFound.class)
    public void testUpdateResourceZoneNotFoundEx() throws ZoneNotFound {
        UpdateSingleUseTicketForm data = new UpdateSingleUseTicketForm();
        data.id = 1L;
        data.zoneId = 123L;

        singleUseTicketService.update(data);
    }

    @Test
    public void testDeleteSuccess() {
        long id = 2;

        SingleUseTicket ticketFinal = singleUseTicketService.deleteById(id);
        assertNotNull(ticketFinal);
        assertFalse(ticketFinal.isActive());
        assertEquals(id, ticketFinal.getId(), 0.0);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testDeleteResourceTicketNotFoundEx() {
        long id = 123L;

        singleUseTicketService.deleteById(id);
    }
}

package com.metrolejbus.app.ticketing.services;

import com.metrolejbus.app.ticketing.forms.CreatePeriodicTicketForm;
import com.metrolejbus.app.ticketing.forms.UpdatePeriodicTicketForm;
import com.metrolejbus.app.ticketing.models.PeriodicTicket;
import com.metrolejbus.app.ticketing.models.TicketDuration;
import com.metrolejbus.app.ticketing.repositories.PeriodicTicketRepository;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.services.ZoneService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.management.InvalidAttributeValueException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class PeriodicTicketServiceTest {

    @Autowired
    private PeriodicTicketService periodicTicketService;

    @MockBean
    private PeriodicTicketRepository periodicTicketRepositoryMocked;

    @MockBean
    private ZoneService zoneServiceMocked;

//    @Test
//    public void testCreateSuccess()
//        throws InvalidAttributeValueException, ZoneNotFound
//    {
//        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
//        data.zoneId = 1L;
//        data.duration = "month";
//        data.validFrom = "25.10.2019.";
//
//        Zone zone = new Zone("zone 1");
//        zone.setId(1L);
//
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
//
//        PeriodicTicket ticket = new PeriodicTicket(zone, TicketDuration.valueOf(data.duration.toUpperCase()), LocalDate.parse(data.validFrom, formatter));
//        ticket.setActive(true);
//
//        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(zone);
//        Mockito.when(periodicTicketRepositoryMocked.save(ticket)).thenReturn(ticket);
//
//        PeriodicTicket createdTicket = periodicTicketService.create(data);
//        assertNotNull(createdTicket);
//        assertTrue(createdTicket.isActive());
//        assertEquals(data.duration.toUpperCase(), createdTicket.getDuration().name());
//        assertEquals(data.zoneId, createdTicket.getZone().getId());
//        assertEquals(data.validFrom, createdTicket.getValidFrom().format(formatter));
//        assertEquals(LocalDate.parse(data.validFrom, formatter).plusMonths(1), createdTicket.getValidTo());
//        assertEquals(PeriodicTicketState.CREATED, createdTicket.getState());
//        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
//        verify(periodicTicketRepositoryMocked, atMost(1)).save(ticket);
//
//    }

    @Test(expected = DateTimeParseException.class)
    public void testCreateDateTimeParseEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
        data.zoneId = 1L;
        data.duration = "month";
        data.validFrom = "25.11.2019";

        periodicTicketService.create(data);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testCreateResourceZoneNotFoundEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
        data.zoneId = 1L;
        data.duration = "month";
        data.validFrom = "25.11.2019.";

        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenThrow(ResourceNotFoundException.class);

        periodicTicketService.create(data);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testCreateInvalidDurationEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
        data.zoneId = 1L;
        data.duration = "day";
        data.validFrom = "25.11.2019.";

        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(new Zone("zone 1"));

        periodicTicketService.create(data);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateIllegalDurationEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
        data.zoneId = 1L;
        data.duration = "askds";
        data.validFrom = "25.11.2019.";

        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(new Zone("zone 1"));

        periodicTicketService.create(data);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testCreateDateInPastEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
        data.zoneId = 1L;
        data.duration = "month";
        data.validFrom = "25.11.2017.";

        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(new Zone("zone 1"));

        periodicTicketService.create(data);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test
    public void testUpdateOnlyZoneSuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.id = 1L;
        data.zoneId = 2L;

        Zone oldZone = new Zone("zone 1");
        oldZone.setId(1L);

        Zone newZone = new Zone("zone 2");
        newZone.setId(2L);

        PeriodicTicket ticket = new PeriodicTicket();
        ticket.setId(1L);
        ticket.setZone(oldZone);

        PeriodicTicket updatedTicket = new PeriodicTicket();
        updatedTicket.setId(ticket.getId());
        updatedTicket.setZone(newZone);

        Mockito.when(periodicTicketRepositoryMocked.findById(data.id)).thenReturn(Optional.of(ticket));
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(newZone);
        Mockito.when(periodicTicketRepositoryMocked.save(updatedTicket)).thenReturn(updatedTicket);

        PeriodicTicket ticketFinal = periodicTicketService.update(data);
        assertNotNull(ticketFinal);
        assertEquals(ticket.getId(), ticketFinal.getId());
        assertEquals(data.zoneId, ticketFinal.getZone().getId());
        verify(periodicTicketRepositoryMocked, atMost(1)).findById(data.id);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
        verify(periodicTicketRepositoryMocked, atMost(1)).save(updatedTicket);
    }

    @Test
    public void testUpdateOnlyValidityPeriodSuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.id = 1L;
        data.validFrom = "25.10.2019.";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");

        PeriodicTicket ticket = new PeriodicTicket();
        ticket.setId(1L);
        ticket.setDuration(TicketDuration.MONTH);
        ticket.setValidFrom(LocalDate.parse("25.09.2019.", formatter));

        PeriodicTicket updatedTicket = new PeriodicTicket();
        updatedTicket.setId(ticket.getId());
        updatedTicket.setDuration(ticket.getDuration());
        updatedTicket.setValidFrom(LocalDate.parse(data.validFrom, formatter));

        Mockito.when(periodicTicketRepositoryMocked.findById(data.id)).thenReturn(Optional.of(ticket));
        Mockito.when(periodicTicketRepositoryMocked.save(updatedTicket)).thenReturn(updatedTicket);

        PeriodicTicket ticketFinal = periodicTicketService.update(data);
        assertNotNull(ticketFinal);
        assertEquals(ticket.getId(), ticketFinal.getId());
        assertEquals(data.validFrom, ticketFinal.getValidFrom().format(formatter));
        assertEquals(LocalDate.parse(data.validFrom, formatter).plusMonths(1), ticketFinal.getValidTo());
        verify(periodicTicketRepositoryMocked, atMost(1)).findById(data.id);
        verify(periodicTicketRepositoryMocked, atMost(1)).save(updatedTicket);
    }

    @Test
    public void testUpdateOnlyDurationSuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.id = 1L;
        data.duration = "month";

        PeriodicTicket ticket = new PeriodicTicket();
        ticket.setId(1L);
        ticket.setDuration(TicketDuration.YEAR);

        PeriodicTicket updatedTicket = new PeriodicTicket();
        updatedTicket.setId(ticket.getId());
        updatedTicket.setDuration(TicketDuration.valueOf(data.duration.toUpperCase()));

        Mockito.when(periodicTicketRepositoryMocked.findById(data.id)).thenReturn(Optional.of(ticket));
        Mockito.when(periodicTicketRepositoryMocked.save(updatedTicket)).thenReturn(updatedTicket);

        PeriodicTicket ticketFinal = periodicTicketService.update(data);
        assertNotNull(ticketFinal);
        assertEquals(data.duration.toUpperCase(), ticketFinal.getDuration().name());
        assertEquals(ticket.getId(), ticketFinal.getId());
        verify(periodicTicketRepositoryMocked, atMost(1)).findById(data.id);
        verify(periodicTicketRepositoryMocked, atMost(1)).save(updatedTicket);
    }

    @Test
    public void testUpdateNoChange()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.id = 1L;

        PeriodicTicket ticket = new PeriodicTicket();
        ticket.setId(1L);
        ticket.setValidFrom(LocalDate.parse("28.12.2019.", DateTimeFormatter.ofPattern("d.M.yyyy.")));
        ticket.setDuration(TicketDuration.MONTH);
        ticket.setZone(new Zone("zone 1"));

        Mockito.when(periodicTicketRepositoryMocked.findById(data.id)).thenReturn(Optional.of(ticket));
        Mockito.when(periodicTicketRepositoryMocked.save(ticket)).thenReturn(ticket);

        PeriodicTicket updatedTicket = periodicTicketService.update(data);
        assertEquals(ticket.getId(), updatedTicket.getId());
        assertEquals(ticket.getValidFrom(), updatedTicket.getValidFrom());
        assertEquals(ticket.getValidTo(), updatedTicket.getValidTo());
        assertEquals(ticket.getZone().getName(), updatedTicket.getZone().getName());
        verify(periodicTicketRepositoryMocked, atMost(1)).findById(data.id);
        verify(periodicTicketRepositoryMocked, atMost(1)).save(ticket);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testUpdateResourceTicketNotFoundEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.id = 1L;

        Mockito.when(periodicTicketRepositoryMocked.findById(data.id)).thenThrow(ResourceNotFoundException.class);

        periodicTicketService.update(data);
        verify(periodicTicketRepositoryMocked, atMost(1)).findById(data.id);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testUpdateResourceZoneNotFoundEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.id = 1L;
        data.zoneId = 1L;

        Mockito.when(periodicTicketRepositoryMocked.findById(data.id)).thenReturn(Optional.of(new PeriodicTicket()));
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenThrow(ResourceNotFoundException.class);

        periodicTicketService.update(data);
        verify(periodicTicketRepositoryMocked, atMost(1)).findById(data.id);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test(expected = DateTimeParseException.class)
    public void testUpdateDateTimeParseEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.id = 1L;
        data.validFrom = "25.12.2019";

        Mockito.when(periodicTicketRepositoryMocked.findById(data.id)).thenReturn(Optional.of(new PeriodicTicket()));

        periodicTicketService.update(data);
        verify(periodicTicketRepositoryMocked, atMost(1)).findById(data.id);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void testUpdateInvalidDurationEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.id = 1L;
        data.duration = "day";

        Mockito.when(periodicTicketRepositoryMocked.findById(data.id)).thenReturn(Optional.of(new PeriodicTicket()));

        periodicTicketService.update(data);
        verify(periodicTicketRepositoryMocked, atMost(1)).findById(data.id);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateIllegalDurationEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.id = 1L;
        data.duration = "asds";

        Mockito.when(periodicTicketRepositoryMocked.findById(data.id)).thenReturn(Optional.of(new PeriodicTicket()));

        periodicTicketService.update(data);
        verify(periodicTicketRepositoryMocked, atMost(1)).findById(data.id);
    }

    @Test
    public void testDeleteSuccess() {
        long id = 1L;

        PeriodicTicket ticket = new PeriodicTicket();
        ticket.setId(1L);
        ticket.setActive(true);

        Mockito.when(periodicTicketRepositoryMocked.findById(ticket.getId())).thenReturn(Optional.of(ticket));

        PeriodicTicket deletedTicket = new PeriodicTicket();
        deletedTicket.setId(ticket.getId());
        deletedTicket.setActive(false);

        Mockito.when(periodicTicketRepositoryMocked.save(deletedTicket)).thenReturn(deletedTicket);

        PeriodicTicket ticketFinal = periodicTicketService.deleteById(id);
        assertNotNull(ticketFinal);
        assertFalse(ticketFinal.isActive());
        assertEquals(ticket.getId(), ticketFinal.getId());
        verify(periodicTicketRepositoryMocked, atMost(1)).findById(id);
        verify(periodicTicketRepositoryMocked, atMost(1)).save(deletedTicket);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testDeleteResourceTicketNotFoundEx() {
        long id = 1L;

        Mockito.when(periodicTicketRepositoryMocked.findById(id)).thenThrow(ResourceNotFoundException.class);
        periodicTicketService.deleteById(id);
        verify(periodicTicketRepositoryMocked, atMost(1)).findById(id);
    }
}

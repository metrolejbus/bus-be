package com.metrolejbus.app.ticketing.services;

import com.metrolejbus.app.ticketing.forms.CreateSingleUseTicketForm;
import com.metrolejbus.app.ticketing.forms.UpdateSingleUseTicketForm;
import com.metrolejbus.app.ticketing.models.SingleUseTicket;
import com.metrolejbus.app.ticketing.repositories.SingleUseTicketRepository;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.services.ZoneService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class SingleUseTicketServiceTest {

    @Autowired
    private SingleUseTicketService singleUseTicketService;

    @MockBean
    private SingleUseTicketRepository singleUseTicketRepositoryMocked;

    @MockBean
    private ZoneService zoneServiceMocked;

//    @Test
//    public void testCreateSuccess() throws ZoneNotFound {
//        CreateSingleUseTicketForm data = new CreateSingleUseTicketForm();
//        data.zoneId = 1L;
//
//        Zone zone = new Zone("zone 1");
//        zone.setId(1L);
//
//        SingleUseTicket ticket = new SingleUseTicket(zone);
//        ticket.setActive(true);
//
//        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(zone);
//        Mockito.when(singleUseTicketRepositoryMocked.save(ticket)).thenReturn(ticket);
//
//        SingleUseTicket createdTicket = singleUseTicketService.create(data);
//        assertNotNull(createdTicket);
//        assertTrue(createdTicket.isActive());
//        assertEquals(data.zoneId, createdTicket.getZone().getId());
//        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
//        verify(singleUseTicketRepositoryMocked, atMost(1)).save(ticket);
//    }

    @Test(expected = ResourceNotFoundException.class)
    public void testCreateResourceZoneNotFoundEx() throws ZoneNotFound {
        CreateSingleUseTicketForm data = new CreateSingleUseTicketForm();
        data.zoneId = 1L;

        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenThrow(ResourceNotFoundException.class);

        singleUseTicketService.create(data);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test
    public void testUpdateSuccess() throws ZoneNotFound {
        UpdateSingleUseTicketForm data = new UpdateSingleUseTicketForm();
        data.id = 1L;
        data.zoneId = 2L;

        Zone oldZone = new Zone("zone 1");
        oldZone.setId(1L);

        Zone newZone = new Zone("zone 2");
        newZone.setId(2L);

        SingleUseTicket ticket = new SingleUseTicket();
        ticket.setId(1L);
        ticket.setZone(oldZone);

        SingleUseTicket updatedTicket = new SingleUseTicket();
        updatedTicket.setId(ticket.getId());
        updatedTicket.setZone(newZone);

        Mockito.when(singleUseTicketRepositoryMocked.findById(data.id)).thenReturn(Optional.of(ticket));
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenReturn(newZone);
        Mockito.when(singleUseTicketRepositoryMocked.save(updatedTicket)).thenReturn(updatedTicket);

        SingleUseTicket ticketFinal = singleUseTicketService.update(data);
        assertNotNull(ticketFinal);
        assertEquals(data.zoneId, ticketFinal.getZone().getId());
        assertEquals(data.id, ticketFinal.getId());
        verify(singleUseTicketRepositoryMocked, atMost(1)).findById(data.id);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
        verify(singleUseTicketRepositoryMocked, atMost(1)).save(updatedTicket);
    }

    @Test
    public void testUpdateNoChange() throws ZoneNotFound {
        UpdateSingleUseTicketForm data = new UpdateSingleUseTicketForm();
        data.id = 1L;

        SingleUseTicket ticket = new SingleUseTicket();
        ticket.setId(1L);

        Mockito.when(singleUseTicketRepositoryMocked.findById(data.id)).thenReturn(Optional.of(ticket));
        Mockito.when(singleUseTicketRepositoryMocked.save(ticket)).thenReturn(ticket);

        SingleUseTicket updatedTicket = singleUseTicketService.update(data);
        assertEquals(ticket.getId(), updatedTicket.getId());
        verify(singleUseTicketRepositoryMocked, atMost(1)).findById(data.id);
        verify(singleUseTicketRepositoryMocked, atMost(1)).save(ticket);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testUpdateResourceTicketNotFoundEx() throws ZoneNotFound {
        UpdateSingleUseTicketForm data = new UpdateSingleUseTicketForm();
        data.id = 1L;

        Mockito.when(singleUseTicketRepositoryMocked.findById(data.id)).thenThrow(ResourceNotFoundException.class);

        singleUseTicketService.update(data);
        verify(singleUseTicketRepositoryMocked, atMost(1)).findById(data.id);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testUpdateResourceZoneNotFoundEx() throws ZoneNotFound {
        UpdateSingleUseTicketForm data = new UpdateSingleUseTicketForm();
        data.id = 1L;
        data.zoneId = 1L;

        Mockito.when(singleUseTicketRepositoryMocked.findById(data.id)).thenReturn(Optional.of(new SingleUseTicket()));
        Mockito.when(zoneServiceMocked.get(data.zoneId)).thenThrow(ResourceNotFoundException.class);

        singleUseTicketService.update(data);
        verify(singleUseTicketRepositoryMocked, atMost(1)).findById(data.id);
        verify(zoneServiceMocked, atMost(1)).get(data.zoneId);
    }

    @Test
    public void testDeleteSuccess() {
        long id = 1L;

        SingleUseTicket ticket = new SingleUseTicket();
        ticket.setId(1L);
        ticket.setActive(true);

        Mockito.when(singleUseTicketRepositoryMocked.findById(id)).thenReturn(Optional.of(ticket));

        SingleUseTicket deletedTicket = new SingleUseTicket();
        deletedTicket.setId(ticket.getId());
        deletedTicket.setActive(false);

        Mockito.when(singleUseTicketRepositoryMocked.save(deletedTicket)).thenReturn(deletedTicket);

        SingleUseTicket ticketFinal = singleUseTicketService.deleteById(id);
        assertNotNull(ticketFinal);
        assertFalse(ticketFinal.isActive());
        assertEquals(id, ticketFinal.getId(), 0.0);
        verify(singleUseTicketRepositoryMocked, atMost(1)).findById(id);
        verify(singleUseTicketRepositoryMocked, atMost(1)).save(deletedTicket);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testDeleteResourceTicketNotFoundEx() {
        long id = 1L;

        Mockito.when(singleUseTicketRepositoryMocked.findById(id)).thenThrow(ResourceNotFoundException.class);
        singleUseTicketService.deleteById(id);
        verify(singleUseTicketRepositoryMocked, atMost(1)).findById(id);
    }
}

package com.metrolejbus.app.ticketing.controllers;

import com.metrolejbus.app.ClearDatabaseRule;
import com.metrolejbus.app.common.models.ValidityPeriod;
import com.metrolejbus.app.customers.models.CustomerStatus;
import com.metrolejbus.app.ticketing.forms.CreateTicketPriceForm;
import com.metrolejbus.app.ticketing.forms.UpdateTicketPriceForm;
import com.metrolejbus.app.ticketing.models.TicketDuration;
import com.metrolejbus.app.ticketing.models.TicketPrice;
import com.metrolejbus.app.ticketing.repositories.TicketPriceRepository;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.repositories.ZoneRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import javax.management.InvalidAttributeValueException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test,controller-test")
public class TicketPriceControllerIntegrationTest {

    @Rule
    @Autowired
    public ClearDatabaseRule clearDatabaseRule;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private TestRestTemplate adminClient;

    @Autowired
    private TicketPriceRepository ticketPriceRepository;

    @Autowired
    private ZoneRepository zoneRepository;

    private static TicketPrice ticketPrice2;

    @PostConstruct
    public void postConstruct() {
        this.adminClient = this.adminClient.withBasicAuth("admin", "password");
    }

    @Before
    public void setUp() throws InvalidAttributeValueException  {
        Zone zone = new Zone("z-1");
        zone.setActive(true);
        zoneRepository.save(zone);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
        ValidityPeriod validityPeriod = new ValidityPeriod(LocalDate.parse("11.11.2019.", formatter), LocalDate.parse("11.11.2020.", formatter));
        TicketPrice ticketPrice = new TicketPrice(CustomerStatus.STUDENT, TicketDuration.MONTH, zone);
        ticketPrice.setValidityPeriod(validityPeriod);
        ticketPrice.setActive(true);
        ticketPriceRepository.save(ticketPrice);

        ticketPrice2 = new TicketPrice(CustomerStatus.STUDENT, TicketDuration.YEAR, zone);
        ticketPrice2.setActive(true);
        ticketPrice2.setValidityPeriod(new ValidityPeriod(LocalDate.of(2019, 12, 12), null));
        ticketPriceRepository.save(ticketPrice2);
    }

    @Test
    public void testFindByIdSuccess() {
        long zoneId = zoneRepository.findByName("z-1").getId();
        String duration = "month";
        String customer = "student";

        ResponseEntity<TicketPrice> response = restTemplate.getForEntity("/ticket-prices/{zoneId}/{duration}/{customer}", TicketPrice.class, zoneId, duration, customer);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testFindByIdResourceNotFoundEx() {
        long zoneId = 123;
        String duration = "month";
        String customer = "student";

        ResponseEntity<TicketPrice> response = restTemplate.getForEntity("/ticket-prices/{zoneId}/{duration}/{customer}", TicketPrice.class, zoneId, duration, customer);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testCreateSuccess() {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.zoneId = zoneRepository.findByName("z-1").getId();
        data.value = (float)10;
        data.ticketDuration = "day";
        data.startDate = "12.11.2019.";
        data.endDate = "12.11.2020.";
        data.customerStatus = "student";

        ResponseEntity<TicketPrice> response = adminClient.exchange("/ticket-prices", HttpMethod.POST, new HttpEntity<>(data), TicketPrice.class);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    public void testCreateResourceNotFoundEx() {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.zoneId = 123L;
        data.value = (float)10;
        data.ticketDuration = "month";
        data.startDate = "12.11.2019.";
        data.endDate = "12.11.2020.";
        data.customerStatus = "student";

        ResponseEntity<TicketPrice> response = adminClient.exchange("/ticket-prices", HttpMethod.POST, new HttpEntity<>(data), TicketPrice.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testCreateDateTimeParseEx() {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.zoneId = zoneRepository.findByName("z-1").getId();
        data.value = (float)10;
        data.ticketDuration = "month";
        data.startDate = "12.11.2019";
        data.endDate = "12.11.2020.";
        data.customerStatus = "student";

        ResponseEntity<TicketPrice> response = adminClient.exchange("/ticket-prices", HttpMethod.POST, new HttpEntity<>(data), TicketPrice.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testCreateInvalidAttributeValueEx() {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.zoneId = zoneRepository.findByName("z-1").getId();
        data.value = (float)10;
        data.ticketDuration = "month";
        data.startDate = "12.11.2017.";
        data.endDate = "12.11.2020.";
        data.customerStatus = "student";

        ResponseEntity<TicketPrice> response = adminClient.exchange("/ticket-prices", HttpMethod.POST, new HttpEntity<>(data), TicketPrice.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testCreateIllegalArgumentEx() {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.zoneId = zoneRepository.findByName("z-1").getId();
        data.value = (float)10;
        data.ticketDuration = "asd";
        data.startDate = "12.11.2019.";
        data.endDate = "12.11.2020.";
        data.customerStatus = "student";

        ResponseEntity<TicketPrice> response = adminClient.exchange("/ticket-prices", HttpMethod.POST, new HttpEntity<>(data), TicketPrice.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testUpdateSuccess() {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.zoneId = zoneRepository.findByName("z-1").getId();
        data.startDate = "12.11.2019.";
        data.endDate = "12.11.2020.";
        data.value = (float)12;
        data.ticketDuration = "month";
        data.customerStatus = "student";

        ResponseEntity<TicketPrice> response = adminClient.exchange("/ticket-prices", HttpMethod.PUT, new HttpEntity<>(data), TicketPrice.class);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testUpdateResourceNotFoundEx() {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.zoneId = 123L;
        data.startDate = "12.11.2019.";
        data.endDate = "12.11.2020.";
        data.value = (float)10;
        data.ticketDuration = "month";
        data.customerStatus = "student";

        ResponseEntity<TicketPrice> response = adminClient.exchange("/ticket-prices", HttpMethod.PUT, new HttpEntity<>(data), TicketPrice.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testUpdateInvalidAttributeValueEx() {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.startDate = "12.11.2019.";
        data.endDate = "12.11.2020.";
        data.value = (float)10;
        data.ticketDuration = "month";
        data.customerStatus = "student";

        ResponseEntity<TicketPrice> response = adminClient.exchange("/ticket-prices", HttpMethod.PUT, new HttpEntity<>(data), TicketPrice.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testDeleteSuccess() {
        long zoneId = zoneRepository.findByName("z-1").getId();
        String duration = "year";
        String customer = "student";

        ResponseEntity<TicketPrice> response = adminClient.exchange("/ticket-prices/{zoneId}/{duration}/{customer}", HttpMethod.DELETE, new HttpEntity<>(ticketPrice2), TicketPrice.class, zoneId, duration, customer);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testDeleteResourceNotFoundEx() {
        long zoneId = 123;
        String duration = "month";
        String customer = "student";

        ResponseEntity<TicketPrice> response = adminClient.exchange("/ticket-prices/{zoneId}/{duration}/{customer}", HttpMethod.DELETE, new HttpEntity<>(ticketPrice2), TicketPrice.class, zoneId, duration, customer);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
}

package com.metrolejbus.app.ticketing.controllers;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
    DailyTicketControllerIntegrationTest.class,
    DailyTicketControllerTest.class,
    PeriodicTicketControllerIntegrationTest.class,
    PeriodicTicketControllerTest.class,
    SingleUseTicketControllerIntegrationTest.class,
    SingleUseTicketControllerTest.class,
    TicketPriceControllerIntegrationTest.class,
    TicketPriceControllerTest.class
})
public class TicketingControllersTestSuite {}

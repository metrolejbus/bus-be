package com.metrolejbus.app.ticketing.controllers;

import com.metrolejbus.app.ticketing.forms.CreatePeriodicTicketForm;
import com.metrolejbus.app.ticketing.forms.UpdatePeriodicTicketForm;
import com.metrolejbus.app.ticketing.models.PeriodicTicket;
import com.metrolejbus.app.ticketing.models.TicketDuration;
import com.metrolejbus.app.ticketing.services.PeriodicTicketService;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Zone;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import javax.management.InvalidAttributeValueException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test,controller-test")
public class PeriodicTicketControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private TestRestTemplate customerClient;

    @MockBean
    private PeriodicTicketService periodicTicketServiceMocked;

    @PostConstruct
    public void postConstruct() {
        this.customerClient = this.customerClient.withBasicAuth("customer", "password");
    }

    @Test
    public void testFindByIdSuccess() throws InvalidAttributeValueException {
        long id = 1L;

        PeriodicTicket ticket = new PeriodicTicket(new Zone("zone 1"), TicketDuration.MONTH, LocalDate.parse("28.11.2019.", DateTimeFormatter.ofPattern("d.M.yyyy.")));
        Mockito.when(periodicTicketServiceMocked.findById(id)).thenReturn(ticket);

        ResponseEntity<PeriodicTicket> response = restTemplate.getForEntity("/periodic-tickets/{id}", PeriodicTicket.class, id);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testFindByIdResourceNotFoundEx() {
        long id = 1L;

        Mockito.when(periodicTicketServiceMocked.findById(id)).thenThrow(ResourceNotFoundException.class);

        ResponseEntity<PeriodicTicket> response = restTemplate.getForEntity("/periodic-tickets/{id}", PeriodicTicket.class, id);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testCreateSuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
        data.zoneId = 1L;
        data.validFrom = "25.11.2019.";
        data.duration = "month";

        PeriodicTicket ticket = new PeriodicTicket(new Zone("zone 1"), TicketDuration.MONTH, LocalDate.parse("25.11.2019.", DateTimeFormatter.ofPattern("d.M.yyyy.")));
        Mockito.when(periodicTicketServiceMocked.create(any(CreatePeriodicTicketForm.class))).thenReturn(ticket);

        ResponseEntity<PeriodicTicket> response = customerClient.exchange("/periodic-tickets", HttpMethod.POST, new HttpEntity<>(data), PeriodicTicket.class);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    public void testCreateResourceNotFoundEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
        data.zoneId = 1L;
        data.validFrom = "25.11.2019.";
        data.duration = "month";

        Mockito.when(periodicTicketServiceMocked.create(any(CreatePeriodicTicketForm.class))).thenThrow(ResourceNotFoundException.class);

        ResponseEntity<PeriodicTicket> response = customerClient.exchange("/periodic-tickets", HttpMethod.POST, new HttpEntity<>(data), PeriodicTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testCreateDateTimeParseEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
        data.zoneId = 1L;
        data.validFrom = "25.11.2019";
        data.duration = "month";

        Mockito.when(periodicTicketServiceMocked.create(any(CreatePeriodicTicketForm.class))).thenThrow(DateTimeParseException.class);

        ResponseEntity<PeriodicTicket> response = customerClient.exchange("/periodic-tickets", HttpMethod.POST, new HttpEntity<>(data), PeriodicTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testCreateInvalidAttributeValueEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
        data.zoneId = 1L;
        data.validFrom = "25.11.2017.";
        data.duration = "month";

        Mockito.when(periodicTicketServiceMocked.create(any(CreatePeriodicTicketForm.class))).thenThrow(InvalidAttributeValueException.class);

        ResponseEntity<PeriodicTicket> response = customerClient.exchange("/periodic-tickets", HttpMethod.POST, new HttpEntity<>(data), PeriodicTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testCreateIllegalArgumentEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
        data.zoneId = 1L;
        data.validFrom = "25.11.2017.";
        data.duration = "asd";

        Mockito.when(periodicTicketServiceMocked.create(any(CreatePeriodicTicketForm.class))).thenThrow(IllegalArgumentException.class);

        ResponseEntity<PeriodicTicket> response = customerClient.exchange("/periodic-tickets", HttpMethod.POST, new HttpEntity<>(data), PeriodicTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testUpdateSuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.duration = "month";
        data.validFrom = "12.11.2019.";
        data.zoneId = 1L;
        data.id = 1L;

        PeriodicTicket ticket = new PeriodicTicket();
        ticket.setValidFrom(LocalDate.parse(data.validFrom, DateTimeFormatter.ofPattern("d.M.yyyy.")));
        ticket.setDuration(TicketDuration.MONTH);
        Mockito.when(periodicTicketServiceMocked.update(any(UpdatePeriodicTicketForm.class))).thenReturn(ticket);

        ResponseEntity<PeriodicTicket> response = customerClient.exchange("/periodic-tickets", HttpMethod.PUT, new HttpEntity<>(ticket), PeriodicTicket.class);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testUpdateResourceNotFoundEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.duration = "month";
        data.validFrom = "12.11.2019.";
        data.zoneId = 1L;
        data.id = 1L;

        PeriodicTicket ticket = new PeriodicTicket();
        ticket.setValidFrom(LocalDate.parse(data.validFrom, DateTimeFormatter.ofPattern("d.M.yyyy.")));
        ticket.setDuration(TicketDuration.MONTH);
        Mockito.when(periodicTicketServiceMocked.update(any(UpdatePeriodicTicketForm.class))).thenThrow(ResourceNotFoundException.class);

        ResponseEntity<PeriodicTicket> response = customerClient.exchange("/periodic-tickets", HttpMethod.PUT, new HttpEntity<>(ticket), PeriodicTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testUpdateInvalidAttributeValueEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.duration = "month";
        data.validFrom = "12.11.2019.";
        data.zoneId = 1L;
        data.id = 1L;

        PeriodicTicket ticket = new PeriodicTicket();
        ticket.setValidFrom(LocalDate.parse(data.validFrom, DateTimeFormatter.ofPattern("d.M.yyyy.")));
        ticket.setDuration(TicketDuration.MONTH);
        Mockito.when(periodicTicketServiceMocked.update(any(UpdatePeriodicTicketForm.class))).thenThrow(InvalidAttributeValueException.class);

        ResponseEntity<PeriodicTicket> response = customerClient.exchange("/periodic-tickets", HttpMethod.PUT, new HttpEntity<>(ticket), PeriodicTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testDeleteSuccess() throws InvalidAttributeValueException {
        long id = 1L;

        PeriodicTicket ticket = new PeriodicTicket();
        ticket.setValidFrom(LocalDate.parse("12.11.2019.", DateTimeFormatter.ofPattern("d.M.yyyy.")));
        ticket.setDuration(TicketDuration.MONTH);
        Mockito.when(periodicTicketServiceMocked.deleteById(id)).thenReturn(ticket);

        ResponseEntity<PeriodicTicket> response = customerClient.exchange("/periodic-tickets/{id}", HttpMethod.DELETE, new HttpEntity<>(ticket), PeriodicTicket.class, id);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testDeleteResourceNotFoundEx() throws InvalidAttributeValueException {
        long id = 1L;

        PeriodicTicket ticket = new PeriodicTicket();
        ticket.setValidFrom(LocalDate.parse("12.11.2019.", DateTimeFormatter.ofPattern("d.M.yyyy.")));
        ticket.setDuration(TicketDuration.MONTH);
        Mockito.when(periodicTicketServiceMocked.deleteById(id)).thenThrow(ResourceNotFoundException.class);

        ResponseEntity<PeriodicTicket> response = customerClient.exchange("/periodic-tickets/{id}", HttpMethod.DELETE, new HttpEntity<>(ticket), PeriodicTicket.class, id);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

}

package com.metrolejbus.app.ticketing.controllers;

import com.metrolejbus.app.ClearDatabaseRule;
import com.metrolejbus.app.authentication.models.Role;
import com.metrolejbus.app.authentication.models.User;
import com.metrolejbus.app.authentication.repositories.UserRepository;
import com.metrolejbus.app.customers.models.Customer;
import com.metrolejbus.app.customers.models.CustomerStatus;
import com.metrolejbus.app.customers.repositories.CustomersRepository;
import com.metrolejbus.app.ticketing.forms.CreatePeriodicTicketForm;
import com.metrolejbus.app.ticketing.forms.UpdatePeriodicTicketForm;
import com.metrolejbus.app.ticketing.models.PeriodicTicket;
import com.metrolejbus.app.ticketing.models.TicketDuration;
import com.metrolejbus.app.ticketing.repositories.PeriodicTicketRepository;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.repositories.ZoneRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import javax.management.InvalidAttributeValueException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test,controller-test")
public class PeriodicTicketControllerIntegrationTest {

    @Rule
    @Autowired
    public ClearDatabaseRule clearDatabaseRule;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private TestRestTemplate customerClient;

    @Autowired
    private PeriodicTicketRepository periodicTicketRepository;

    @Autowired
    private ZoneRepository zoneRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomersRepository customersRepository;

    private static PeriodicTicket periodicTicket;
    private static PeriodicTicket periodicTicket2;

    @PostConstruct
    public void postConstruct() {
        this.customerClient = this.customerClient.withBasicAuth("customer", "password");
    }

    @Before
    public void setUp() throws InvalidAttributeValueException {
        Zone zone = new Zone("zone_1");
        zone.setActive(true);
        zoneRepository.save(zone);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
        periodicTicket = new PeriodicTicket(zone, TicketDuration.MONTH, LocalDate.parse("28.11.2019.", formatter));
        periodicTicket.setActive(true);
        periodicTicketRepository.save(periodicTicket);

        periodicTicket2 = new PeriodicTicket(zone, TicketDuration.MONTH, LocalDate.parse("28.11.2019.", formatter));
        periodicTicket2.setActive(true);
        periodicTicketRepository.save(periodicTicket2);
    }

    @Test
    public void testFindByIdSuccess() {
        long id = periodicTicket.getId();

        ResponseEntity<PeriodicTicket> response = restTemplate.getForEntity("/periodic-tickets/{id}", PeriodicTicket.class, id);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testFindByIdResourceNotFoundEx() {
        long id = 123;

        ResponseEntity<PeriodicTicket> response = restTemplate.getForEntity("/periodic-tickets/{id}", PeriodicTicket.class, id);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testCreateSuccess() throws InvalidAttributeValueException {
        User user = new User();
        user.setUsername("customer");
        user.setPassword("password");
        user.setRole(Role.CUSTOMER);

        Customer customer = new Customer();
        customer.setFirstName("firstName");
        customer.setMiddleName("middleName");
        customer.setLastName("lastName");
        customer.setAge(22);
        customer.setStatus(CustomerStatus.STUDENT);
        customer.setUser(user);
        customersRepository.save(customer);

        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
        data.zoneId = zoneRepository.findByName("zone_1").getId();
        data.validFrom = "21.11.2019.";
        data.duration = "month";

        ResponseEntity<PeriodicTicket> response = customerClient.exchange("/periodic-tickets", HttpMethod.POST, new HttpEntity<>(data), PeriodicTicket.class);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    public void testCreateResourceNotFoundEx() {
        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
        data.zoneId = 123L;
        data.validFrom = "25.11.2019.";
        data.duration = "month";

        ResponseEntity<PeriodicTicket> response = customerClient.exchange("/periodic-tickets", HttpMethod.POST, new HttpEntity<>(data), PeriodicTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test(expected = Exception.class)
    public void testCreateDateTimeParseEx() {
        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
        data.zoneId = zoneRepository.findByName("zone_1").getId();
        data.validFrom = "25.11.2019";
        data.duration = "month";

        ResponseEntity<PeriodicTicket> response = customerClient.exchange("/periodic-tickets", HttpMethod.POST, new HttpEntity<>(data), PeriodicTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test(expected = Exception.class)
    public void testCreateInvalidAttributeValueEx() {
        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
        data.zoneId = zoneRepository.findByName("zone_1").getId();
        data.validFrom = "25.11.2017.";
        data.duration = "month";

        ResponseEntity<PeriodicTicket> response = customerClient.exchange("/periodic-tickets", HttpMethod.POST, new HttpEntity<>(data), PeriodicTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testCreateIllegalArgumentEx() {
        CreatePeriodicTicketForm data = new CreatePeriodicTicketForm();
        data.zoneId = zoneRepository.findByName("zone_1").getId();
        data.validFrom = "25.11.2017.";
        data.duration = "asd";

        ResponseEntity<PeriodicTicket> response = customerClient.exchange("/periodic-tickets", HttpMethod.POST, new HttpEntity<>(data), PeriodicTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testUpdateSuccess() {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.duration = "month";
        data.validFrom = "12.11.2019.";
        data.zoneId = zoneRepository.findByName("zone_1").getId();
        data.id = periodicTicket.getId();

        ResponseEntity<PeriodicTicket> response = customerClient.exchange("/periodic-tickets", HttpMethod.PUT, new HttpEntity<>(data), PeriodicTicket.class);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testUpdateResourceNotFoundEx() {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.duration = "month";
        data.validFrom = "12.11.2019.";
        data.zoneId = zoneRepository.findByName("zone_1").getId();
        data.id = 123;

        ResponseEntity<PeriodicTicket> response = customerClient.exchange("/periodic-tickets", HttpMethod.PUT, new HttpEntity<>(data), PeriodicTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testUpdateInvalidAttributeValueEx() {
        UpdatePeriodicTicketForm data = new UpdatePeriodicTicketForm();
        data.duration = "day";
        data.validFrom = "12.11.2019.";
        data.zoneId = zoneRepository.findByName("zone_1").getId();
        data.id = periodicTicket.getId();

        ResponseEntity<PeriodicTicket> response = customerClient.exchange("/periodic-tickets", HttpMethod.PUT, new HttpEntity<>(data), PeriodicTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testDeleteSuccess() {
        long id = periodicTicket2.getId();

        ResponseEntity<PeriodicTicket> response = customerClient.exchange("/periodic-tickets/{id}", HttpMethod.DELETE, new HttpEntity<>(id), PeriodicTicket.class, id);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testDeleteResourceNotFoundEx() {
        long id = 123;

        ResponseEntity<PeriodicTicket> response = customerClient.exchange("/periodic-tickets/{id}", HttpMethod.DELETE, new HttpEntity<>(id), PeriodicTicket.class, id);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

}

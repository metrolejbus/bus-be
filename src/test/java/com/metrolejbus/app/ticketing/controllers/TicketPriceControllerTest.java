package com.metrolejbus.app.ticketing.controllers;

import com.metrolejbus.app.common.exceptions.NotFound;
import com.metrolejbus.app.common.models.ValidityPeriod;
import com.metrolejbus.app.customers.models.CustomerStatus;
import com.metrolejbus.app.ticketing.forms.CreateTicketPriceForm;
import com.metrolejbus.app.ticketing.forms.UpdateTicketPriceForm;
import com.metrolejbus.app.ticketing.models.TicketDuration;
import com.metrolejbus.app.ticketing.models.TicketPrice;
import com.metrolejbus.app.ticketing.services.TicketPriceService;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Zone;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import javax.management.InvalidAttributeValueException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test,controller-test")
public class TicketPriceControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private TestRestTemplate adminClient;

    @MockBean
    private TicketPriceService ticketPriceServiceMocked;

    @PostConstruct
    public void postConstruct() {
        this.adminClient = this.adminClient.withBasicAuth("admin", "password");
    }

    @Test
    public void testFindByIdSuccess()
        throws ZoneNotFound, InvalidAttributeValueException
    {
        long zoneId = 1L;
        String duration = "month";
        String customer = "student";

        TicketPrice ticketPrice = new TicketPrice(CustomerStatus.STUDENT, TicketDuration.MONTH, new Zone("zone 1"));
        ticketPrice.setValidityPeriod(new ValidityPeriod(LocalDate.of(2019, 12, 12), null));
        Mockito.when(ticketPriceServiceMocked.findById(zoneId, duration, customer)).thenReturn(ticketPrice);

        ResponseEntity<TicketPrice> response = restTemplate.getForEntity("/ticket-prices/{zoneId}/{duration}/{customer}", TicketPrice.class, zoneId, duration, customer);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testFindByIdResourceNotFoundEx() throws ZoneNotFound {
        long zoneId = 1L;
        String duration = "month";
        String customer = "student";

        Mockito.when(ticketPriceServiceMocked.findById(zoneId, duration, customer)).thenThrow(ResourceNotFoundException.class);

        ResponseEntity<TicketPrice> response = restTemplate.getForEntity("/ticket-prices/{zoneId}/{duration}/{customer}", TicketPrice.class, zoneId, duration, customer);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testCreateSuccess()
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.zoneId = 1L;
        data.value = (float)10;
        data.ticketDuration = "month";
        data.startDate = "12.11.2019.";
        data.endDate = "12.11.2020.";
        data.customerStatus = "student";

        TicketPrice ticketPrice = new TicketPrice(
                CustomerStatus.STUDENT,
                TicketDuration.MONTH,
                new Zone("zone 1"));
        ticketPrice.setValidityPeriod(new ValidityPeriod(LocalDate.of(2019, 12, 12), null));
        Mockito.when(ticketPriceServiceMocked.create(any(CreateTicketPriceForm.class))).thenReturn(ticketPrice);

        ResponseEntity<TicketPrice> response = adminClient.exchange("/ticket-prices", HttpMethod.POST, new HttpEntity<>(data), TicketPrice.class);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    public void testCreateResourceNotFoundEx()
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.zoneId = 1L;
        data.value = (float)10;
        data.ticketDuration = "month";
        data.startDate = "12.11.2019.";
        data.endDate = "12.11.2020.";
        data.customerStatus = "student";

        Mockito.when(ticketPriceServiceMocked.create(any(CreateTicketPriceForm.class))).thenThrow(ResourceNotFoundException.class);

        ResponseEntity<TicketPrice> response = adminClient.exchange("/ticket-prices", HttpMethod.POST, new HttpEntity<>(data), TicketPrice.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testCreateDateTimeParseEx()
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.zoneId = 1L;
        data.value = (float)10;
        data.ticketDuration = "month";
        data.startDate = "12.11.2019";
        data.endDate = "12.11.2020.";
        data.customerStatus = "student";

        Mockito.when(ticketPriceServiceMocked.create(any(CreateTicketPriceForm.class))).thenThrow(DateTimeParseException.class);

        ResponseEntity<TicketPrice> response = adminClient.exchange("/ticket-prices", HttpMethod.POST, new HttpEntity<>(data), TicketPrice.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testCreateInvalidAttributeValueEx()
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.zoneId = 1L;
        data.value = (float)10;
        data.ticketDuration = "month";
        data.startDate = "12.11.2017.";
        data.endDate = "12.11.2020.";
        data.customerStatus = "student";

        Mockito.when(ticketPriceServiceMocked.create(any(CreateTicketPriceForm.class))).thenThrow(InvalidAttributeValueException.class);

        ResponseEntity<TicketPrice> response = adminClient.exchange("/ticket-prices", HttpMethod.POST, new HttpEntity<>(data), TicketPrice.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testCreateIllegalArgumentEx()
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        CreateTicketPriceForm data = new CreateTicketPriceForm();
        data.zoneId = 1L;
        data.value = (float)10;
        data.ticketDuration = "asd";
        data.startDate = "12.11.2019.";
        data.endDate = "12.11.2020.";
        data.customerStatus = "student";

        Mockito.when(ticketPriceServiceMocked.create(any(CreateTicketPriceForm.class))).thenThrow(IllegalArgumentException.class);

        ResponseEntity<TicketPrice> response = adminClient.exchange("/ticket-prices", HttpMethod.POST, new HttpEntity<>(data), TicketPrice.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testUpdateSuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.zoneId = 1L;
        data.startDate = "12.11.2019.";
        data.endDate = "12.11.2020.";
        data.value = (float)10;
        data.ticketDuration = "month";
        data.customerStatus = "student";

        Zone zone = new Zone("zone 1");
        zone.setId(1L);
        TicketPrice ticketPrice = new TicketPrice(
                CustomerStatus.STUDENT,
                TicketDuration.MONTH,
                zone);
        ticketPrice.setValue(10);
        ticketPrice.setActive(true);
        ticketPrice.setValidityPeriod(new ValidityPeriod(LocalDate.of(2019, 12, 12), null));

        Mockito.when(ticketPriceServiceMocked.update(any(UpdateTicketPriceForm.class))).thenReturn(ticketPrice);

        ResponseEntity<TicketPrice> response = adminClient.exchange("/ticket-prices", HttpMethod.PUT, new HttpEntity<>(ticketPrice), TicketPrice.class);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testUpdateResourceNotFoundEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.zoneId = 1L;
        data.startDate = "12.11.2019.";
        data.endDate = "12.11.2020.";
        data.value = (float)10;
        data.ticketDuration = "month";
        data.customerStatus = "student";

        Zone zone = new Zone("zone 1");
        zone.setId(1L);
        TicketPrice ticketPrice = new TicketPrice(
                CustomerStatus.STUDENT,
                TicketDuration.MONTH,
                zone);
        ticketPrice.setValue(10);
        ticketPrice.setActive(true);

        Mockito.when(ticketPriceServiceMocked.update(any(UpdateTicketPriceForm.class))).thenThrow(ResourceNotFoundException.class);

        ResponseEntity<TicketPrice> response = adminClient.exchange("/ticket-prices", HttpMethod.PUT, new HttpEntity<>(ticketPrice), TicketPrice.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testUpdateInvalidAttributeValueEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateTicketPriceForm data = new UpdateTicketPriceForm();
        data.zoneId = 1L;
        data.startDate = "12.11.2019.";
        data.endDate = "12.11.2020.";
        data.value = (float)10;
        data.ticketDuration = "month";
        data.customerStatus = "student";

        Zone zone = new Zone("zone 1");
        zone.setId(1L);
        TicketPrice ticketPrice = new TicketPrice(
                CustomerStatus.STUDENT,
                TicketDuration.MONTH,
                zone);
        ticketPrice.setValue(10);
        ticketPrice.setActive(true);

        Mockito.when(ticketPriceServiceMocked.update(any(UpdateTicketPriceForm.class))).thenThrow(InvalidAttributeValueException.class);

        ResponseEntity<TicketPrice> response = adminClient.exchange("/ticket-prices", HttpMethod.PUT, new HttpEntity<>(ticketPrice), TicketPrice.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testDeleteSuccess()
        throws ZoneNotFound, InvalidAttributeValueException
    {
        long zoneId = 1L;
        String duration = "month";
        String customer = "student";

        TicketPrice ticketPrice = new TicketPrice(
                CustomerStatus.valueOf(customer.toUpperCase()),
                TicketDuration.valueOf(duration.toUpperCase()),
                new Zone("zone 1")
        );
        ticketPrice.setValidityPeriod(new ValidityPeriod(LocalDate.of(2019, 12, 12), null));

        Mockito.when(ticketPriceServiceMocked.deleteById(zoneId, duration, customer)).thenReturn(ticketPrice);

        ResponseEntity<TicketPrice> response = adminClient.exchange("/ticket-prices/{zoneId}/{duration}/{customer}", HttpMethod.DELETE, new HttpEntity<>(ticketPrice), TicketPrice.class, zoneId, duration, customer);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testDeleteResourceNotFoundEx() throws ZoneNotFound {
        long zoneId = 1L;
        String duration = "month";
        String customer = "student";

        TicketPrice ticketPrice = new TicketPrice(
                CustomerStatus.valueOf(customer.toUpperCase()),
                TicketDuration.valueOf(duration.toUpperCase()),
                new Zone("zone 1")
        );

        Mockito.when(ticketPriceServiceMocked.deleteById(zoneId, duration, customer)).thenThrow(ResourceNotFoundException.class);

        ResponseEntity<TicketPrice> response = adminClient.exchange("/ticket-prices/{zoneId}/{duration}/{customer}", HttpMethod.DELETE, new HttpEntity<>(ticketPrice), TicketPrice.class, zoneId, duration, customer);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
}

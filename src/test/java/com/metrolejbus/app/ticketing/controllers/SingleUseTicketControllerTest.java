package com.metrolejbus.app.ticketing.controllers;

import com.metrolejbus.app.ticketing.forms.CreateSingleUseTicketForm;
import com.metrolejbus.app.ticketing.forms.UpdateSingleUseTicketForm;
import com.metrolejbus.app.ticketing.models.SingleUseTicket;
import com.metrolejbus.app.ticketing.services.SingleUseTicketService;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Zone;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test,controller-test")
public class SingleUseTicketControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private TestRestTemplate customerClient;

    @MockBean
    private SingleUseTicketService singleUseTicketServiceMocked;

    @PostConstruct
    public void postConstruct() {
        this.customerClient = this.customerClient.withBasicAuth("customer", "password");
    }

    @Test
    public void testFindByIdSuccess() {
        long id = 1L;

        SingleUseTicket ticket = new SingleUseTicket(new Zone("zone 1"));
        Mockito.when(singleUseTicketServiceMocked.findById(id)).thenReturn(ticket);

        ResponseEntity<SingleUseTicket> response = restTemplate.getForEntity("/single-use-tickets/{id}", SingleUseTicket.class, id);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testFindByIdResourceNotFoundEx() {
        long id = 1L;

        Mockito.when(singleUseTicketServiceMocked.findById(id)).thenThrow(ResourceNotFoundException.class);

        ResponseEntity<SingleUseTicket> response = restTemplate.getForEntity("/single-use-tickets/{id}", SingleUseTicket.class, id);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testCreateSuccess() throws ZoneNotFound {
        CreateSingleUseTicketForm data = new CreateSingleUseTicketForm();
        data.zoneId = 1L;

        SingleUseTicket ticket = new SingleUseTicket(new Zone("zone 1"));
        Mockito.when(singleUseTicketServiceMocked.create(any(CreateSingleUseTicketForm.class))).thenReturn(ticket);

        ResponseEntity<SingleUseTicket> response = customerClient.exchange("/single-use-tickets", HttpMethod.POST, new HttpEntity<>(data), SingleUseTicket.class);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    public void testCreateResourceNotFoundEx() throws ZoneNotFound {
        CreateSingleUseTicketForm data = new CreateSingleUseTicketForm();
        data.zoneId = 1L;

        Mockito.when(singleUseTicketServiceMocked.create(any(CreateSingleUseTicketForm.class))).thenThrow(ResourceNotFoundException.class);

        ResponseEntity<SingleUseTicket> response = customerClient.exchange("/single-use-tickets", HttpMethod.POST, new HttpEntity<>(data), SingleUseTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testUpdateSuccess() throws ZoneNotFound {
        UpdateSingleUseTicketForm data = new UpdateSingleUseTicketForm();
        data.zoneId = 1L;
        data.id = 1L;

        SingleUseTicket ticket = new SingleUseTicket();
        ticket.setId(1L);
        Mockito.when(singleUseTicketServiceMocked.update(any(UpdateSingleUseTicketForm.class))).thenReturn(ticket);

        ResponseEntity<SingleUseTicket> response = customerClient.exchange("/single-use-tickets", HttpMethod.PUT, new HttpEntity<>(ticket), SingleUseTicket.class);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testUpdateResourceNotFoundEx() throws ZoneNotFound {
        UpdateSingleUseTicketForm data = new UpdateSingleUseTicketForm();
        data.zoneId = 1L;
        data.id = 1L;

        SingleUseTicket ticket = new SingleUseTicket();
        ticket.setId(1L);
        Mockito.when(singleUseTicketServiceMocked.update(any(UpdateSingleUseTicketForm.class))).thenThrow(ResourceNotFoundException.class);

        ResponseEntity<SingleUseTicket> response = customerClient.exchange("/single-use-tickets", HttpMethod.PUT, new HttpEntity<>(ticket), SingleUseTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testDeleteSuccess() {
        long id = 1L;

        SingleUseTicket ticket = new SingleUseTicket();
        ticket.setId(1L);
        Mockito.when(singleUseTicketServiceMocked.deleteById(id)).thenReturn(ticket);

        ResponseEntity<SingleUseTicket> response = customerClient.exchange("/single-use-tickets/{id}", HttpMethod.DELETE, new HttpEntity<>(ticket), SingleUseTicket.class, id);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testDeleteResourceNotFoundEx() {
        long id = 1L;

        SingleUseTicket ticket = new SingleUseTicket();
        ticket.setId(1L);
        Mockito.when(singleUseTicketServiceMocked.deleteById(id)).thenThrow(ResourceNotFoundException.class);

        ResponseEntity<SingleUseTicket> response = customerClient.exchange("/single-use-tickets/{id}", HttpMethod.DELETE, new HttpEntity<>(ticket), SingleUseTicket.class, id);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
}

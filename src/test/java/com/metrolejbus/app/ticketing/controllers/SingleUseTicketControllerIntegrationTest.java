package com.metrolejbus.app.ticketing.controllers;

import com.metrolejbus.app.ClearDatabaseRule;
import com.metrolejbus.app.authentication.models.Role;
import com.metrolejbus.app.authentication.models.User;
import com.metrolejbus.app.authentication.repositories.UserRepository;
import com.metrolejbus.app.customers.models.Customer;
import com.metrolejbus.app.customers.models.CustomerStatus;
import com.metrolejbus.app.customers.repositories.CustomersRepository;
import com.metrolejbus.app.ticketing.forms.CreateSingleUseTicketForm;
import com.metrolejbus.app.ticketing.forms.UpdateSingleUseTicketForm;
import com.metrolejbus.app.ticketing.models.SingleUseTicket;
import com.metrolejbus.app.ticketing.repositories.SingleUseTicketRepository;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.repositories.ZoneRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import javax.management.InvalidAttributeValueException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test,controller-test")
public class SingleUseTicketControllerIntegrationTest {

    @Rule
    @Autowired
    public ClearDatabaseRule clearDatabaseRule;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private TestRestTemplate customerClient;

    @Autowired
    private SingleUseTicketRepository periodicTicketRepository;

    @Autowired
    private ZoneRepository zoneRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomersRepository customersRepository;

    private static SingleUseTicket singleUseTicket;
    private static SingleUseTicket singleUseTicket2;


    @PostConstruct
    public void postConstruct() {
        this.customerClient = this.customerClient.withBasicAuth("customer", "password");
    }

    @Before
    public void setUp() {
        Zone zone = new Zone("zone.1");
        zone.setActive(true);
        zoneRepository.save(zone);

        singleUseTicket = new SingleUseTicket(zone);
        singleUseTicket.setActive(true);
        periodicTicketRepository.save(singleUseTicket);

        singleUseTicket2 = new SingleUseTicket(zone);
        singleUseTicket2.setActive(true);
        periodicTicketRepository.save(singleUseTicket2);
    }

    @Test
    public void testFindByIdSuccess() {
        long id = singleUseTicket.getId();

        ResponseEntity<SingleUseTicket> response = restTemplate.getForEntity("/single-use-tickets/{id}", SingleUseTicket.class, id);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testFindByIdResourceNotFoundEx() {
        long id = 123;

        ResponseEntity<SingleUseTicket> response = restTemplate.getForEntity("/single-use-tickets/{id}", SingleUseTicket.class, id);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testCreateSuccess() throws InvalidAttributeValueException {
        User user = new User();
        user.setUsername("customer");
        user.setPassword("password");
        user.setRole(Role.CUSTOMER);

        Customer customer = new Customer();
        customer.setFirstName("firstName");
        customer.setMiddleName("middleName");
        customer.setLastName("lastName");
        customer.setAge(22);
        customer.setStatus(CustomerStatus.STUDENT);
        customer.setUser(user);
        customersRepository.save(customer);

        CreateSingleUseTicketForm data = new CreateSingleUseTicketForm();
        data.zoneId = zoneRepository.findByName("zone.1").getId();

        ResponseEntity<SingleUseTicket> response = customerClient.exchange("/single-use-tickets",  HttpMethod.POST, new HttpEntity<>(data), SingleUseTicket.class);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
    }

    @Test
    public void testCreateResourceNotFoundEx() {
        CreateSingleUseTicketForm data = new CreateSingleUseTicketForm();
        data.zoneId = 123L;

        ResponseEntity<SingleUseTicket> response = customerClient.exchange("/single-use-tickets",  HttpMethod.POST, new HttpEntity<>(data), SingleUseTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testUpdateSuccess() {
        UpdateSingleUseTicketForm data = new UpdateSingleUseTicketForm();
        data.zoneId = zoneRepository.findByName("zone.1").getId();
        data.id = singleUseTicket.getId();

        ResponseEntity<SingleUseTicket> response = customerClient.exchange("/single-use-tickets", HttpMethod.PUT, new HttpEntity<>(data), SingleUseTicket.class);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testUpdateResourceNotFoundEx() {
        UpdateSingleUseTicketForm data = new UpdateSingleUseTicketForm();
        data.zoneId = zoneRepository.findByName("zone.1").getId();
        data.id = 123L;

        ResponseEntity<SingleUseTicket> response = customerClient.exchange("/single-use-tickets", HttpMethod.PUT, new HttpEntity<>(data), SingleUseTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testDeleteSuccess() {
        long id = singleUseTicket2.getId();

        ResponseEntity<SingleUseTicket> response = customerClient.exchange("/single-use-tickets/{id}", HttpMethod.DELETE, new HttpEntity<>(id), SingleUseTicket.class, id);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testDeleteResourceNotFoundEx() {
        long id = 123;

        ResponseEntity<SingleUseTicket> response = customerClient.exchange("/single-use-tickets/{id}", HttpMethod.DELETE, new HttpEntity<>(id), SingleUseTicket.class, id);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
}

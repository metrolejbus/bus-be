package com.metrolejbus.app.ticketing.controllers;

import com.metrolejbus.app.ticketing.forms.CreateDailyTicketForm;
import com.metrolejbus.app.ticketing.forms.UpdateDailyTicketForm;
import com.metrolejbus.app.ticketing.models.DailyTicket;
import com.metrolejbus.app.ticketing.services.DailyTicketService;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Zone;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import javax.management.InvalidAttributeValueException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test,controller-test")
public class DailyTicketControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private TestRestTemplate customerClient;

    @MockBean
    private DailyTicketService dailyTicketServiceMocked;

    @PostConstruct
    public void postConstruct() {
        this.customerClient = this.customerClient.withBasicAuth("customer", "password");
    }

    @Test
    public void testFindByIdSuccess() throws InvalidAttributeValueException {
        long id = 1L;

        DailyTicket dailyTicket = new DailyTicket(new Zone("zone 1"), LocalDate.parse("28.12.2019.", DateTimeFormatter.ofPattern("d.M.yyyy.")));
        Mockito.when(dailyTicketServiceMocked.findById(id)).thenReturn(dailyTicket);

        ResponseEntity<DailyTicket> response = restTemplate.getForEntity("/daily-tickets/{id}", DailyTicket.class, id);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testFindByIdResourceNotFoundEx() {
        long id = 1L;

        Mockito.when(dailyTicketServiceMocked.findById(id)).thenThrow(ResourceNotFoundException.class);

        ResponseEntity<DailyTicket> response = restTemplate.getForEntity("/daily-tickets/{id}", DailyTicket.class, id);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testCreateSuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreateDailyTicketForm data = new CreateDailyTicketForm();
        data.zoneId = 1L;
        data.date = "25.12.2019.";

        Zone zone = new Zone("zone 1");
        zone.setId(data.zoneId);

        DailyTicket ticket = new DailyTicket();
        ticket.setId(1L);
        ticket.setZone(zone);
        ticket.setDate(LocalDate.parse(data.date, DateTimeFormatter.ofPattern("d.M.yyyy.")));
        ticket.setActive(true);

        Mockito.when(dailyTicketServiceMocked.create(any(CreateDailyTicketForm.class))).thenReturn(ticket);

        ResponseEntity<DailyTicket> response = customerClient.exchange("/daily-tickets", HttpMethod.POST, new HttpEntity<>(data), DailyTicket.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void testCreateDateTimeParseEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreateDailyTicketForm data = new CreateDailyTicketForm();
        data.zoneId = 1L;
        data.date = "12.12.2019";

        Mockito.when(dailyTicketServiceMocked.create(any(CreateDailyTicketForm.class))).thenThrow(DateTimeParseException.class);

        ResponseEntity<DailyTicket> response = customerClient.exchange("/daily-tickets", HttpMethod.POST, new HttpEntity<>(data), DailyTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testCreateInvalidAttributeValueEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreateDailyTicketForm data = new CreateDailyTicketForm();
        data.zoneId = 1L;
        data.date = "12.12.2017.";

        Mockito.when(dailyTicketServiceMocked.create(any(CreateDailyTicketForm.class))).thenThrow(InvalidAttributeValueException.class);

        ResponseEntity<DailyTicket> response = customerClient.exchange("/daily-tickets", HttpMethod.POST, new HttpEntity<>(data), DailyTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testCreateResourceNotFoundEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        CreateDailyTicketForm data = new CreateDailyTicketForm();
        data.zoneId = 1L;
        data.date = "12.12.2019.";

        Mockito.when(dailyTicketServiceMocked.create(any(CreateDailyTicketForm.class))).thenThrow(ResourceNotFoundException.class);

        ResponseEntity<DailyTicket> response = customerClient.exchange("/daily-tickets", HttpMethod.POST, new HttpEntity<>(data), DailyTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testUpdateSuccess()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateDailyTicketForm data = new UpdateDailyTicketForm();
        data.date = "12.11.2019.";
        data.id = 1L;
        data.zoneId = 1L;

        DailyTicket ticket = new DailyTicket();
        ticket.setDate(LocalDate.of(2019, 11, 12));
        Mockito.when(dailyTicketServiceMocked.update(any(UpdateDailyTicketForm.class))).thenReturn(ticket);

        ResponseEntity<DailyTicket> response = customerClient.exchange("/daily-tickets", HttpMethod.PUT, new HttpEntity<>(ticket), DailyTicket.class);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testUpdateResourceNotFoundEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateDailyTicketForm data = new UpdateDailyTicketForm();
        data.date = "12.11.2019.";
        data.id = 1L;
        data.zoneId = 1L;

        DailyTicket ticket = new DailyTicket();
        Mockito.when(dailyTicketServiceMocked.update(any(UpdateDailyTicketForm.class))).thenThrow(ResourceNotFoundException.class);

        ResponseEntity<DailyTicket> response = customerClient.exchange("/daily-tickets", HttpMethod.PUT, new HttpEntity<>(ticket), DailyTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testUpdateInvalidAttributeValueEx()
        throws InvalidAttributeValueException, ZoneNotFound
    {
        UpdateDailyTicketForm data = new UpdateDailyTicketForm();
        data.date = "12.11.2017.";
        data.id = 1L;
        data.zoneId = 1L;

        DailyTicket ticket = new DailyTicket();
        Mockito.when(dailyTicketServiceMocked.update(any(UpdateDailyTicketForm.class))).thenThrow(InvalidAttributeValueException.class);

        ResponseEntity<DailyTicket> response = customerClient.exchange("/daily-tickets", HttpMethod.PUT, new HttpEntity<>(ticket), DailyTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testDeleteSuccess() {
        long id = 1L;

        DailyTicket ticket = new DailyTicket();
        ticket.setDate(LocalDate.of(2019, 12, 12));
        Mockito.when(dailyTicketServiceMocked.deleteById(id)).thenReturn(ticket);

        ResponseEntity<DailyTicket> response = customerClient.exchange("/daily-tickets/{id}", HttpMethod.DELETE, new HttpEntity<>(ticket), DailyTicket.class, id);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testDeleteResourceNotFoundEx() {
        long id = 1L;

        DailyTicket ticket = new DailyTicket();
        Mockito.when(dailyTicketServiceMocked.deleteById(id)).thenThrow(ResourceNotFoundException.class);

        ResponseEntity<DailyTicket> response = customerClient.exchange("/daily-tickets/{id}", HttpMethod.DELETE, new HttpEntity<>(ticket), DailyTicket.class, id);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
}

package com.metrolejbus.app.ticketing.controllers;

import com.metrolejbus.app.ClearDatabaseRule;
import com.metrolejbus.app.authentication.models.Role;
import com.metrolejbus.app.authentication.models.User;
import com.metrolejbus.app.authentication.repositories.UserRepository;
import com.metrolejbus.app.customers.models.Customer;
import com.metrolejbus.app.customers.models.CustomerStatus;
import com.metrolejbus.app.customers.repositories.CustomersRepository;
import com.metrolejbus.app.ticketing.forms.CreateDailyTicketForm;
import com.metrolejbus.app.ticketing.forms.UpdateDailyTicketForm;
import com.metrolejbus.app.ticketing.models.DailyTicket;
import com.metrolejbus.app.ticketing.repositories.DailyTicketRepository;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.repositories.ZoneRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import javax.management.InvalidAttributeValueException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test,controller-test")
public class DailyTicketControllerIntegrationTest {

    @Rule
    @Autowired
    public ClearDatabaseRule clearDatabaseRule;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private TestRestTemplate customerClient;

    @Autowired
    private ZoneRepository zoneRepository;

    @Autowired
    private DailyTicketRepository dailyTicketRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CustomersRepository customersRepository;

    private static DailyTicket dailyTicket;
    private static DailyTicket dailyTicket2;

    @PostConstruct
    public void postConstruct() {
        this.customerClient = this.customerClient.withBasicAuth("customer", "password");
    }

    @Before
    public void setUp() throws InvalidAttributeValueException {
        Zone zone = new Zone("zone-1");
        zone.setActive(true);
        zoneRepository.save(zone);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
        dailyTicket = new DailyTicket(zone, LocalDate.parse("28.12.2019.", formatter));
        dailyTicket.setActive(true);
        dailyTicketRepository.save(dailyTicket);

        dailyTicket2 = new DailyTicket(zone, LocalDate.parse("28.12.2019.", formatter));
        dailyTicket2.setActive(true);
        dailyTicketRepository.save(dailyTicket2);
    }

    @Test
    public void testFindByIdSuccess() {
        long id = dailyTicket.getId();

        ResponseEntity<DailyTicket> response = restTemplate.getForEntity("/daily-tickets/{id}", DailyTicket.class, id);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testFindByIdResourceNotFoundEx() {
        long id = 123;

        ResponseEntity<DailyTicket> response = restTemplate.getForEntity("/daily-tickets/{id}", DailyTicket.class, id);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testCreateSuccess() throws InvalidAttributeValueException {
        User user = new User();
        user.setUsername("customer");
        user.setPassword("password");
        user.setRole(Role.CUSTOMER);

        Customer customer = new Customer();
        customer.setFirstName("firstName");
        customer.setMiddleName("middleName");
        customer.setLastName("lastName");
        customer.setAge(22);
        customer.setStatus(CustomerStatus.STUDENT);
        customer.setUser(user);
        customersRepository.save(customer);

        CreateDailyTicketForm data = new CreateDailyTicketForm();
        data.zoneId = zoneRepository.findByName("zone-1").getId();
        data.date = "25.12.2019.";

        ResponseEntity<DailyTicket> response = customerClient.exchange("/daily-tickets", HttpMethod.POST, new HttpEntity<>(data), DailyTicket.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void testCreateDateTimeParseEx() {
        CreateDailyTicketForm data = new CreateDailyTicketForm();
        data.zoneId = zoneRepository.findByName("zone-1").getId();
        data.date = "12.12.2019";

        ResponseEntity<DailyTicket> response = customerClient.exchange("/daily-tickets", HttpMethod.POST, new HttpEntity<>(data), DailyTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testCreateInvalidAttributeValueEx() {
        CreateDailyTicketForm data = new CreateDailyTicketForm();
        data.zoneId = zoneRepository.findByName("zone-1").getId();
        data.date = "12.12.2017.";

        ResponseEntity<DailyTicket> response = customerClient.exchange("/daily-tickets", HttpMethod.POST, new HttpEntity<>(data), DailyTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testCreateResourceNotFoundEx() {
        CreateDailyTicketForm data = new CreateDailyTicketForm();
        data.zoneId = 123L;
        data.date = "12.12.2019.";

        ResponseEntity<DailyTicket> response = customerClient.exchange("/daily-tickets", HttpMethod.POST, new HttpEntity<>(data), DailyTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testUpdateSuccess() {
        UpdateDailyTicketForm data = new UpdateDailyTicketForm();
        data.date = "12.11.2019.";
        data.id = dailyTicket.getId();
        data.zoneId = zoneRepository.findByName("zone-1").getId();

        ResponseEntity<DailyTicket> response = customerClient.exchange("/daily-tickets", HttpMethod.PUT, new HttpEntity<>(data), DailyTicket.class);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testUpdateResourceNotFoundEx() {
        UpdateDailyTicketForm data = new UpdateDailyTicketForm();
        data.date = "12.11.2019.";
        data.id = 123;
        data.zoneId = zoneRepository.findByName("zone-1").getId();

        ResponseEntity<DailyTicket> response = customerClient.exchange("/daily-tickets", HttpMethod.PUT, new HttpEntity<>(data), DailyTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    public void testUpdateInvalidAttributeValueEx() {
        UpdateDailyTicketForm data = new UpdateDailyTicketForm();
        data.date = "12.11.2017.";
        data.id = dailyTicket.getId();
        data.zoneId = zoneRepository.findByName("zone-1").getId();

        ResponseEntity<DailyTicket> response = customerClient.exchange("/daily-tickets", HttpMethod.PUT, new HttpEntity<>(data), DailyTicket.class);
        assertNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
    }

    @Test
    public void testDeleteSuccess() {
        long id = dailyTicket2.getId();

        ResponseEntity<DailyTicket> response = customerClient.exchange("/daily-tickets/{id}", HttpMethod.DELETE, new HttpEntity<>(id), DailyTicket.class, id);
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testDeleteResourceNotFoundEx() {
        long id = 123L;

        ResponseEntity<DailyTicket> response = customerClient.exchange("/daily-tickets/{id}", HttpMethod.DELETE, new HttpEntity<>(id), DailyTicket.class, id);
        assertNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
}

package com.metrolejbus.app.transportation.controllers;

import com.metrolejbus.app.transportation.exceptions.LineNotFound;
import com.metrolejbus.app.transportation.exceptions.StationNotFound;
import com.metrolejbus.app.transportation.forms.*;
import com.metrolejbus.app.transportation.models.*;
import com.metrolejbus.app.transportation.services.JourneyService;
import com.metrolejbus.app.transportation.services.LineService;
import com.metrolejbus.app.transportation.services.StationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test,controller-test")
public class LineControllerTest {

    @Autowired
    private TestRestTemplate adminClient;
    @Autowired
    private TestRestTemplate publicClient;
    @MockBean
    private LineService lineService;
    @MockBean
    private JourneyService journeyService;
    @MockBean
    private StationService stationService;

    private Map<Long, Line> mockedLines = new HashMap<>();
    private Map<Long, Station> mockedStations = new HashMap<>();
    private Map<Long, Journey> mockedJourneys = new HashMap<>();

    @PostConstruct
    public void postConstruct() {
        this.adminClient =
            this.adminClient.withBasicAuth("admin", "password");
    }

    @Before
    public void setUp() throws LineNotFound, StationNotFound {
        mockedLines.clear();
        mockedStations.clear();
        mockedJourneys.clear();

        Zone zone1 = new Zone("Zone 1");
        zone1.setId(1L);

        Station station1 = new Station("Station 1", zone1, 30, 30);
        station1.setId(1L);
        mockedStations.put(1L, station1);

        Station station2 = new Station("Station 2", zone1, 30, 30);
        station2.setId(2L);
        mockedStations.put(2L, station2);

        Station station3 = new Station("Station 3", zone1, 30, 30);
        station3.setId(3L);
        mockedStations.put(3L, station3);

        Line line1 = new Line("Line 1", TransportationType.BUS, "");
        line1.setId(1L);
        line1.getStations().add(station1);
        mockedLines.put(1L, line1);

        Line line2 = new Line("Line 2", TransportationType.METRO, "");
        line2.setId(2L);
        mockedLines.put(2L, line2);

        Line line3 = new Line("Line 3", TransportationType.TRAM, "");
        line3.setId(3L);
        line3.getStations().add(station1);
        mockedLines.put(3L, line3);

        Line line4 = new Line("Line 4", TransportationType.BUS, "");
        line4.setId(4L);
        line4.getStations().add(station1);
        line4.getStations().add(station2);
        line4.getStations().add(station3);
        mockedLines.put(4L, line4);

        Journey journey1 = new Journey(line1,
            LocalDateTime.of(2019, 1, 1, 12, 0)
        );
        journey1.setId(1L);
        mockedJourneys.put(1L, journey1);

        Journey journey2 = new Journey(line1,
            LocalDateTime.of(2019, 1, 1, 16, 0)
        );
        journey2.setId(2L);
        mockedJourneys.put(2L, journey2);

        when(lineService.create(
            anyString(),
            any(TransportationType.class),
            anyString()
        )).thenAnswer(invocation -> {
            String name = invocation.getArgument(0);
            TransportationType type = invocation.getArgument(1);
            String route = invocation.getArgument(2);

            long key = mockedLines.size();
            Line l = new Line(name, type, route);
            l.setId(++key);
            mockedLines.put(key, l);

            return l;
        });

        when(lineService.get(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);

            Line l = mockedLines.getOrDefault(key, null);
            if (l == null) { throw new LineNotFound(key); }

            return l;
        });

        when(lineService.getAll())
            .thenReturn(new LinkedList<>(mockedLines.values()));

        when(lineService.getAll(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            return mockedLines.values()
                .stream()
                .filter(l -> l.getStations()
                    .stream()
                    .filter(s -> key == s.getId())
                    .count() == 1)
                .collect(Collectors.toList());
        });

        when(lineService.delete(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);

            Line l = mockedLines.getOrDefault(key, null);
            if (l == null) { throw new LineNotFound(key); }
            l.setActive(false);

            return l;
        });

        when(lineService.update(anyLong(), anyString()))
            .thenAnswer(invocation -> {
                long key = invocation.getArgument(0);
                String name = invocation.getArgument(1);

                Line l = mockedLines.getOrDefault(key, null);
                if (l == null) { throw new LineNotFound(key); }
                l.setName(name);

                return l;
            });

        when(lineService.update(anyLong(), anyString(), anyString()))
            .thenAnswer(invocation -> {
                long key = invocation.getArgument(0);
                String name = invocation.getArgument(1);
                String route = invocation.getArgument(2);

                Line l = mockedLines.getOrDefault(key, null);
                if (l == null) { throw new LineNotFound(key); }

                l.setName(name);
                l.setRoute(route);

                return l;
            });

        when(lineService.addStation(anyLong(), anyLong()))
            .thenAnswer(invocation -> {
                long key = invocation.getArgument(0);
                long stationId = invocation.getArgument(1);

                Line l = lineService.get(key);
                Station s = stationService.get(stationId);
                l.getStations().add(s);

                return l;
            });

        when(lineService.addStations(anyLong(), anyList()))
            .thenAnswer(invocation -> {
                long key = invocation.getArgument(0);
                List<Long> stationIds = invocation.getArgument(1);
                Line l = lineService.get(key);

                Set<Station> stations = l.getStations();
                for(Long stationId: stationIds) {
                    stations.add(stationService.get(stationId));
                }

                return l;
            });

        when(lineService.removeStation(anyLong(), anyLong())).thenAnswer(
            invocation -> {
                long key = invocation.getArgument(0);
                long stationId = invocation.getArgument(1);

                Line l = lineService.get(key);
                Station s = stationService.get(stationId);
                l.getStations().remove(s);

                return l;
            });

        when(lineService.removeStations(anyLong(), anyList()))
            .thenAnswer(invocation -> {
                long key = invocation.getArgument(0);
                List<Long> stationIds = invocation.getArgument(1);
                Line l = lineService.get(key);

                Set<Station> stations = l.getStations();
                for(Long stationId: stationIds) {
                    stations.remove(stationService.get(stationId));
                }

                return l;
            });

        when(lineService.getStations(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            Line l = lineService.get(key);
            return l.getStations();
        });

        when(lineService.exists(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            return mockedLines.containsKey(key);
        });

        when(stationService.get(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);

            Station s = mockedStations.getOrDefault(key, null);
            if (s == null) { throw new StationNotFound(key); }

            return s;
        });

        when(journeyService.getAll(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            return mockedJourneys.values()
                .stream()
                .filter(j -> key == j.getLine().getId())
                .collect(Collectors.toList());
        });
    }

    @Test
    public void createSuccess() {
        CreateLine form = new CreateLine();
        form.name = "Line 3";
        form.type = TransportationType.TRAM;
        form.route = "Route";

        ResponseEntity<Line> response = adminClient.exchange(
            "/lines",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Line.class
        );
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(lineService, times(1)).create(
            anyString(),
            any(TransportationType.class),
            anyString()
        );
        verifyNoMoreInteractions(lineService);
    }

    @Test
    public void createNameIsNull() {
        CreateLine form = new CreateLine();
        form.type = TransportationType.TRAM;
        form.route = "Route";

        ResponseEntity<String> response = adminClient.exchange(
            "/lines",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());

        verifyZeroInteractions(lineService);
    }

    @Test
    public void createNameIsBlank() {
        CreateLine form = new CreateLine();
        form.name = "";
        form.type = TransportationType.TRAM;
        form.route = "Route";

        ResponseEntity<String> response = adminClient.exchange(
            "/lines",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());

        verifyZeroInteractions(lineService);
    }

    @Test
    public void createTypeIsNull() {
        CreateLine form = new CreateLine();
        form.name = "Line 3";
        form.route = "Route";

        ResponseEntity<String> response = adminClient.exchange(
            "/lines",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());

        verifyZeroInteractions(lineService);
    }

    @Test
    public void createRouteIsNull() {
        CreateLine form = new CreateLine();
        form.name = "Line 3";
        form.type = TransportationType.BUS;

        ResponseEntity<String> response = adminClient.exchange(
            "/lines",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());

        verifyZeroInteractions(lineService);
    }

    @Test
    public void createRouteIsBlank() {
        CreateLine form = new CreateLine();
        form.name = "Line 3";
        form.type = TransportationType.BUS;
        form.route = "";

        ResponseEntity<String> response = adminClient.exchange(
            "/lines",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());

        verifyZeroInteractions(lineService);
    }

    @Test
    public void createForbidden() {
        CreateLine form = new CreateLine();
        form.name = "Line 3";
        form.type = TransportationType.TRAM;
        form.route = "Route";

        ResponseEntity<Line> response = publicClient.exchange(
            "/lines",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Line.class
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void getSuccess() throws LineNotFound {
        ResponseEntity<Line> response = publicClient.exchange(
            "/lines/{id}",
            HttpMethod.GET,
            null,
            Line.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(lineService, times(1)).get(anyLong());
        verifyNoMoreInteractions(lineService);
    }

    @Test
    public void getLineNotFound() throws LineNotFound {
        ResponseEntity<String> response = publicClient.exchange(
            "/lines/{id}",
            HttpMethod.GET,
            null,
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(lineService, times(1)).get(anyLong());
        verifyNoMoreInteractions(lineService);
    }

    @Test
    public void getAllSuccess() {
        ResponseEntity<List<Line>> response = publicClient.exchange(
            "/lines",
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<List<Line>>() {}
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(4, response.getBody().size());

        verify(lineService, times(1)).getAll();
        verifyNoMoreInteractions(lineService);

        response = publicClient.exchange(
            "/lines?station={stationId}",
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<List<Line>>() {},
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(3, response.getBody().size());

        verify(lineService, times(1)).getAll(anyLong());
        verifyNoMoreInteractions(lineService);

        response = publicClient.exchange(
            "/lines?station={stationId}",
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<List<Line>>() {},
            99
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(0, response.getBody().size());
    }

    @Test
    public void deleteSuccess() throws LineNotFound {
        ResponseEntity<Line> response = adminClient.exchange(
            "/lines/{id}",
            HttpMethod.DELETE,
            null,
            Line.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertFalse(response.getBody().isActive());

        verify(lineService, times(1)).delete(anyLong());
        verifyNoMoreInteractions(lineService);
    }

    @Test
    public void deleteLineNotFound() throws LineNotFound {
        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}",
            HttpMethod.DELETE,
            null,
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(lineService, times(1)).delete(anyLong());
        verifyNoMoreInteractions(lineService);
    }

    @Test
    public void deleteForbidden() {
        ResponseEntity<Line> response = publicClient.exchange(
            "/lines/{id}",
            HttpMethod.DELETE,
            null,
            Line.class,
            1
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
        assertFalse(response.getBody().isActive());
    }

    @Test
    public void updateSuccess() throws LineNotFound {
        UpdateLine form = new UpdateLine();
        form.name = "Line 3";

        ResponseEntity<Line> response = adminClient.exchange(
            "/lines/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            Line.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(lineService, times(1)).update(anyLong(), anyString());
        verifyNoMoreInteractions(lineService);
    }

    @Test
    public void updateNameIsBlank() {
        UpdateLine form = new UpdateLine();
        form.name = "";

        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            String.class,
            1
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());

        verifyZeroInteractions(lineService);
    }

    @Test
    public void updateRouteIsBlank() {
        UpdateLine form = new UpdateLine();
        form.name = "Line 3";
        form.route = "";

        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            String.class,
            1
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());

        verifyZeroInteractions(lineService);
    }

    @Test
    public void updateForbidden() {
        UpdateLine form = new UpdateLine();
        form.name = "Line 3";

        ResponseEntity<Line> response = publicClient.exchange(
            "/lines/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            Line.class,
            1
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void addStationSuccess() throws StationNotFound, LineNotFound {
        AddStationToLine form = new AddStationToLine();
        form.stationId = 1L;

        ResponseEntity<Line> response = adminClient.exchange(
            "/lines/{id}/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Line.class,
            2
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(1, response.getBody().getStations().size());

        verify(lineService, times(1)).addStation(anyLong(), anyLong());
        verify(lineService, times(1)).get(anyLong());
        verifyNoMoreInteractions(lineService);

        verify(stationService, times(1)).get(anyLong());
        verifyNoMoreInteractions(stationService);

        response = adminClient.exchange(
            "/lines/{id}/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Line.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(1, response.getBody().getStations().size());
    }

    @Test
    public void addStationLineNotFound() throws StationNotFound, LineNotFound {
        AddStationToLine form = new AddStationToLine();
        form.stationId = 1L;

        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(lineService, times(1)).addStation(anyLong(), anyLong());
        verify(lineService, times(1)).get(anyLong());
        verifyNoMoreInteractions(lineService);

        verifyZeroInteractions(stationService);
    }

    @Test
    public void addStationStationNotFound() throws StationNotFound, LineNotFound
    {
        AddStationToLine form = new AddStationToLine();
        form.stationId = 99L;

        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class,
            1
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(lineService, times(1)).addStation(anyLong(), anyLong());
        verify(lineService, times(1)).get(anyLong());
        verifyNoMoreInteractions(lineService);

        verify(stationService, times(1)).get(anyLong());
        verifyNoMoreInteractions(stationService);
    }

    @Test
    public void addStationStationIdIsNull() {
        AddStationToLine form = new AddStationToLine();

        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class,
            1
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());

        verifyZeroInteractions(lineService);
    }

    @Test
    public void addStationForbidden() {
        AddStationToLine form = new AddStationToLine();
        form.stationId = 1L;

        ResponseEntity<Line> response = publicClient.exchange(
            "/lines/{id}/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Line.class,
            2
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void addStationsSuccess() throws StationNotFound, LineNotFound {
        AddStationsToLine form = new AddStationsToLine();
        form.stationIds = Arrays.asList(1L, 2L, 3L);

        ResponseEntity<Line> response = adminClient.exchange(
            "/lines/{id}/stations/bulk",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Line.class,
            2
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());

        Line line = response.getBody();
        assertNotNull(line);
        assertEquals(3, line.getStations().size());

        verify(lineService, times(1)).addStations(anyLong(), anyList());
        verify(lineService, times(1)).get(anyLong());
        verifyNoMoreInteractions(lineService);

        verify(stationService, times(3)).get(anyLong());
        verifyNoMoreInteractions(stationService);
    }

    @Test
    public void addStationsLineNotFound() throws StationNotFound, LineNotFound {
        AddStationsToLine form = new AddStationsToLine();
        form.stationIds = Arrays.asList(1L, 2L, 3L);

        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}/stations/bulk",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(lineService, times(1)).addStations(anyLong(), anyList());
        verify(lineService, times(1)).get(anyLong());
        verifyNoMoreInteractions(lineService);

        verifyZeroInteractions(stationService);
    }

    @Test
    public void addStationsStationNotFound()
        throws StationNotFound, LineNotFound
    {
        AddStationsToLine form = new AddStationsToLine();
        form.stationIds = Arrays.asList(1L, 99L, 3L);

        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}/stations/bulk",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class,
            2
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(lineService, times(1)).addStations(anyLong(), anyList());
        verify(lineService, times(1)).get(anyLong());
        verifyNoMoreInteractions(lineService);

        verify(stationService, times(2)).get(anyLong());
        verifyNoMoreInteractions(stationService);
    }

    @Test
    public void addStationsForbidden() {
        AddStationsToLine form = new AddStationsToLine();
        form.stationIds = Arrays.asList(1L, 2L, 3L);

        ResponseEntity<String> response = publicClient.exchange(
            "/lines/{id}/stations/bulk",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class,
            2
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());

        verifyZeroInteractions(lineService);
    }

    @Test
    public void getStationsSuccess() throws LineNotFound {
        ResponseEntity<Set<Station>> response = publicClient.exchange(
            "/lines/{id}/stations",
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<Set<Station>>() {},
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(1, response.getBody().size());

        verify(lineService, times(1)).getStations(anyLong());
        verify(lineService, times(1)).get(anyLong());
        verifyNoMoreInteractions(lineService);
    }

    @Test
    public void getStationsLineNotFound() throws LineNotFound {
        ResponseEntity<String> response = publicClient.exchange(
            "/lines/{id}/stations",
            HttpMethod.GET,
            null,
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(lineService, times(1)).getStations(anyLong());
        verify(lineService, times(1)).get(anyLong());
        verifyNoMoreInteractions(lineService);
    }

    @Test
    public void removeStationSuccess() throws StationNotFound, LineNotFound {
        ResponseEntity<Line> response = adminClient.exchange(
            "/lines/{id}/stations/{stationId}",
            HttpMethod.DELETE,
            null,
            Line.class,
            1,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(0, response.getBody().getStations().size());

        verify(lineService, times(1)).removeStation(anyLong(), anyLong());
        verify(lineService, times(1)).get(anyLong());
        verifyNoMoreInteractions(lineService);

        verify(stationService, times(1)).get(anyLong());
        verifyNoMoreInteractions(stationService);
    }

    @Test
    public void removeStationLineNotFound() throws StationNotFound, LineNotFound
    {
        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}/stations/{stationId}",
            HttpMethod.DELETE,
            null,
            String.class,
            99,
            1
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(lineService, times(1)).removeStation(anyLong(), anyLong());
        verify(lineService, times(1)).get(anyLong());
        verifyNoMoreInteractions(lineService);

        verifyZeroInteractions(stationService);
    }

    @Test
    public void removeStationStationNotFound()
        throws StationNotFound, LineNotFound
    {
        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}/stations/{stationId}",
            HttpMethod.DELETE,
            null,
            String.class,
            1,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(lineService, times(1)).removeStation(anyLong(), anyLong());
        verify(lineService, times(1)).get(anyLong());
        verifyNoMoreInteractions(lineService);

        verify(stationService, times(1)).get(anyLong());
        verifyNoMoreInteractions(stationService);
    }

    @Test
    public void removeStationForbidden() {
        ResponseEntity<Line> response = publicClient.exchange(
            "/lines/{id}/stations/{stationId}",
            HttpMethod.DELETE,
            null,
            Line.class,
            1,
            1
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void removeStationsSuccess() throws StationNotFound, LineNotFound {
        RemoveStationsFromLine form = new RemoveStationsFromLine();
        form.stationIds = Arrays.asList(1L, 2L);

        ResponseEntity<Line> response = adminClient.exchange(
            "/lines/{id}/stations/bulk",
            HttpMethod.DELETE,
            new HttpEntity<>(form),
            Line.class,
            4
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());

        Line line = response.getBody();
        assertNotNull(line);
        assertEquals(1, line.getStations().size());

        verify(lineService, times(1)).removeStations(anyLong(), anyList());
        verify(lineService, times(1)).get(anyLong());
        verifyNoMoreInteractions(lineService);

        verify(stationService, times(2)).get(anyLong());
        verifyNoMoreInteractions(stationService);
    }

    @Test
    public void removeStationsLineNotFound()
        throws StationNotFound, LineNotFound
    {
        RemoveStationsFromLine form = new RemoveStationsFromLine();
        form.stationIds = Arrays.asList(1L, 2L);

        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}/stations/bulk",
            HttpMethod.DELETE,
            new HttpEntity<>(form),
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(lineService, times(1)).removeStations(anyLong(), anyList());
        verify(lineService, times(1)).get(anyLong());
        verifyNoMoreInteractions(lineService);

        verifyZeroInteractions(stationService);
    }

    @Test
    public void removeStationsStationNotFound()
        throws StationNotFound, LineNotFound
    {
        RemoveStationsFromLine form = new RemoveStationsFromLine();
        form.stationIds = Arrays.asList(1L, 99L);

        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}/stations/bulk",
            HttpMethod.DELETE,
            new HttpEntity<>(form),
            String.class,
            4
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(lineService, times(1)).removeStations(anyLong(), anyList());
        verify(lineService, times(1)).get(anyLong());
        verifyNoMoreInteractions(lineService);

        verify(stationService, times(2)).get(anyLong());
        verifyNoMoreInteractions(stationService);
    }

    @Test
    public void removeStationsForbidden() {
        RemoveStationsFromLine form = new RemoveStationsFromLine();
        form.stationIds = Arrays.asList(1L, 2L);

        ResponseEntity<String> response = publicClient.exchange(
            "/lines/{id}/stations/bulk",
            HttpMethod.DELETE,
            new HttpEntity<>(form),
            String.class,
            4
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());

        verifyZeroInteractions(lineService);
    }

    @Test
    public void getJourneysSuccess() {
        ResponseEntity<List<Journey>> response = publicClient.exchange(
            "/lines/{id}/journeys",
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<List<Journey>>() {},
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(2, response.getBody().size());

        verify(lineService, times(1)).exists(anyLong());
        verifyNoMoreInteractions(lineService);

        verify(journeyService, times(1)).getAll(anyLong());
        verifyNoMoreInteractions(journeyService);
    }

    @Test
    public void getJourneysLineNotFound() {
        ResponseEntity<String> response = publicClient.exchange(
            "/lines/{id}/journeys",
            HttpMethod.GET,
            null,
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(lineService, times(1)).exists(anyLong());
        verifyNoMoreInteractions(lineService);
        verifyZeroInteractions(journeyService);
    }

}

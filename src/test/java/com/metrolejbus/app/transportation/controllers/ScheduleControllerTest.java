package com.metrolejbus.app.transportation.controllers;

import com.metrolejbus.app.common.models.ValidityPeriod;
import com.metrolejbus.app.transportation.exceptions.LineNotFound;
import com.metrolejbus.app.transportation.exceptions.ScheduleNotFound;
import com.metrolejbus.app.transportation.exceptions.StationNotFound;
import com.metrolejbus.app.transportation.forms.*;
import com.metrolejbus.app.transportation.models.*;
import com.metrolejbus.app.transportation.services.JourneyService;
import com.metrolejbus.app.transportation.services.LineService;
import com.metrolejbus.app.transportation.services.ScheduleService;
import com.metrolejbus.app.transportation.services.StationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import javax.management.InvalidAttributeValueException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test,controller-test")
public class ScheduleControllerTest {

    @Autowired
    private TestRestTemplate adminClient;
    @Autowired
    private TestRestTemplate publicClient;
    @MockBean
    private LineService lineService;
    @MockBean
    private JourneyService journeyService;
    @MockBean
    private StationService stationService;

    @MockBean
    private ScheduleService scheduleService;

    private Map<Long, Line> mockedLines = new HashMap<>();
    private Map<Long, Station> mockedStations = new HashMap<>();
    private Map<Long, Journey> mockedJourneys = new HashMap<>();
    private Map<Long, Schedule> mockedSchedules = new HashMap<>();

    @PostConstruct
    public void postConstruct() {
        this.adminClient =
                this.adminClient.withBasicAuth("admin", "password");
    }

    @Before
    public void setUp() throws LineNotFound, StationNotFound, ScheduleNotFound, InvalidAttributeValueException {
        mockedLines.clear();
        mockedStations.clear();
        mockedJourneys.clear();
        mockedSchedules.clear();

        Zone zone1 = new Zone("Zone 1");
        zone1.setId(1L);

        Station station1 = new Station("Station 1", zone1, 30, 30);
        station1.setId(1L);
        mockedStations.put(1L, station1);

        Station station2 = new Station("Station 2", zone1, 30, 30);
        station2.setId(2L);
        mockedStations.put(2L, station2);

        Station station3 = new Station("Station 3", zone1, 30, 30);
        station3.setId(3L);
        mockedStations.put(3L, station3);

        Line line1 = new Line("Line 1", TransportationType.BUS, "");
        line1.setId(1L);
        line1.getStations().add(station1);
        mockedLines.put(1L, line1);

        Line line2 = new Line("Line 2", TransportationType.METRO, "");
        line2.setId(2L);
        mockedLines.put(2L, line2);

        Line line3 = new Line("Line 3", TransportationType.TRAM, "");
        line3.setId(3L);
        line3.getStations().add(station1);
        mockedLines.put(3L, line3);

        Line line4 = new Line("Line 4", TransportationType.BUS, "");
        line4.setId(4L);
        line4.getStations().add(station1);
        line4.getStations().add(station2);
        line4.getStations().add(station3);
        mockedLines.put(4L, line4);

        Journey journey1 = new Journey(line1,
                LocalDateTime.of(2019, 1, 1, 12, 0)
        );
        journey1.setId(1L);
        mockedJourneys.put(1L, journey1);

        Journey journey2 = new Journey(line1,
                LocalDateTime.of(2019, 1, 1, 16, 0)
        );
        journey2.setId(2L);
        mockedJourneys.put(2L, journey2);

        List<DayOfWeek> appliesTo = new ArrayList<DayOfWeek>();
        appliesTo.add(DayOfWeek.MONDAY);
        appliesTo.add(DayOfWeek.TUESDAY);
        appliesTo.add(DayOfWeek.WEDNESDAY);
        appliesTo.add(DayOfWeek.THURSDAY);
        appliesTo.add(DayOfWeek.FRIDAY);
        List<LocalTime> routeA = new ArrayList<LocalTime>();
        routeA.add(LocalTime.of(12, 0));
        routeA.add(LocalTime.of(12, 10));
        routeA.add(LocalTime.of(12, 15));
        routeA.add(LocalTime.of(13, 0));
        routeA.add(LocalTime.of(13, 10));
        routeA.add(LocalTime.of(13, 15));
        List<LocalTime> routeB = new ArrayList<LocalTime>();
        routeA.add(LocalTime.of(12, 20));
        routeA.add(LocalTime.of(12, 25));
        routeA.add(LocalTime.of(12, 35));
        routeA.add(LocalTime.of(13, 12));
        routeA.add(LocalTime.of(13, 22));
        routeA.add(LocalTime.of(13, 32));
        ValidityPeriod validityPeriod = null;
        try {
            validityPeriod = new ValidityPeriod(LocalDate.of(2019, 5, 20), LocalDate.of(2019, 10, 20));
        } catch (InvalidAttributeValueException e) {
            e.printStackTrace();
        }

        Schedule schedule1 = new Schedule(appliesTo, routeA, routeB, validityPeriod);
        schedule1.setId(1L);
        mockedSchedules.put(schedule1.getId(), schedule1);
///
        when(scheduleService.save(
                any(ScheduleForm.class)
        )).thenAnswer(invocation -> {
            Schedule schedule = invocation.getArgument(0);
            long key = mockedSchedules.size();
            Schedule s = new Schedule(schedule.getAppliesTo(), schedule.getRouteA(), schedule.getRouteB(), schedule.getValidityPeriod());
            s.setId(++key);
            mockedSchedules.put(key, s);
            return s;
        });

        when(scheduleService.findAll()).thenReturn(new LinkedList<>(mockedSchedules.values()));

        when(scheduleService.findById(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);

            Schedule l = mockedSchedules.getOrDefault(key, null);
            if (l == null) {
                throw new ScheduleNotFound(key);
            }
            return l;
        });

        when(scheduleService.deleteById(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);

            Schedule l = mockedSchedules.getOrDefault(key, null);
            if (l == null) {
                throw new ScheduleNotFound(key);
            }
            l.setActive(false);
            return l;
        });
        ///

    }





    @Test
    public void getSuccess() throws ScheduleNotFound {
        ResponseEntity<Schedule> response = publicClient.exchange(
                "/schedules/{id}",
                HttpMethod.GET,
                null,
                Schedule.class,
                1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(scheduleService, times(1)).findById(anyLong());
        verifyNoMoreInteractions(scheduleService);
    }

//    @Test
//    public void getScheduleNotFound() throws LineNotFound {
//        ResponseEntity<String> response = publicClient.exchange(
//                "/schedule/{id}",
//                HttpMethod.GET,
//                null,
//                String.class,
//                99
//        );
//        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
//        assertNotNull(response.getBody());
//
//        verify(lineService, times(1)).get(anyLong());
//        verifyNoMoreInteractions(lineService);
//    }

    @Test
    public void getAllSuccess() {
        ResponseEntity<List<Schedule>> response = publicClient.exchange(
                "/schedules",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Schedule>>() {
                }
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

//        verify(scheduleService, times(1)).findAll();
//        verifyNoMoreInteractions(scheduleService);


    }

//    @Test
//    public void deleteSuccess() throws ScheduleNotFound {
//        ResponseEntity<String> response = adminClient.exchange(
//                "/schedule/{id}",
//                HttpMethod.DELETE,
//                null,
//                String.class,
//                1
//        );
//        assertEquals(HttpStatus.OK, response.getStatusCode());
//        assertNotNull(response.getBody());
//
//        verify(scheduleService, times(1)).deleteById(anyLong());
//        verifyNoMoreInteractions(scheduleService);
//    }

//    @Test
//    public void deleteScheduleNotFound() throws ScheduleNotFound {
//        ResponseEntity<String> response = adminClient.exchange(
//                "/schedule/{id}",
//                HttpMethod.DELETE,
//                null,
//                String.class,
//                99
//        );
//        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
//        assertNotNull(response.getBody());
//
//        verify(scheduleService, times(1)).deleteById(anyLong());
//        verifyNoMoreInteractions(lineService);
//    }




}

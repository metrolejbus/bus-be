package com.metrolejbus.app.transportation.controllers;

import com.metrolejbus.app.transportation.exceptions.StationNotFound;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.forms.CreateStation;
import com.metrolejbus.app.transportation.forms.UpdateStation;
import com.metrolejbus.app.transportation.models.Station;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.services.StationService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test,controller-test")
public class StationControllerTest {

    @Autowired
    private TestRestTemplate adminClient;
    @Autowired
    private TestRestTemplate publicClient;
    @MockBean
    private StationService stationService;

    private Map<Long, Station> mockedStations = new HashMap<>();

    @PostConstruct
    public void postConstruct() {
        this.adminClient =
            this.adminClient.withBasicAuth("admin", "password");
    }

    @Before
    public void setUp() throws Exception {
        mockedStations.clear();

        Zone zone1 = new Zone("Zone 1");
        zone1.setId(1L);

        Zone zone2 = new Zone("Zone 2");
        zone2.setId(2L);

        Station station1 = new Station("Station 1", zone1, 30, 30);
        station1.setId(1L);
        mockedStations.put(1L, station1);

        Station station2 = new Station("Station 2", zone1, 30, 30);
        station2.setId(2L);
        mockedStations.put(2L, station2);

        when(stationService.get(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            Station s = mockedStations.getOrDefault(key, null);
            if (s == null) {
                throw new StationNotFound(key);
            }
            return s;
        });

        when(stationService.getAll()).thenReturn(
            new LinkedList<>(mockedStations.values())
        );

        when(stationService.create(anyString(),
            anyLong(),
            anyFloat(),
            anyFloat()
        )).thenAnswer(invocation -> {
            String name = invocation.getArgument(0);
            long zoneId = invocation.getArgument(1);
            float lat = invocation.getArgument(2);
            float lng = invocation.getArgument(3);

            if (zoneId != 1L && zoneId != 2L) {
                throw new ZoneNotFound(zoneId);
            }

            Zone z = zoneId == 1L ? zone1 : zone2;

            long id = mockedStations.size();

            Station s = new Station(name, z, lat, lng);
            s.setId(++id);
            mockedStations.put(id, s);
            return s;
        });

        when(stationService.delete(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            Station s = mockedStations.getOrDefault(key, null);
            if (s == null) {
                throw new StationNotFound(key);
            }
            s.setActive(false);
            return s;
        });


        when(stationService.update(anyLong(), anyString())).thenAnswer(
            invocation -> {
                long key = invocation.getArgument(0);
                String name = invocation.getArgument(1);
                Station s = mockedStations.getOrDefault(key, null);
                if (s == null) {
                    throw new StationNotFound(key);
                }
                s.setName(name);
                return s;
            });


        when(stationService.update(anyLong(),
            anyLong()
        )).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            long zoneId = invocation.getArgument(1);

            Station s = mockedStations.getOrDefault(key, null);
            if (s == null) {
                throw new StationNotFound(key);
            }

            if (zoneId != 1 && zoneId != 2) {
                throw new ZoneNotFound(zoneId);
            }

            Zone z = zoneId == 1 ? zone1 : zone2;
            s.setZone(z);
            return s;
        });

        when(stationService.update(anyLong(),
            anyString(),
            anyLong()
        )).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            String name = invocation.getArgument(1);
            long zoneId = invocation.getArgument(2);

            Station s = mockedStations.getOrDefault(key, null);
            if (s == null) {
                throw new StationNotFound(key);
            }

            s.setName(name);

            if (zoneId != 1 && zoneId != 2) {
                throw new ZoneNotFound(zoneId);
            }

            Zone z = zoneId == 1 ? zone1 : zone2;
            s.setZone(z);
            return s;
        });
    }

    @Test
    public void deleteSuccess() throws StationNotFound {
        ResponseEntity<Station> response = adminClient.exchange(
            "/stations/{id}",
            HttpMethod.DELETE,
            null,
            Station.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        Station station = response.getBody();
        assertNotNull(station.getId());
        assertEquals(1L, (long) station.getId());

        verify(stationService, times(1)).delete(anyLong());
        verifyNoMoreInteractions(stationService);
    }

    @Test
    public void deleteStationNotFound() {
        ResponseEntity<String> response = adminClient.exchange(
            "/stations/{id}",
            HttpMethod.DELETE,
            null,
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void deleteForbidden() {
        ResponseEntity<Station> response = publicClient.exchange(
            "/stations/{id}",
            HttpMethod.DELETE,
            null,
            Station.class,
            1
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void getSuccess() throws StationNotFound {
        ResponseEntity<Station> response = publicClient.exchange(
            "/stations/{id}",
            HttpMethod.GET,
            null,
            Station.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        Station station = response.getBody();
        assertNotNull(station.getId());
        assertEquals(1L, (long) station.getId());

        verify(stationService, times(1)).get(anyLong());
        verifyNoMoreInteractions(stationService);
    }

    @Test
    public void getStationNotFound() throws StationNotFound {
        ResponseEntity<String> response = publicClient.exchange(
            "/stations/{id}",
            HttpMethod.GET,
            null,
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(stationService, times(1)).get(anyLong());
        verifyNoMoreInteractions(stationService);
    }

    @Test
    public void getAllSuccess() {
        ResponseEntity<List<Station>> response = publicClient.exchange(
            "/stations",
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<List<Station>>() {}
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(2, response.getBody().size());

        verify(stationService, times(1)).getAll();
        verifyNoMoreInteractions(stationService);
    }

    @Test
    public void createSuccess() throws ZoneNotFound {
        CreateStation form = new CreateStation();
        form.name = "Station 3";
        form.zoneId = 1L;
        form.lat = 30f;
        form.lng = 30f;

        ResponseEntity<Station> response = adminClient.exchange(
            "/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Station.class
        );
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());

        Station station = response.getBody();
        assertNotNull(station.getId());
        assertEquals(3L, (long) station.getId());

        verify(stationService, times(1)).create(anyString(),
            anyLong(),
            anyFloat(),
            anyFloat()
        );
        verifyNoMoreInteractions(stationService);
    }

    @Test
    public void createZoneNotFound() throws ZoneNotFound {
        CreateStation form = new CreateStation();
        form.name = "Station 3";
        form.zoneId = 99L;
        form.lat = 30f;
        form.lng = 30f;

        ResponseEntity<String> response = adminClient.exchange(
            "/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(stationService, times(1)).create(anyString(),
            anyLong(),
            anyFloat(),
            anyFloat()
        );
        verifyNoMoreInteractions(stationService);
    }

    @Test
    public void createInvalidCoordinates() {
        CreateStation form = new CreateStation();
        form.name = "Station 3";
        form.zoneId = 1L;
        form.lat = -91f;
        form.lng = 30f;

        ResponseEntity<String> response = adminClient.exchange(
            "/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());

        form.lat = 91f;
        response = adminClient.exchange(
            "/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());

        form.lng = -181f;
        response = adminClient.exchange(
            "/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());

        form.lng = 181f;
        response = adminClient.exchange(
            "/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void createForbidden() {
        CreateStation form = new CreateStation();
        form.name = "Station 3";
        form.zoneId = 1L;
        form.lat = 30f;
        form.lng = 30f;

        ResponseEntity<Station> response = publicClient.exchange(
            "/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Station.class
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void updateNameSuccess() throws StationNotFound {
        UpdateStation form = new UpdateStation();
        form.name = "New Station Name";

        ResponseEntity<Station> response = adminClient.exchange(
            "/stations/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            Station.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        Station station = response.getBody();
        assertNotNull(station.getId());
        assertEquals(1L, (long) station.getId());
        assertEquals("New Station Name", station.getName());
        assertEquals(1L, (long) station.getZone().getId());

        verify(stationService, times(1)).update(anyLong(), anyString());
        verifyNoMoreInteractions(stationService);
    }

    @Test
    public void updateZoneSuccess() throws StationNotFound, ZoneNotFound {
        UpdateStation form = new UpdateStation();
        form.zoneId = 2L;

        ResponseEntity<Station> response = adminClient.exchange(
            "/stations/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            Station.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        Station station = response.getBody();
        assertNotNull(station.getId());
        assertEquals(1L, (long) station.getId());
        assertEquals("Station 1", station.getName());
        assertEquals(2L, (long) station.getZone().getId());

        verify(stationService, times(1)).update(anyLong(), anyLong());
        verifyNoMoreInteractions(stationService);
    }

    @Test
    public void updateZoneZoneNotFound() throws StationNotFound, ZoneNotFound {
        UpdateStation form = new UpdateStation();
        form.zoneId = 99L;

        ResponseEntity<String> response = adminClient.exchange(
            "/stations/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            String.class,
            1
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(stationService, times(1)).update(anyLong(), anyLong());
        verifyNoMoreInteractions(stationService);
    }

    @Test
    public void updateNameAndZoneSuccess() throws StationNotFound, ZoneNotFound
    {
        UpdateStation form = new UpdateStation();
        form.name = "New Zone Name";
        form.zoneId = 2L;

        ResponseEntity<Station> response = adminClient.exchange(
            "/stations/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            Station.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        Station station = response.getBody();
        assertNotNull(station.getId());
        assertEquals(1L, (long) station.getId());
        assertEquals("New Zone Name", station.getName());
        assertEquals(2L, (long) station.getZone().getId());

        verify(stationService, times(1)).update(anyLong(),
            anyString(),
            anyLong()
        );
        verifyNoMoreInteractions(stationService);
    }

    @Test
    public void updateNameAndZoneZoneNotFound()
        throws StationNotFound, ZoneNotFound
    {
        UpdateStation form = new UpdateStation();
        form.name = "New Zone Name";
        form.zoneId = 99L;

        ResponseEntity<String> response = adminClient.exchange(
            "/stations/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            String.class,
            1
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(stationService, times(1)).update(anyLong(),
            anyString(),
            anyLong()
        );
        verifyNoMoreInteractions(stationService);
    }

    @Test
    public void updateZoneNotFound() throws StationNotFound, ZoneNotFound {
        UpdateStation form = new UpdateStation();
        form.name = "New Zone Name";
        form.zoneId = 1L;

        ResponseEntity<String> response = adminClient.exchange(
            "/stations/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(stationService, atMost(1)).update(anyLong(), anyLong());
        verify(stationService, atMost(1)).update(anyLong(), anyString());
        verify(stationService, atMost(1)).update(anyLong(),
            anyString(),
            anyLong()
        );
        verifyNoMoreInteractions(stationService);
    }

    @Test
    public void updateForbidden() {
        UpdateStation form = new UpdateStation();
        form.name = "New Zone Name";
        form.zoneId = 2L;

        ResponseEntity<Station> response = publicClient.exchange(
            "/stations/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            Station.class,
            1
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

}

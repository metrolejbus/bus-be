package com.metrolejbus.app.transportation.controllers;

import com.metrolejbus.app.ClearDatabaseRule;
import com.metrolejbus.app.transportation.forms.CreateStation;
import com.metrolejbus.app.transportation.forms.UpdateStation;
import com.metrolejbus.app.transportation.models.Station;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.repositories.StationRepository;
import com.metrolejbus.app.transportation.repositories.ZoneRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test,controller-test")
public class StationControllerIntegrationTest {

    @Rule
    @Autowired
    public ClearDatabaseRule clearDatabaseRule;

    @Autowired
    private TestRestTemplate adminClient;
    @Autowired
    private TestRestTemplate publicClient;
    @Autowired
    private ZoneRepository zoneRepository;
    @Autowired
    private StationRepository stationRepository;

    @PostConstruct
    public void postConstruct() {
        this.adminClient =
            this.adminClient.withBasicAuth("admin", "password");
    }

    @Before
    public void setUp() {
        Zone zone1 = this.zoneRepository.save(new Zone("Zone 1"));
        this.zoneRepository.saveAndFlush(new Zone("Zone 2"));
        this.stationRepository.save(new Station("Station 1", zone1, 30, 30));
        this.stationRepository.saveAndFlush(
            new Station("Station 2", zone1, 30,30)
        );
    }

    @Test
    public void deleteSuccess() {
        ResponseEntity<Station> response = adminClient.exchange(
            "/stations/{id}",
            HttpMethod.DELETE,
            null,
            Station.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        Station station = response.getBody();
        assertNotNull(station.getId());
        assertEquals(1L, (long) station.getId());
    }

    @Test
    public void deleteStationNotFound() {
        ResponseEntity<String> response = adminClient.exchange(
            "/stations/{id}",
            HttpMethod.DELETE,
            null,
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void deleteForbidden() {
        ResponseEntity<Station> response = publicClient.exchange(
            "/stations/{id}",
            HttpMethod.DELETE,
            null,
            Station.class,
            1
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void getSuccess() {
        ResponseEntity<Station> response = publicClient.exchange(
            "/stations/{id}",
            HttpMethod.GET,
            null,
            Station.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        Station station = response.getBody();
        assertNotNull(station.getId());
        assertEquals(1L, (long) station.getId());
    }

    @Test
    public void getStationNotFound() {
        ResponseEntity<String> response = publicClient.exchange(
            "/stations/{id}",
            HttpMethod.GET,
            null,
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void getAllSuccess() {
        ResponseEntity<List<Station>> response = publicClient.exchange(
            "/stations",
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<List<Station>>() {}
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(2, response.getBody().size());
    }

    @Test
    public void createSuccess() {
        CreateStation form = new CreateStation();
        form.name = "Station 3";
        form.zoneId = 1L;
        form.lat = 30f;
        form.lng = 30f;

        ResponseEntity<Station> response = adminClient.exchange(
            "/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Station.class
        );
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());

        Station station = response.getBody();
        assertNotNull(station.getId());
        assertEquals(3L, (long) station.getId());
    }

    @Test
    public void createZoneNotFound() {
        CreateStation form = new CreateStation();
        form.name = "Station 3";
        form.zoneId = 99L;
        form.lat = 30f;
        form.lng = 30f;

        ResponseEntity<String> response = adminClient.exchange(
            "/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void createInvalidCoordinates() {
        CreateStation form = new CreateStation();
        form.name = "Station 3";
        form.zoneId = 1L;
        form.lat = -91f;
        form.lng = 30f;

        ResponseEntity<String> response = adminClient.exchange(
            "/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());

        form.lat = 91f;
        response = adminClient.exchange(
            "/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());

        form.lng = -181f;
        response = adminClient.exchange(
            "/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());

        form.lng = 181f;
        response = adminClient.exchange(
            "/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void createForbidden() {
        CreateStation form = new CreateStation();
        form.name = "Station 3";
        form.zoneId = 1L;
        form.lat = 30f;
        form.lng = 30f;

        ResponseEntity<Station> response = publicClient.exchange(
            "/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Station.class
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void updateNameSuccess() {
        UpdateStation form = new UpdateStation();
        form.name = "New Station Name";

        ResponseEntity<Station> response = adminClient.exchange(
            "/stations/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            Station.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        Station station = response.getBody();
        assertNotNull(station.getId());
        assertEquals(1L, (long) station.getId());
        assertEquals("New Station Name", station.getName());
        assertEquals(1L, (long) station.getZone().getId());
    }

    @Test
    public void updateZoneSuccess() {
        UpdateStation form = new UpdateStation();
        form.zoneId = 2L;

        ResponseEntity<Station> response = adminClient.exchange(
            "/stations/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            Station.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        Station station = response.getBody();
        assertNotNull(station.getId());
        assertEquals(1L, (long) station.getId());
        assertEquals("Station 1", station.getName());
        assertEquals(2L, (long) station.getZone().getId());
    }

    @Test
    public void updateZoneZoneNotFound() {
        UpdateStation form = new UpdateStation();
        form.zoneId = 99L;

        ResponseEntity<String> response = adminClient.exchange(
            "/stations/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            String.class,
            3
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void updateNameAndZoneSuccess() {
        UpdateStation form = new UpdateStation();
        form.name = "New Zone Name";
        form.zoneId = 2L;

        ResponseEntity<Station> response = adminClient.exchange(
            "/stations/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            Station.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        Station station = response.getBody();
        assertNotNull(station.getId());
        assertEquals(1L, (long) station.getId());
        assertEquals("New Zone Name", station.getName());
        assertEquals(2L, (long) station.getZone().getId());
    }

    @Test
    public void updateNameAndZoneZoneNotFound() {
        UpdateStation form = new UpdateStation();
        form.name = "New Zone Name";
        form.zoneId = 99L;

        ResponseEntity<String> response = adminClient.exchange(
            "/stations/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            String.class,
            3
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void updateZoneNotFound() {
        UpdateStation form = new UpdateStation();
        form.name = "New Zone Name";
        form.zoneId = 1L;

        ResponseEntity<String> response = adminClient.exchange(
            "/stations/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }


    @Test
    public void updateForbidden() {
        UpdateStation form = new UpdateStation();
        form.name = "New Zone Name";
        form.zoneId = 2L;

        ResponseEntity<Station> response = publicClient.exchange(
            "/stations/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            Station.class,
            1
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }
}

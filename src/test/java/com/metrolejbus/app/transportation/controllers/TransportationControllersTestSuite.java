package com.metrolejbus.app.transportation.controllers;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
    JourneyControllerIntegrationTest.class,
    JourneyControllerTest.class,
    LineControllerIntegrationTest.class,
    LineControllerTest.class,
    StationControllerIntegrationTest.class,
    StationControllerTest.class,
    ZoneControllerIntegrationTest.class,
    ZoneControllerTest.class,
    ScheduleControllerTest.class
})
public class TransportationControllersTestSuite {}

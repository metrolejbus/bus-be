package com.metrolejbus.app.transportation.controllers;

import com.metrolejbus.app.ClearDatabaseRule;
import com.metrolejbus.app.transportation.forms.CreateZone;
import com.metrolejbus.app.transportation.forms.UpdateZone;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.repositories.ZoneRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test,controller-test")
public class ZoneControllerIntegrationTest {

    @Rule
    @Autowired
    public ClearDatabaseRule clearDatabaseRule;

    @Autowired
    private TestRestTemplate adminClient;
    @Autowired
    private TestRestTemplate publicClient;
    @Autowired
    private ZoneRepository zoneRepository;

    @PostConstruct
    public void postConstruct() {
        this.adminClient =
            this.adminClient.withBasicAuth("admin", "password");
    }

    @Before
    public void setUp() {
        this.zoneRepository.save(new Zone("Zone 1"));
        this.zoneRepository.saveAndFlush(new Zone("Zone 2"));
    }

    @Test
    public void getSuccess() {
        ResponseEntity<Zone> response = publicClient.getForEntity("/zones/{id}",
            Zone.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        Zone zone = response.getBody();
        assertNotNull(zone.getId());
        assertEquals(1L, (long) zone.getId());
    }

    @Test
    public void getZoneNotFound() {
        ResponseEntity<String> response = publicClient.getForEntity("/zones" +
                "/{id}",
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void getAll() {
        ResponseEntity<List<Zone>> response = publicClient.exchange("/zones",
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<List<Zone>>() {}
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());

        List<Zone> zones = response.getBody();
        assertNotNull(zones);
        assertEquals(2, zones.size());
    }

    @Test
    public void deleteSuccess() {
        ResponseEntity<Zone> response = adminClient.exchange("/zones/{id}",
            HttpMethod.DELETE,
            null,
            Zone.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        Zone zone = response.getBody();
        assertNotNull(zone.getId());
        assertEquals(1L, (long) zone.getId());
    }

    @Test
    public void deleteZoneNotFound() {
        ResponseEntity<String> response = adminClient.exchange("/zones/{id}",
            HttpMethod.DELETE,
            null,
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void deleteForbidden() {
        ResponseEntity<String> response = publicClient.exchange(
            "/zones/{id}",
            HttpMethod.DELETE,
            null,
            String.class,
            1
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void createSuccess() {
        CreateZone form = new CreateZone();
        form.name = "Zone 3";

        ResponseEntity<Zone> response = adminClient.exchange("/zones",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Zone.class
        );
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());

        Zone zone = response.getBody();
        assertNotNull(zone.getId());
        assertEquals(3L, (long) zone.getId());
    }

    @Test
    public void createNameNotAvailable() {
        CreateZone form = new CreateZone();
        form.name = "Zone 1";

        ResponseEntity<String> response = adminClient.exchange("/zones",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void createForbidden() {
        CreateZone form = new CreateZone();
        form.name = "Zone 3";

        ResponseEntity<Zone> response = publicClient.exchange(
            "/zones",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Zone.class
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void updateSuccess() {
        UpdateZone form = new UpdateZone();
        form.name = "New Zone Name";

        ResponseEntity<Zone> response = adminClient.exchange("/zones/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            Zone.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        Zone zone = response.getBody();
        assertNotNull(zone.getId());
        assertEquals(1L, (long) zone.getId());
        assertEquals("New Zone Name", zone.getName());
    }

    @Test
    public void updateNameNotAvailable() {
        UpdateZone form = new UpdateZone();
        form.name = "Zone 2";

        ResponseEntity<String> response = adminClient.exchange("/zones/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            String.class,
            1
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void updateZoneNotFound() {
        UpdateZone form = new UpdateZone();
        form.name = "Zone 2";

        ResponseEntity<String> response = adminClient.exchange("/zones/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void updateForbidden() {
        UpdateZone form = new UpdateZone();
        form.name = "New Zone Name";

        ResponseEntity<Zone> response = publicClient.exchange(
            "/zones/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            Zone.class,
            1
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

}

package com.metrolejbus.app.transportation.controllers;

import com.metrolejbus.app.transportation.exceptions.JourneyNotFound;
import com.metrolejbus.app.transportation.exceptions.LineNotFound;
import com.metrolejbus.app.transportation.forms.CreateJourney;
import com.metrolejbus.app.transportation.models.Journey;
import com.metrolejbus.app.transportation.models.Line;
import com.metrolejbus.app.transportation.models.TransportationType;
import com.metrolejbus.app.transportation.services.JourneyService;
import com.metrolejbus.app.transportation.services.LineService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test,controller-test")
public class JourneyControllerTest {

    @Autowired
    private TestRestTemplate systemClient;
    @Autowired
    private TestRestTemplate publicClient;
    @MockBean
    private JourneyService journeyService;
    @MockBean
    private LineService lineService;

    private Map<Long, Journey> mockedJourneys = new HashMap<>();
    private Map<Long, Line> mockedLines = new HashMap<>();

    @PostConstruct
    public void postConstruct() {
        this.systemClient =
            this.systemClient.withBasicAuth("system", "password");
    }

    @Before
    public void setUp() throws Exception {
        Line line1 = new Line("Line 1", TransportationType.BUS, "");
        line1.setId(1L);
        mockedLines.put(1L, line1);

        Journey journey1 = new Journey(line1,
            LocalDateTime.of(2019, 1, 1, 12, 0)
        );
        journey1.setId(1L);
        mockedJourneys.put(1L, journey1);

        Journey journey2 = new Journey(line1,
            LocalDateTime.of(2019, 1, 1, 16, 0)
        );
        journey2.setId(2L);
        mockedJourneys.put(2L, journey2);

        when(journeyService.create(anyLong(), anyString())).thenAnswer(
            invocation -> {
                long lineId = invocation.getArgument(0);
                String departureTime = invocation.getArgument(1);

                Line line = this.lineService.get(lineId);
                LocalDateTime dt = LocalDateTime.parse(departureTime,
                    DateTimeFormatter.ISO_LOCAL_DATE_TIME
                );

                long id = mockedJourneys.size();

                Journey j = new Journey(line, dt);
                j.setId(++id);
                mockedJourneys.put(id, j);
                return j;
            });

        when(journeyService.get(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            Journey j = mockedJourneys.getOrDefault(key, null);
            if (j == null) { throw new JourneyNotFound(key); }
            return j;
        });

        when(journeyService.getAll(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            return mockedJourneys.values()
                .stream()
                .filter(j -> key == j.getLine().getId())
                .collect(Collectors.toList());
        });

        when(lineService.get(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            Line l = mockedLines.getOrDefault(key, null);
            if (l == null) { throw new LineNotFound(key); }
            return l;
        });
    }

    @Test
    public void createSuccess() throws LineNotFound {
        CreateJourney form = new CreateJourney();
        form.lineId = 1L;
        form.departureTime = "2019-01-01T18:00";

        ResponseEntity<Journey> response = systemClient.exchange(
            "/journeys",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Journey.class
        );
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(journeyService, times(1)).create(anyLong(), anyString());
        verifyNoMoreInteractions(journeyService);
    }

    @Test
    public void createLineIdIsNull() {
        CreateJourney form = new CreateJourney();
        form.departureTime = "2019-01-01T18:00";

        ResponseEntity<String> response = systemClient.exchange(
            "/journeys",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());

        verifyZeroInteractions(journeyService);
    }


    @Test
    public void createDepartureTimeInvalidFormat() {
        CreateJourney form = new CreateJourney();
        form.lineId = 1L;
        form.departureTime = "2019-01-01T18-00";

        ResponseEntity<String> response = systemClient.exchange(
            "/journeys",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());

        //TODO(aleksandar): find out if date checking is possible on json parsing level
//        verifyZeroInteractions(journeyService);
    }

    @Test
    public void createForbidden() {
        CreateJourney form = new CreateJourney();
        form.lineId = 1L;
        form.departureTime = "2019-01-01T18:00";

        ResponseEntity<Journey> response = publicClient.exchange(
            "/journeys",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Journey.class
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void getSuccess() throws JourneyNotFound {
        ResponseEntity<Journey> response = publicClient.exchange(
            "/journeys" +
                "/{id}",
            HttpMethod.GET,
            null,
            Journey.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(journeyService, times(1)).get(anyLong());
        verifyNoMoreInteractions(journeyService);
    }

    @Test
    public void getJourneyNotFound() throws JourneyNotFound {
        ResponseEntity<String> response = publicClient.exchange(
            "/journeys/{id}",
            HttpMethod.GET,
            null,
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(journeyService, times(1)).get(anyLong());
        verifyNoMoreInteractions(journeyService);
    }

    //TODO(aleksandar): setup redis store, revise storing locations and write
    // these tests
    @Test
    public void getLocation() {
    }

    @Test
    public void updateLocation() {
    }

}

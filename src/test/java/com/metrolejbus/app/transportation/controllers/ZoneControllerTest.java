package com.metrolejbus.app.transportation.controllers;

import com.metrolejbus.app.transportation.exceptions.ZoneNameNotAvailable;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.forms.CreateZone;
import com.metrolejbus.app.transportation.forms.UpdateZone;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.services.ZoneService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test,controller-test")
public class ZoneControllerTest {

    @Autowired
    private TestRestTemplate adminClient;
    @Autowired
    private TestRestTemplate publicClient;
    @MockBean
    private ZoneService zoneService;

    private Map<Long, Zone> mockedZones = new HashMap<>();

    @PostConstruct
    public void postConstruct() {
        this.adminClient =
            this.adminClient.withBasicAuth("admin", "password");
    }

    @Before
    public void setUp() throws Exception {
        mockedZones.clear();

        Zone zone = new Zone("Zone 1");
        zone.setId(1L);
        mockedZones.put(1L, zone);

        Zone zone2 = new Zone("Zone 2");
        zone2.setId(2L);
        mockedZones.put(2L, zone2);

        when(zoneService.create(anyString())).thenAnswer(invocation -> {
            String name = invocation.getArgument(0);
            if (name.equals("Zone 1") || name.equals("Zone 2")) {
                throw new ZoneNameNotAvailable(name);
            }
            Zone z = new Zone(name);
            z.setId(3L);
            return z;
        });

        when(zoneService.get(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            Zone z = mockedZones.getOrDefault(key, null);
            if (z == null) {
                throw new ZoneNotFound(key);
            }
            return z;
        });

        when(zoneService.getAll()).thenReturn(new LinkedList<>(mockedZones.values()));

        when(zoneService.delete(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            Zone z = mockedZones.getOrDefault(key, null);
            if (z == null) {
                throw new ZoneNotFound(key);
            }
            z.setActive(false);
            return z;
        });

        when(zoneService.update(anyLong(),
            anyString()
        )).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            Zone z = mockedZones.getOrDefault(key, null);
            if (z == null) {
                throw new ZoneNotFound(key);
            }
            String newName = invocation.getArgument(1);
            if (newName.equals("Zone 1") || newName.equals("Zone 2")) {
                throw new ZoneNameNotAvailable(newName);
            }
            z.setName(newName);
            return z;
        });
    }

    @Test
    public void getSuccess() throws ZoneNotFound {
        ResponseEntity<Zone> response = publicClient.getForEntity(
            "/zones/{id}",
            Zone.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        Zone zone = response.getBody();
        assertNotNull(zone.getId());
        assertEquals(1L, (long) zone.getId());

        verify(zoneService, times(1)).get(anyLong());
        verifyNoMoreInteractions(zoneService);
    }

    @Test
    public void getZoneNotFound() {
        ResponseEntity<String> response = publicClient.getForEntity(
            "/zones/{id}",
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void getAll() {
        ResponseEntity<List<Zone>> response = publicClient.exchange(
            "/zones",
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<List<Zone>>() {}
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());

        List<Zone> zones = response.getBody();
        assertNotNull(zones);
        assertEquals(2, zones.size());

        verify(zoneService, times(1)).getAll();
        verifyNoMoreInteractions(zoneService);
    }

    @Test
    public void deleteSuccess() throws ZoneNotFound {
        ResponseEntity<Zone> response = adminClient.exchange(
            "/zones/{id}",
            HttpMethod.DELETE,
            null,
            Zone.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        Zone zone = response.getBody();
        assertNotNull(zone.getId());
        assertEquals(1L, (long) zone.getId());

        verify(zoneService, times(1)).delete(anyLong());
        verifyNoMoreInteractions(zoneService);
    }

    @Test
    public void deleteZoneNotFound() {
        ResponseEntity<String> response = adminClient.exchange(
            "/zones/{id}",
            HttpMethod.DELETE,
            null,
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void deleteForbidden() {
        ResponseEntity<String> response = publicClient.exchange(
            "/zones/{id}",
            HttpMethod.DELETE,
            null,
            String.class,
            1
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void createSuccess() throws ZoneNameNotAvailable {
        CreateZone form = new CreateZone();
        form.name = "Zone 3";

        ResponseEntity<Zone> response = adminClient.exchange(
                "/zones",
                HttpMethod.POST,
                new HttpEntity<>(form),
                Zone.class
            );
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());

        Zone zone = response.getBody();
        assertNotNull(zone.getId());
        assertEquals(3L, (long) zone.getId());

        verify(zoneService, times(1)).create(anyString());
        verifyNoMoreInteractions(zoneService);
    }

    @Test
    public void createNameNotAvailable() throws ZoneNameNotAvailable {
        CreateZone form = new CreateZone();
        form.name = "Zone 1";

        ResponseEntity<String> response = adminClient.exchange(
            "/zones",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(zoneService, times(1)).create(anyString());
        verifyNoMoreInteractions(zoneService);
    }

    @Test
    public void createForbidden() {
        CreateZone form = new CreateZone();
        form.name = "Zone 3";

        ResponseEntity<Zone> response = publicClient.exchange(
            "/zones",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Zone.class
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void updateSuccess() throws ZoneNameNotAvailable, ZoneNotFound {
        UpdateZone form = new UpdateZone();
        form.name = "New Zone Name";

        ResponseEntity<Zone> response = adminClient.exchange(
            "/zones/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            Zone.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());

        Zone zone = response.getBody();
        assertNotNull(zone.getId());
        assertEquals(1L, (long) zone.getId());
        assertEquals("New Zone Name", zone.getName());

        verify(zoneService, times(1)).update(anyLong(), anyString());
        verifyNoMoreInteractions(zoneService);
    }

    @Test
    public void updateNameNotAvailable()
        throws ZoneNameNotAvailable, ZoneNotFound
    {
        UpdateZone form = new UpdateZone();
        form.name = "Zone 2";

        ResponseEntity<String> response = adminClient.exchange(
            "/zones/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            String.class,
            1
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(zoneService, times(1)).update(anyLong(), anyString());
        verifyNoMoreInteractions(zoneService);
    }

    @Test
    public void updateZoneNotFound() throws ZoneNameNotAvailable, ZoneNotFound {
        UpdateZone form = new UpdateZone();
        form.name = "Zone 2";

        ResponseEntity<String> response = adminClient.exchange(
            "/zones/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());

        verify(zoneService, times(1)).update(anyLong(), anyString());
        verifyNoMoreInteractions(zoneService);
    }

    @Test
    public void updateForbidden() {
        UpdateZone form = new UpdateZone();
        form.name = "New Zone Name";

        ResponseEntity<Zone> response = publicClient.exchange(
            "/zones/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            Zone.class,
            1
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

}

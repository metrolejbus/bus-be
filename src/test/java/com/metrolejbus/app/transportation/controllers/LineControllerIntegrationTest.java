package com.metrolejbus.app.transportation.controllers;

import com.metrolejbus.app.ClearDatabaseRule;
import com.metrolejbus.app.transportation.forms.*;
import com.metrolejbus.app.transportation.models.*;
import com.metrolejbus.app.transportation.repositories.JourneyRepository;
import com.metrolejbus.app.transportation.repositories.LineRepository;
import com.metrolejbus.app.transportation.repositories.StationRepository;
import com.metrolejbus.app.transportation.repositories.ZoneRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test,controller-test")
public class LineControllerIntegrationTest {

    @Rule
    @Autowired
    public ClearDatabaseRule clearDatabaseRule;

    @Autowired
    private TestRestTemplate adminClient;
    @Autowired
    private TestRestTemplate publicClient;
    @Autowired
    private ZoneRepository zoneRepository;
    @Autowired
    private StationRepository stationRepository;
    @Autowired
    private LineRepository lineRepository;
    @Autowired
    private JourneyRepository journeyRepository;

    @PostConstruct
    public void postConstruct() {
        this.adminClient =
            this.adminClient.withBasicAuth("admin", "password");
    }

    @Before
    public void setUp() {
        Zone zone1 = this.zoneRepository.save(new Zone("Zone 1"));

        Station station1 = this.stationRepository.save(
            new Station("Station 1", zone1, 30, 30)
        );

        Station station2 = this.stationRepository.save(
            new Station("Station 2", zone1, 30, 30)
        );

        Station station3 = this.stationRepository.save(
            new Station("Station 3", zone1, 30, 30)
        );

        Line line1 = this.lineRepository.save(
            new Line("Line 1", TransportationType.BUS, "")
        );
        line1.getStations().add(station1);
        this.lineRepository.save(line1);

        this.lineRepository.save(new Line("Line 2", TransportationType.METRO, ""));

        Line line3 = this.lineRepository.save(
            new Line("Line 3", TransportationType.BUS, "")
        );
        line3.getStations().add(station1);
        this.lineRepository.save(line3);

        Line line4 = this.lineRepository.save(
            new Line("Line 4", TransportationType.BUS, "")
        );
        line4.getStations().add(station1);
        line4.getStations().add(station2);
        line4.getStations().add(station3);
        this.lineRepository.save(line4);

        this.journeyRepository.save(
            new Journey(line1, LocalDateTime.of(2019, 1, 1, 12, 0))
        );
        this.journeyRepository.saveAndFlush(
            new Journey(line1, LocalDateTime.of(2019, 1, 1, 16, 0))
        );
    }

    @Test
    public void createSuccess() {
        CreateLine form = new CreateLine();
        form.name = "Line 3";
        form.type = TransportationType.TRAM;
        form.route = "Route";

        ResponseEntity<Line> response = adminClient.exchange(
            "/lines",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Line.class
        );
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void createNameIsNull() {
        CreateLine form = new CreateLine();
        form.type = TransportationType.TRAM;
        form.route = "Route";

        ResponseEntity<String> response = adminClient.exchange(
            "/lines",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void createNameIsBlank() {
        CreateLine form = new CreateLine();
        form.name = "";
        form.type = TransportationType.TRAM;
        form.route = "Route";

        ResponseEntity<String> response = adminClient.exchange(
            "/lines",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void createTypeIsNull() {
        CreateLine form = new CreateLine();
        form.name = "Line 3";
        form.route = "Route";

        ResponseEntity<String> response = adminClient.exchange(
            "/lines",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void createRouteIsNull() {
        CreateLine form = new CreateLine();
        form.name = "Line 3";
        form.type = TransportationType.BUS;

        ResponseEntity<String> response = adminClient.exchange(
            "/lines",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void createRouteIsBlank() {
        CreateLine form = new CreateLine();
        form.name = "Line 3";
        form.type = TransportationType.BUS;
        form.route = "";

        ResponseEntity<String> response = adminClient.exchange(
            "/lines",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void createForbidden() {
        CreateLine form = new CreateLine();
        form.name = "Line 3";
        form.type = TransportationType.TRAM;
        form.route = "Route";

        ResponseEntity<Line> response = publicClient.exchange(
            "/lines",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Line.class
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void getSuccess() {
        ResponseEntity<Line> response = publicClient.exchange(
            "/lines/{id}",
            HttpMethod.GET,
            null,
            Line.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void getLineNotFound() {
        ResponseEntity<String> response = publicClient.exchange(
            "/lines/{id}",
            HttpMethod.GET,
            null,
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void getAllSuccess() {
        ResponseEntity<List<Line>> response = publicClient.exchange(
            "/lines",
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<List<Line>>() {}
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(4, response.getBody().size());

        response = publicClient.exchange(
            "/lines?station={stationId}",
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<List<Line>>() {},
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(3, response.getBody().size());

        response = publicClient.exchange(
            "/lines?station={stationId}",
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<List<Line>>() {},
            99
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(0, response.getBody().size());
    }

    @Test
    public void deleteSuccess() {
        ResponseEntity<Line> response = adminClient.exchange(
            "/lines/{id}",
            HttpMethod.DELETE,
            null,
            Line.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertFalse(response.getBody().isActive());
    }

    @Test
    public void deleteLineNotFound() {
        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}",
            HttpMethod.DELETE,
            null,
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void deleteForbidden() {
        ResponseEntity<Line> response = publicClient.exchange(
            "/lines/{id}",
            HttpMethod.DELETE,
            null,
            Line.class,
            1
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
        assertFalse(response.getBody().isActive());
    }

    @Test
    public void updateSuccess() {
        UpdateLine form = new UpdateLine();
        form.name = "Line 3";

        ResponseEntity<Line> response = adminClient.exchange(
            "/lines/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            Line.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void updateNameIsBlank() {
        UpdateLine form = new UpdateLine();
        form.name = "";

        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            String.class,
            3
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void updateForbidden() {
        UpdateLine form = new UpdateLine();
        form.name = "Line 3";

        ResponseEntity<Line> response = publicClient.exchange(
            "/lines/{id}",
            HttpMethod.PUT,
            new HttpEntity<>(form),
            Line.class,
            1
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void addStationSuccess() {
        AddStationToLine form = new AddStationToLine();
        form.stationId = 1L;

        ResponseEntity<Line> response = adminClient.exchange(
            "/lines/{id}/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Line.class,
            2
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(1, response.getBody().getStations().size());

        response = adminClient.exchange(
            "/lines/{id}/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Line.class,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(1, response.getBody().getStations().size());
    }

    @Test
    public void addStationLineNotFound() {
        AddStationToLine form = new AddStationToLine();
        form.stationId = 2L;

        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void addStationStationNotFound() {
        AddStationToLine form = new AddStationToLine();
        form.stationId = 99L;

        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class,
            3
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void addStationStationIdIsNull() {
        AddStationToLine form = new AddStationToLine();

        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class,
            3
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void addStationForbidden() {
        AddStationToLine form = new AddStationToLine();
        form.stationId = 1L;

        ResponseEntity<Line> response = publicClient.exchange(
            "/lines/{id}/stations",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Line.class,
            2
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void addStationsSuccess() {
        AddStationsToLine form = new AddStationsToLine();
        form.stationIds = Arrays.asList(1L, 2L, 3L);

        ResponseEntity<Line> response = adminClient.exchange(
            "/lines/{id}/stations/bulk",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Line.class,
            2
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());

        Line line = response.getBody();
        assertNotNull(line);
        assertEquals(3, line.getStations().size());
    }

    @Test
    public void addStationsLineNotFound() {
        AddStationsToLine form = new AddStationsToLine();
        form.stationIds = Arrays.asList(1L, 2L, 3L);

        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}/stations/bulk",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void addStationsStationNotFound() {
        AddStationsToLine form = new AddStationsToLine();
        form.stationIds = Arrays.asList(1L, 99L, 3L);

        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}/stations/bulk",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class,
            2
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void addStationsForbidden() {
        AddStationsToLine form = new AddStationsToLine();
        form.stationIds = Arrays.asList(1L, 2L, 3L);

        ResponseEntity<String> response = publicClient.exchange(
            "/lines/{id}/stations/bulk",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class,
            2
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void getStationsSuccess() {
        ResponseEntity<Set<Station>> response = publicClient.exchange(
            "/lines/{id}/stations",
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<Set<Station>>() {},
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(1, response.getBody().size());
    }

    @Test
    public void getStationsLineNotFound() {
        ResponseEntity<String> response = publicClient.exchange(
            "/lines/{id}/stations",
            HttpMethod.GET,
            null,
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void removeStationSuccess() {
        ResponseEntity<Line> response = adminClient.exchange(
            "/lines/{id}/stations/{stationId}",
            HttpMethod.DELETE,
            null,
            Line.class,
            1,
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(0, response.getBody().getStations().size());
    }

    @Test
    public void removeStationLineNotFound() {
        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}/stations/{stationId}",
            HttpMethod.DELETE,
            null,
            String.class,
            99,
            1
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void removeStationStationNotFound() {
        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}/stations/{stationId}",
            HttpMethod.DELETE,
            null,
            String.class,
            1,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void removeStationForbidden() {
        ResponseEntity<Line> response = publicClient.exchange(
            "/lines/{id}/stations/{stationId}",
            HttpMethod.DELETE,
            null,
            Line.class,
            1,
            1
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void removeStationsSuccess() {
        RemoveStationsFromLine form = new RemoveStationsFromLine();
        form.stationIds = Arrays.asList(1L, 2L);

        ResponseEntity<Line> response = adminClient.exchange(
            "/lines/{id}/stations/bulk",
            HttpMethod.DELETE,
            new HttpEntity<>(form),
            Line.class,
            4
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());

        Line line = response.getBody();
        assertNotNull(line);
        assertEquals(1, line.getStations().size());
    }

    @Test
    public void removeStationsLineNotFound() {
        RemoveStationsFromLine form = new RemoveStationsFromLine();
        form.stationIds = Arrays.asList(1L, 2L);

        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}/stations/bulk",
            HttpMethod.DELETE,
            new HttpEntity<>(form),
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void removeStationsStationNotFound() {
        RemoveStationsFromLine form = new RemoveStationsFromLine();
        form.stationIds = Arrays.asList(1L, 99L);

        ResponseEntity<String> response = adminClient.exchange(
            "/lines/{id}/stations/bulk",
            HttpMethod.DELETE,
            new HttpEntity<>(form),
            String.class,
            4
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void removeStationsForbidden() {
        RemoveStationsFromLine form = new RemoveStationsFromLine();
        form.stationIds = Arrays.asList(1L, 2L);

        ResponseEntity<String> response = publicClient.exchange(
            "/lines/{id}/stations/bulk",
            HttpMethod.DELETE,
            new HttpEntity<>(form),
            String.class,
            4
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void getJourneysSuccess() {
        ResponseEntity<List<Journey>> response = publicClient.exchange(
            "/lines/{id}/journeys",
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<List<Journey>>() {},
            1
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(2, response.getBody().size());
    }

    @Test
    public void getJourneysLineNotFound() {
        ResponseEntity<String> response = publicClient.exchange(
            "/lines/{id}/journeys",
            HttpMethod.GET,
            null,
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

}

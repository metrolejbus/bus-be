package com.metrolejbus.app.transportation.controllers;

import com.metrolejbus.app.ClearDatabaseRule;
import com.metrolejbus.app.transportation.forms.CreateJourney;
import com.metrolejbus.app.transportation.models.Journey;
import com.metrolejbus.app.transportation.models.Line;
import com.metrolejbus.app.transportation.models.TransportationType;
import com.metrolejbus.app.transportation.repositories.JourneyRepository;
import com.metrolejbus.app.transportation.repositories.LineRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test,controller-test")
public class JourneyControllerIntegrationTest {

    @Rule
    @Autowired
    public ClearDatabaseRule clearDatabaseRule;

    @Autowired
    private TestRestTemplate systemClient;
    @Autowired
    private TestRestTemplate publicClient;
    @Autowired
    private LineRepository lineRepository;
    @Autowired
    private JourneyRepository journeyRepository;

    @PostConstruct
    public void postConstruct() {
        this.systemClient =
            this.systemClient.withBasicAuth("system", "password");
    }

    @Before
    public void setUp() {
        Line line1 = this.lineRepository.save(
            new Line("Line 1", TransportationType.BUS, "")
        );
        this.journeyRepository.save(new Journey(line1,
            LocalDateTime.of(2019, 1, 1, 12, 0)
        ));
        this.journeyRepository.saveAndFlush(new Journey(line1,
            LocalDateTime.of(2019, 1, 1, 16, 0)
        ));
    }

    @Test
    public void createSuccess() {
        CreateJourney form = new CreateJourney();
        form.lineId = 1L;
        form.departureTime = "2019-01-01T18:00";

        ResponseEntity<Journey> response = systemClient.exchange(
            "/journeys",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Journey.class
        );
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void createLineIdIsNull() {
        CreateJourney form = new CreateJourney();
        form.departureTime = "2019-01-01T18:00";

        ResponseEntity<String> response = systemClient.exchange(
            "/journeys",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
    }


    @Test
    public void createDepartureTimeInvalidFormat() {
        CreateJourney form = new CreateJourney();
        form.lineId = 1L;
        form.departureTime = "2019-01-01T18-00";

        ResponseEntity<String> response = systemClient.exchange(
            "/journeys",
            HttpMethod.POST,
            new HttpEntity<>(form),
            String.class
        );
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void createForbidden() {
        CreateJourney form = new CreateJourney();
        form.lineId = 1L;
        form.departureTime = "2019-01-01T18:00";

        ResponseEntity<Journey> response = publicClient.exchange(
            "/journeys",
            HttpMethod.POST,
            new HttpEntity<>(form),
            Journey.class
        );
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void getSuccess() {
        ResponseEntity<Journey> response = publicClient.exchange(
            "/journeys/{id}",
            HttpMethod.GET,
            null,
            Journey.class,
            2
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    public void getJourneyNotFound() {
        ResponseEntity<String> response = publicClient.exchange(
            "/journeys/{id}",
            HttpMethod.GET,
            null,
            String.class,
            99
        );
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    //TODO(aleksandar): setup redis store, revise storing locations and write
    // these tests
    @Test
    public void getLocation() {
    }

    @Test
    public void updateLocation() {
    }

}

package com.metrolejbus.app.transportation.services;

import com.metrolejbus.app.ClearDatabaseRule;
import com.metrolejbus.app.transportation.exceptions.JourneyNotFound;
import com.metrolejbus.app.transportation.exceptions.LineNotFound;
import com.metrolejbus.app.transportation.models.Journey;
import com.metrolejbus.app.transportation.models.Line;
import com.metrolejbus.app.transportation.models.TransportationType;
import com.metrolejbus.app.transportation.repositories.JourneyRepository;
import com.metrolejbus.app.transportation.repositories.LineRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class JourneyServiceIntegrationTest {

    @Rule
    @Autowired
    public ClearDatabaseRule clearDatabaseRule;

    @Autowired
    private JourneyRepository journeyRepository;

    @Autowired
    private LineRepository lineRepository;

    @Autowired
    private JourneyService journeyService;

    @Before
    public void setUp() {
        Line line = this.lineRepository.saveAndFlush(
            new Line("Line 1", TransportationType.BUS, "")
        );
        this.journeyRepository.save(
            new Journey(line, LocalDateTime.of(2019, 1, 1, 12, 0))
        );
        this.journeyRepository.saveAndFlush(
            new Journey(line, LocalDateTime.of(2019, 6, 4, 8, 0))
        );
    }

    @Test
    public void createSuccess() throws LineNotFound {
        Journey j = this.journeyService.create(1L, "2019-01-01T12:00:00");
        assertNotNull(j);
        assertNotNull(j.getId());
        assertEquals(3L, (long) j.getId());
    }

    @Test(expected = LineNotFound.class)
    public void createLineNotFound() throws LineNotFound {
        this.journeyService.create(99L, "2019-01-01T12:00:00");
    }

    @Test
    public void getSuccess() throws JourneyNotFound {
        Journey j = this.journeyService.get(2L);
        assertNotNull(j);
        assertNotNull(j.getId());
        assertEquals(2L, (long) j.getId());
    }

    @Test(expected = JourneyNotFound.class)
    public void getJourneyNotFound() throws JourneyNotFound {
        this.journeyService.get(99L);
    }

    @Test
    public void getAllSuccess() {
        List<Journey> journeys = this.journeyService.getAll(1L);
        assertNotNull(journeys);
        assertEquals(2, journeys.size());
    }

}

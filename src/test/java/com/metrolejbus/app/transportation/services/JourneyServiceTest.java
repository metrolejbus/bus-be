package com.metrolejbus.app.transportation.services;

import com.metrolejbus.app.transportation.exceptions.JourneyNotFound;
import com.metrolejbus.app.transportation.exceptions.LineNotFound;
import com.metrolejbus.app.transportation.models.Journey;
import com.metrolejbus.app.transportation.models.Line;
import com.metrolejbus.app.transportation.models.TransportationType;
import com.metrolejbus.app.transportation.repositories.JourneyRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class JourneyServiceTest {

    @MockBean
    private JourneyRepository journeyRepository;

    @MockBean
    private LineService lineService;

    @Autowired
    private JourneyService journeyService;

    private Map<Long, Journey> mockedJourneys = new HashMap<>();

    @Before
    public void setUp() throws LineNotFound {
        mockedJourneys.clear();

        Line line = new Line("Line 1", TransportationType.BUS, "");
        line.setId(1L);

        Journey journey = new Journey(line,
            LocalDateTime.of(2019, 1, 1, 12, 0)
        );
        journey.setId(1L);
        mockedJourneys.put(1L, journey);

        Journey journey2 = new Journey(line,
            LocalDateTime.of(2019, 6, 4, 8, 0)
        );
        journey2.setId(2L);
        mockedJourneys.put(2L, journey);

        when(journeyRepository.findById(any(Long.class))).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            Journey j = mockedJourneys.getOrDefault(key, null);
            return Optional.ofNullable(j);
        });

        when(journeyRepository.findAllByLine_Id(any(Long.class))).thenAnswer(
            invocation -> {
                long key = invocation.getArgument(0);
                return mockedJourneys.values()
                    .stream()
                    .filter(j -> j.getLine().getId() == key)
                    .collect(Collectors.toList());
            });

        when(journeyRepository.save(any(Journey.class))).thenAnswer(invocation -> {
            Journey j = invocation.getArgument(0);

            if (j.getId() == null) {
                long key = mockedJourneys.keySet().size();
                j.setId(++key);
                mockedJourneys.put(++key, j);
            } else {
                mockedJourneys.put(j.getId(), j);
            }

            return j;
        });

        when(lineService.get(1L)).thenReturn(line);
        when(lineService.get(99L)).thenThrow(new LineNotFound(99L));
    }

    @Test
    public void createSuccess() throws LineNotFound {
        Journey j = this.journeyService.create(1L, "2019-01-01T12:00:00");
        assertNotNull(j);
        assertNotNull(j.getId());
        assertEquals(3L, (long) j.getId());

        verify(journeyRepository, times(1)).save(any(Journey.class));
        verifyNoMoreInteractions(journeyRepository);

        verify(lineService, times(1)).get(anyLong());
        verifyNoMoreInteractions(lineService);
    }

    @Test(expected = LineNotFound.class)
    public void createLineNotFound() throws LineNotFound {
        this.journeyService.create(99L, "2019-01-01T12:00:00");
    }

    @Test
    public void getSuccess() throws JourneyNotFound {
        Journey j = this.journeyService.get(1L);
        assertNotNull(j);
        assertNotNull(j.getId());
        assertEquals(1L, (long) j.getId());

        verify(journeyRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(journeyRepository);
    }

    @Test(expected = JourneyNotFound.class)
    public void getJourneyNotFound() throws JourneyNotFound {
        this.journeyService.get(99L);
    }

    @Test
    public void getAllSuccess() {
        List<Journey> journeys = this.journeyService.getAll(1L);
        assertNotNull(journeys);
        assertEquals(2, journeys.size());

        verify(journeyRepository, times(1)).findAllByLine_Id(anyLong());
        verifyNoMoreInteractions(journeyRepository);
    }

}

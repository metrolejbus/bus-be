package com.metrolejbus.app.transportation.services;

import com.metrolejbus.app.ClearDatabaseRule;
import com.metrolejbus.app.transportation.exceptions.StationNotFound;
import com.metrolejbus.app.transportation.exceptions.ZoneNameNotAvailable;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Station;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.repositories.StationRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static junit.framework.TestCase.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class StationServiceIntegrationTest {

    @Rule
    @Autowired
    public ClearDatabaseRule clearDatabaseRule;

    @Autowired
    private ZoneService zoneService;

    @Autowired
    private StationService stationService;

    @Autowired
    private StationRepository stationRepository;

    @Before
    public void setUp() throws ZoneNameNotAvailable {
        Zone z = this.zoneService.create("Zone");
        this.zoneService.create("Zone 2");
        stationRepository.saveAndFlush(new Station("Station", z, 30, 30));
    }

    @Test
    public void createSuccess() throws ZoneNotFound {
        Station s = this.stationService.create("Station 2", 1, 30, 45);
        assertNotNull(s);
        assertNotNull(s.getId());
        assertEquals(2L, (long) s.getId());
        assertEquals("Station 2", s.getName());
        assertEquals(30f, s.getLocation().getLat());
        assertEquals(45f, s.getLocation().getLng());
    }

    @Test(expected = ZoneNotFound.class)
    public void createZoneNotFound() throws ZoneNotFound {
        this.stationService.create("Station 2", 99L, 30, 45);
    }

    @Test(expected = NullPointerException.class)
    public void createNameIsNull() throws ZoneNotFound {
        this.stationService.create(null, 1L, 30, 45);
    }

    @Test
    public void deleteSuccess() throws StationNotFound {
        Station deletedStation = this.stationService.delete(1L);
        assertNotNull(deletedStation);
        assertFalse(deletedStation.isActive());
    }

    @Test(expected = StationNotFound.class)
    public void deleteStationNotFound() throws StationNotFound {
        this.stationService.delete(99L);
    }

    @Test
    public void getSuccess() throws StationNotFound {
        Station s = this.stationService.get(1L);
        assertNotNull(s);
        assertEquals(1L, (long) s.getId());
    }

    @Test
    public void getAllSuccess() {
        List<Station> stations = this.stationService.getAll();
        assertNotNull(stations);
        assertEquals(1, stations.size());
    }

    @Test(expected = StationNotFound.class)
    public void getStationNotFound() throws StationNotFound {
        this.stationService.get(99L);
    }

    @Test
    public void updateNameSuccess() throws StationNotFound {
        Station s = this.stationService.update(1L, "New Station Name");
        assertNotNull(s);
        assertEquals("New Station Name", s.getName());
    }

    @Test
    public void updateZoneSuccess() throws ZoneNotFound, StationNotFound {
        Station s = this.stationService.update(1L, 2L);
        assertNotNull(s);
        assertEquals(2L, (long) s.getZone().getId());
        assertEquals("Zone 2", s.getZone().getName());
    }

    @Test
    public void updateNameAndZoneSuccess() throws ZoneNotFound, StationNotFound
    {
        Station s = this.stationService.update(1L, "New Station Name", 2L);
        assertNotNull(s);
        assertEquals("New Station Name", s.getName());
        assertEquals(2L, (long) s.getZone().getId());
        assertEquals("Zone 2", s.getZone().getName());
    }

    @Test(expected = StationNotFound.class)
    public void updateStationNotFound() throws StationNotFound {
        this.stationService.update(99L, "New Station Name");
    }

}

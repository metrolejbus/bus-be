package com.metrolejbus.app.transportation.services;

import com.metrolejbus.app.ClearDatabaseRule;
import com.metrolejbus.app.transportation.exceptions.LineNotFound;
import com.metrolejbus.app.transportation.exceptions.StationNotFound;
import com.metrolejbus.app.transportation.models.Line;
import com.metrolejbus.app.transportation.models.Station;
import com.metrolejbus.app.transportation.models.TransportationType;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.repositories.LineRepository;
import com.metrolejbus.app.transportation.repositories.StationRepository;
import com.metrolejbus.app.transportation.repositories.ZoneRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class LineServiceIntegrationTest {

    @Rule
    @Autowired
    public ClearDatabaseRule clearDatabaseRule;

    @Autowired
    public LineService lineService;

    @Autowired
    public LineRepository lineRepository;

    @Autowired
    public ZoneRepository zoneRepository;

    @Autowired
    public StationRepository stationRepository;

    @Before
    public void setUp() {
        Zone zone = zoneRepository.saveAndFlush(new Zone("Zone 1"));

        Station station = stationRepository.save(
            new Station("Station 1", zone, 30, 30)
        );
        Station station2 = stationRepository.save(
            new Station("Station 2", zone, 30, 30)
        );
        Station station3 = stationRepository.save(
            new Station("Station 3", zone, 30, 30)
        );

        Line line1 = lineRepository.save(
            new Line("Line 1", TransportationType.BUS, "")
        );
        line1.getStations().add(station);
        lineRepository.save(line1);

        lineRepository.save(
            new Line("Line 2", TransportationType.METRO, "")
        );

        Line line3 = lineRepository.save(
            new Line("Line 3", TransportationType.TRAM, "")
        );
        line3.getStations().add(station);
        lineRepository.save(line3);


        Line line4 = lineRepository.save(
            new Line("Line 4", TransportationType.TRAM, "")
        );
        line4.getStations().add(station);
        line4.getStations().add(station2);
        line4.getStations().add(station3);
        lineRepository.saveAndFlush(line4);
    }

    @Test
    public void createSuccess() {
        Line line = this.lineService.create(
            "Line 4", TransportationType.BUS, ""
        );
        assertNotNull(line);
        assertNotNull(line.getId());
        assertEquals(5L, (long) line.getId());
        assertEquals("Line 4", line.getName());
        assertEquals(TransportationType.BUS, line.getTransportationType());

        line = this.lineService.create("Line 5", TransportationType.METRO, "");
        assertEquals(6L, (long) line.getId());
        assertEquals("Line 5", line.getName());
        assertEquals(TransportationType.METRO, line.getTransportationType());

        line = this.lineService.create("Line 6", TransportationType.TRAM, "");
        assertEquals(7L, (long) line.getId());
        assertEquals("Line 6", line.getName());
        assertEquals(TransportationType.TRAM, line.getTransportationType());
    }

    @Test(expected = NullPointerException.class)
    public void createNameIsNull() {
        this.lineService.create(null, TransportationType.BUS, "");
    }

    @Test(expected = NullPointerException.class)
    public void createTransportationTypeIsNull() {
        this.lineService.create("Line 5", null, "");
    }

    @Test(expected = NullPointerException.class)
    public void createRouteIsNull() {
        this.lineService.create("Line 5", TransportationType.BUS, null);
    }

    @Test
    public void getSuccess() throws LineNotFound {
        Line line = this.lineService.get(1L);
        assertNotNull(line);
        assertEquals(1L, (long) line.getId());

        line = this.lineService.get(3L);
        assertNotNull(line);
        assertEquals(3L, (long) line.getId());
    }

    @Test(expected = LineNotFound.class)
    public void getLineNotFound() throws LineNotFound {
        this.lineService.get(99L);
    }

    @Test
    public void getAllSuccess() {
        List<Line> lines = this.lineService.getAll();
        assertNotNull(lines);
        assertEquals(4, lines.size());

        lines = this.lineService.getAll(1L);
        assertEquals(3, lines.size());

        lines = this.lineService.getAll(2L);
        assertEquals(1, lines.size());
    }

    @Test
    public void deleteSuccess() throws LineNotFound {
        Line line = this.lineService.delete(1L);
        assertNotNull(line);
        assertFalse(line.isActive());
    }

    @Test(expected = LineNotFound.class)
    public void deleteLineNotFound() throws LineNotFound {
        this.lineService.delete(99L);
    }

    @Test
    public void updateSuccess() throws LineNotFound {
        Line line = this.lineService.update(1L, "New Line Name");
        assertNotNull(line);
        assertEquals("New Line Name", line.getName());

        line = this.lineService.update(1L, "New Line Name", "New Route");
        assertNotNull(line);
        assertEquals("New Route", line.getRoute());
    }

    @Test(expected = LineNotFound.class)
    public void updateLineNotFound() throws LineNotFound {
        this.lineService.update(99L, "New Line Name");
    }

    @Test(expected = NullPointerException.class)
    public void updateNameIsNull() throws LineNotFound {
        this.lineService.update(1L, null);
    }

    @Test(expected = NullPointerException.class)
    public void updateRouteIsNull() throws LineNotFound {
        this.lineService.update(1L, "New Line Name", null);
    }

    @Test
    public void addStationSuccess() throws StationNotFound, LineNotFound {
        Line line = this.lineService.addStation(2L, 1L);
        assertNotNull(line);
        assertEquals(1, line.getStations().size());

        Zone zone = new Zone("Zone 1");
        Station station = new Station("Station 1", zone, 30, 30);
        station.setId(1L);

        assertTrue(line.getStations().contains(station));

        // If adding station that is already associated with line, nothing
        // should be changed
        line = this.lineService.addStation(1L, 1L);
        assertNotNull(line);
        assertEquals(1, line.getStations().size());
    }


    @Test(expected = LineNotFound.class)
    public void addStationLineNotFound() throws StationNotFound, LineNotFound {
        this.lineService.addStation(99L, 1L);
    }

    @Test(expected = StationNotFound.class)
    public void addStationStationNotFound() throws StationNotFound, LineNotFound
    {
        this.lineService.addStation(1L, 99L);
    }

    @Test
    public void addStationsSuccess() throws LineNotFound, StationNotFound {
        List<Long> stationIds = Arrays.asList(1L, 2L, 3L);
        Line line = this.lineService.addStations(2L, stationIds);
        assertNotNull(line);
        assertEquals(3, line.getStations().size());
    }

    @Test(expected = LineNotFound.class)
    public void addStationsLineNotFound() throws LineNotFound, StationNotFound {
        List<Long> stationIds = Arrays.asList(1L, 2L, 3L);
        this.lineService.addStations(99L, stationIds);
    }

    @Test(expected = StationNotFound.class)
    public void addStationsStationNotFound() throws LineNotFound, StationNotFound {
        List<Long> stationIds = Arrays.asList(1L, 99L, 3L);
        this.lineService.addStations(2L, stationIds);
    }

    @Test
    public void removeStationSuccess() throws StationNotFound, LineNotFound {
        Line line = this.lineService.removeStation(1L, 1L);
        assertNotNull(line);
        assertEquals(0, line.getStations().size());
    }


    @Test(expected = LineNotFound.class)
    public void removeStationLineNotFound() throws StationNotFound, LineNotFound
    {
        this.lineService.removeStation(99L, 1L);
    }

    @Test(expected = StationNotFound.class)
    public void removeStationStationNotFound()
        throws StationNotFound, LineNotFound
    {
        this.lineService.removeStation(1L, 99L);
    }

    @Test
    public void removeStationsSuccess() throws LineNotFound, StationNotFound {
        List<Long> stationIds = Arrays.asList(1L, 2L);
        Line line = this.lineService.removeStations(4L, stationIds);
        assertNotNull(line);
        assertEquals(1, line.getStations().size());

        List<Station> stations = new ArrayList<>(line.getStations());
        assertEquals(3L, (long)stations.get(0).getId());
    }

    @Test(expected = LineNotFound.class)
    public void removeStationsLineNotFound() throws LineNotFound, StationNotFound {
        List<Long> stationIds = Arrays.asList(1L, 2L, 3L);
        this.lineService.removeStations(99L, stationIds);
    }

    @Test(expected = StationNotFound.class)
    public void removeStationsStationNotFound() throws LineNotFound, StationNotFound {
        List<Long> stationIds = Arrays.asList(1L, 99L, 3L);
        this.lineService.removeStations(4L, stationIds);
    }

    @Test
    public void getStations() throws LineNotFound {
        Set<Station> stations = this.lineService.getStations(1);
        assertNotNull(stations);
        assertEquals(1, stations.size());

        stations = this.lineService.getStations(2);
        assertNotNull(stations);
        assertEquals(0, stations.size());
    }

    @Test(expected = LineNotFound.class)
    public void getStationsLineNotFound() throws LineNotFound {
        this.lineService.getStations(99L);
    }

    @Test
    public void existsSuccess() {
        assertTrue(this.lineService.exists(1L));
        assertFalse(this.lineService.exists(99L));
    }

}

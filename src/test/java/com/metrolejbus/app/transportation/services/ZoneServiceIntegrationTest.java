package com.metrolejbus.app.transportation.services;

import com.metrolejbus.app.ClearDatabaseRule;
import com.metrolejbus.app.transportation.exceptions.ZoneNameNotAvailable;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.repositories.ZoneRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ZoneServiceIntegrationTest {

    @Rule
    @Autowired
    public ClearDatabaseRule clearDatabaseRule;

    @Autowired
    private ZoneRepository zoneRepository;

    @Autowired
    private ZoneService zoneService;

    @Before
    public void setUp() {
        this.zoneRepository.save(new Zone("Zone 1"));
        this.zoneRepository.saveAndFlush(new Zone("Zone 2"));
    }

    @Test
    public void createSuccess() throws ZoneNameNotAvailable {
        Zone zone = this.zoneService.create("Zone 3");
        assertNotNull(zone);
        assertNotNull(zone.getId());
        assertEquals("Zone 3", zone.getName());
    }


    @Test(expected = ZoneNameNotAvailable.class)
    public void createZoneNameNotAvailable() throws ZoneNameNotAvailable {
        this.zoneService.create("Zone 1");
    }

    @Test
    public void deleteSuccess() throws ZoneNotFound {
        Zone zone = this.zoneService.delete(1);
        assertNotNull(zone);
        assertEquals(1L, (long) zone.getId());
        assertFalse(zone.isActive());
    }


    @Test(expected = ZoneNotFound.class)
    public void deleteZoneNotFound() throws ZoneNotFound {
        this.zoneService.delete(99L);
    }

    @Test
    public void getSuccess() throws ZoneNotFound {
        Zone zone = this.zoneService.get(1);
        assertNotNull(zone);
        assertEquals(1L, (long) zone.getId());
        assertEquals("Zone 1", zone.getName());
    }

    @Test(expected = ZoneNotFound.class)
    public void getZoneNotFound() throws ZoneNotFound {
        this.zoneService.get(99L);
    }

    @Test
    public void getAllSuccess() {
        List<Zone> zones = this.zoneService.getAll();

        assertNotNull(zones);
        assertEquals(2, zones.size());
    }

    @Test
    public void updateSuccess() throws ZoneNameNotAvailable, ZoneNotFound {
        Zone zone = this.zoneService.update(2, "New Zone Name");
        assertNotNull(zone);
        assertEquals(2L, (long) zone.getId());
        assertEquals("New Zone Name", zone.getName());
    }

    @Test(expected = ZoneNotFound.class)
    public void updateZoneNotFound() throws ZoneNameNotAvailable, ZoneNotFound {
        this.zoneService.update(99L, "New Zone Name");
    }

    @Test(expected = ZoneNameNotAvailable.class)
    public void updateZoneNameNotAvailable()
        throws ZoneNameNotAvailable, ZoneNotFound
    {
        this.zoneService.update(2L, "Zone 1");
    }

}

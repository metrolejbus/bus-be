package com.metrolejbus.app.transportation.services;

import com.metrolejbus.app.transportation.exceptions.StationNotFound;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Station;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.repositories.StationRepository;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static junit.framework.TestCase.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class StationServiceTest {

    private static Zone zone;
    private static Zone zone2;
    @MockBean
    private StationRepository stationRepository;
    @MockBean
    private ZoneService zoneService;

    @Autowired
    private StationService stationService;
    private Map<Long, Station> mockedStations = new HashMap<>();

    @BeforeClass
    public static void setUpClass() {
        zone = new Zone("zone");
        zone.setId(1L);

        zone2 = new Zone("zone2");
        zone2.setId(2L);
    }

    @Before
    public void setUp() throws ZoneNotFound {
        mockedStations.clear();

        Station station = new Station("station 1", zone, 45, 45);
        station.setId(1L);
        mockedStations.put(1L, station);

        when(stationRepository.findById(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            Station s = mockedStations.getOrDefault(key, null);
            return Optional.ofNullable(s);
        });

        when(stationRepository.findAll()).thenReturn(
            new LinkedList<>(mockedStations.values())
        );

        when(stationRepository.save(any(Station.class))).thenAnswer(invocation -> {
            Station s = invocation.getArgument(0);
            if (s.getId() == null) {
                long key = mockedStations.keySet().size();
                s.setId(++key);
                mockedStations.put(++key, s);
            } else {
                mockedStations.put(s.getId(), s);
            }

            return s;
        });

        when(zoneService.get(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            if (key == 1L) {
                return zone;
            } else if (key == 2L) {
                return zone2;
            } else {
                throw new ZoneNotFound(key);
            }
        });
    }

    @Test
    public void createSuccess() throws ZoneNotFound {
        Station s = this.stationService.create("station 2", 1, 30, 45);
        assertNotNull(s);
        assertNotNull(s.getId());
        assertEquals(2L, (long) s.getId());
        assertEquals("station 2", s.getName());
        assertEquals(30f, s.getLocation().getLat());
        assertEquals(45f, s.getLocation().getLng());

        verify(stationRepository, times(1)).save(any(Station.class));
        verifyNoMoreInteractions(stationRepository);

        verify(zoneService, times(1)).get(1L);
        verifyNoMoreInteractions(zoneService);
    }

    @Test(expected = ZoneNotFound.class)
    public void createZoneNotFound() throws ZoneNotFound {
        this.stationService.create("station 2", 99L, 30, 45);
    }

    @Test(expected = NullPointerException.class)
    public void createNameIsNull() throws ZoneNotFound {
        this.stationService.create(null, 1L, 30, 45);
    }

    @Test
    public void deleteSuccess() throws StationNotFound {
        Station station = new Station("station 2", zone, 45, 45);
        station.setId(2L);
        mockedStations.put(2L, station);

        Station deletedStation = this.stationService.delete(2L);
        assertNotNull(deletedStation);
        assertFalse(deletedStation.isActive());

        verify(stationRepository, times(1)).findById(2L);
        verify(stationRepository, times(1)).save(any(Station.class));
        verifyNoMoreInteractions(stationRepository);
    }

    @Test(expected = StationNotFound.class)
    public void deleteStationNotFound() throws StationNotFound {
        this.stationService.delete(99L);

        verify(stationRepository, times(1)).findById(99L);
        verifyNoMoreInteractions(stationRepository);
    }

    @Test
    public void getSuccess() throws StationNotFound {
        Station s = this.stationService.get(1L);
        assertNotNull(s);
        assertEquals(1L, (long) s.getId());

        verify(stationRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(stationRepository);
    }

    @Test(expected = StationNotFound.class)
    public void getStationNotFound() throws StationNotFound {
        this.stationService.get(99L);

        verify(stationRepository, times(1)).findById(99L);
        verifyNoMoreInteractions(stationRepository);
    }

    @Test
    public void getAllSuccess() {
        List<Station> stations = this.stationService.getAll();
        assertNotNull(stations);
        assertEquals(1, stations.size());

        verify(stationRepository, times(1)).findAll();
        verifyNoMoreInteractions(stationRepository);
    }

    @Test
    public void updateNameSuccess() throws StationNotFound {
        Station s = this.stationService.update(1L, "new name");
        assertNotNull(s);
        assertEquals("new name", s.getName());

        verify(stationRepository, times(1)).save(any(Station.class));
        verify(stationRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(stationRepository);
    }

    @Test
    public void updateZoneSuccess() throws ZoneNotFound, StationNotFound {
        Station s = this.stationService.update(1L, 2L);
        assertNotNull(s);
        assertEquals(zone2.getId(), s.getZone().getId());

        verify(stationRepository, times(1)).save(any(Station.class));
        verify(stationRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(stationRepository);

        verify(zoneService, times(1)).get(2L);
        verifyNoMoreInteractions(zoneService);
    }

    @Test
    public void updateNameAndZoneSuccess() throws ZoneNotFound, StationNotFound
    {
        Station s = this.stationService.update(1L, "new station", 2L);
        assertNotNull(s);
        assertEquals("new station", s.getName());
        assertEquals(zone2.getId(), s.getZone().getId());

        verify(stationRepository, times(1)).save(any(Station.class));
        verify(stationRepository, times(1)).findById(1L);
        verifyNoMoreInteractions(stationRepository);

        verify(zoneService, times(1)).get(2L);
        verifyNoMoreInteractions(zoneService);
    }

    @Test(expected = StationNotFound.class)
    public void updateStationNotFound() throws StationNotFound {
        this.stationService.update(99L, "new name");

        verify(stationRepository, times(1)).findById(99L);
        verifyNoMoreInteractions(stationRepository);
    }

}

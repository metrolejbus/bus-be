package com.metrolejbus.app.transportation.services;

import com.metrolejbus.app.transportation.exceptions.ZoneNameNotAvailable;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.repositories.ZoneRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ZoneServiceTest {

    @MockBean
    private ZoneRepository zoneRepository;

    @Autowired
    private ZoneService zoneService;

    private Map<Long, Zone> mockedZones = new HashMap<>();

    @Before
    public void setUp() {
        mockedZones.clear();

        Zone zone = new Zone("Zone 1");
        zone.setId(1L);
        mockedZones.put(1L, zone);

        Zone zone2 = new Zone("Zone 2");
        zone2.setId(2L);
        mockedZones.put(2L, zone2);


        when(zoneRepository.findById(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            Zone z = mockedZones.getOrDefault(key, null);
            return Optional.ofNullable(z);
        });

        when(zoneRepository.findAll()).thenReturn(new LinkedList<>(mockedZones.values()));

        when(zoneRepository.save(any(Zone.class))).thenAnswer(invocation -> {
            Zone z = invocation.getArgument(0);

            boolean nameTaken = mockedZones.values()
                .stream()
                .anyMatch(
                    z2 -> z2.getName().equals(z.getName()) &&
                    !Objects.equals(z2.getId(), z.getId()
                ));

            if (nameTaken) { throw new ZoneNameNotAvailable(z.getName()); }

            if (z.getId() == null) {
                long id = mockedZones.size();
                z.setId(++id);
                mockedZones.put(id, z);
            } else {
                mockedZones.put(z.getId(), z);
            }

            return z;
        });
    }

    @Test
    public void createSuccess() throws ZoneNameNotAvailable {
        Zone z = this.zoneService.create("Zone 3");
        assertNotNull(z);
        assertEquals("Zone 3", z.getName());
        assertEquals(3L, (long) z.getId());

        verify(zoneRepository, times(1)).save(any(Zone.class));
        verifyNoMoreInteractions(zoneRepository);
    }


    @Test(expected = ZoneNameNotAvailable.class)
    public void createZoneNameNotAvailable() throws ZoneNameNotAvailable {
        this.zoneService.create("Zone 1");
    }

    @Test
    public void deleteSuccess() throws ZoneNotFound {
        Zone z = this.zoneService.delete(1L);
        assertNotNull(z);
        assertFalse(z.isActive());

        verify(zoneRepository, times(1)).findById(anyLong());
        verify(zoneRepository, times(1)).save(any(Zone.class));
        verifyNoMoreInteractions(zoneRepository);
    }


    @Test(expected = ZoneNotFound.class)
    public void deleteZoneNotFound() throws ZoneNotFound {
        this.zoneService.delete(99L);
    }

    @Test
    public void getSuccess() throws ZoneNotFound {
        Zone zone = this.zoneService.get(1L);
        assertNotNull(zone);
        assertEquals("Zone 1", zone.getName());

        verify(zoneRepository, times(1)).findById(anyLong());
        verifyNoMoreInteractions(zoneRepository);
    }

    @Test(expected = ZoneNotFound.class)
    public void getZoneNotFound() throws ZoneNotFound {
        this.zoneService.get(99L);
    }

    @Test
    public void getAllSuccess() {
        List<Zone> zones = this.zoneService.getAll();

        assertNotNull(zones);
        assertEquals(2, zones.size());

        verify(zoneRepository, times(1)).findAll();
        verifyNoMoreInteractions(zoneRepository);
    }

    @Test
    public void updateSuccess() throws ZoneNameNotAvailable, ZoneNotFound {
        Zone z = this.zoneService.update(1L, "New Zone Name");
        assertNotNull(z);
        assertEquals("New Zone Name", z.getName());

        verify(zoneRepository, times(1)).findById(anyLong());
        verify(zoneRepository, times(1)).save(any(Zone.class));
        verifyNoMoreInteractions(zoneRepository);
    }

    @Test(expected = ZoneNotFound.class)
    public void updateZoneNotFound() throws ZoneNameNotAvailable, ZoneNotFound {
        this.zoneService.update(99L, "New Zone Name");
    }

    @Test(expected = ZoneNameNotAvailable.class)
    public void updateZoneNameNotAvailable()
        throws ZoneNameNotAvailable, ZoneNotFound
    {
        this.zoneService.update(2L, "Zone 1");
    }

}

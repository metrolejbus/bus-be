package com.metrolejbus.app.transportation.services;

import com.metrolejbus.app.common.models.ValidityPeriod;
import com.metrolejbus.app.transportation.exceptions.LineNotFound;
import com.metrolejbus.app.transportation.exceptions.ScheduleNotFound;
import com.metrolejbus.app.transportation.exceptions.StationNotFound;
import com.metrolejbus.app.transportation.forms.ScheduleForm;
import com.metrolejbus.app.transportation.models.*;
import com.metrolejbus.app.transportation.repositories.LineRepository;
import com.metrolejbus.app.transportation.repositories.ScheduleRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.management.InvalidAttributeValueException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ScheduleServiceTest {

    @MockBean
    public LineRepository lineRepository;

    @MockBean
    public ScheduleRepository scheduleRepository;

    @MockBean
    public StationService stationService;

    @Autowired
    public ScheduleService scheduleService;

    private Map<Long, Schedule> mockedSchedules = new HashMap<>();
    private Map<Long, Line> mockedLines = new HashMap<>();
    private Map<Long, Station> mockedStations = new HashMap<>();

    @Before
    public void setUp() throws Exception {
        mockedLines.clear();
        mockedStations.clear();
        mockedSchedules.clear();


        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
        // schedule 1
        ValidityPeriod validityPeriod = new ValidityPeriod(LocalDate.parse("11.11.2029.", formatter), LocalDate.parse("11.11.2030.", formatter));

        List<DayOfWeek> appliesTo = new ArrayList<DayOfWeek>();

        appliesTo.add(DayOfWeek.FRIDAY);

        List<LocalTime> routeA = new ArrayList<LocalTime>();
        routeA.add(LocalTime.of(12,0));
        routeA.add(LocalTime.of(12,10));
        routeA.add(LocalTime.of(12,15));
        routeA.add(LocalTime.of(13,0));
        routeA.add(LocalTime.of(13,10));
        routeA.add(LocalTime.of(13,15));

        List<LocalTime> routeB = new ArrayList<LocalTime>();
        routeA.add(LocalTime.of(12,20));
        routeA.add(LocalTime.of(12,25));
        routeA.add(LocalTime.of(12,35));
        routeA.add(LocalTime.of(13,12));
        routeA.add(LocalTime.of(13,22));
        routeA.add(LocalTime.of(13,32));

        Schedule schedule1 = new Schedule(appliesTo,routeA,routeB,validityPeriod);
        schedule1.setId(1L);
        mockedSchedules.put(schedule1.getId(),schedule1 );

        // schedule 2
        ValidityPeriod validityPeriod2 = new ValidityPeriod(LocalDate.parse("11.11.2019.", formatter), LocalDate.parse("11.11.2020.", formatter));

        List<DayOfWeek> appliesTo2 = new ArrayList<DayOfWeek>();
        appliesTo2.add(DayOfWeek.MONDAY);
        appliesTo2.add(DayOfWeek.TUESDAY);
        appliesTo2.add(DayOfWeek.WEDNESDAY);
        appliesTo2.add(DayOfWeek.THURSDAY);

        List<LocalTime> routeA2 = new ArrayList<LocalTime>();
        routeA2.add(LocalTime.of(12,0));
        routeA2.add(LocalTime.of(12,10));
        routeA2.add(LocalTime.of(12,15));
        routeA2.add(LocalTime.of(13,0));
        routeA2.add(LocalTime.of(13,10));
        routeA2.add(LocalTime.of(13,15));

        List<LocalTime> routeB2 = new ArrayList<LocalTime>();
        routeB2.add(LocalTime.of(12,20));
        routeB2.add(LocalTime.of(12,25));
        routeB2.add(LocalTime.of(12,35));
        routeB2.add(LocalTime.of(13,12));
        routeB2.add(LocalTime.of(13,22));
        routeB2.add(LocalTime.of(13,32));

        Schedule schedule2 = new Schedule(appliesTo2,routeA2,routeB2,validityPeriod2);
        schedule2.setId(2L);
        mockedSchedules.put(schedule2.getId(),schedule2 );

        // schedule 3
        ValidityPeriod validityPeriod3 = new ValidityPeriod(LocalDate.parse("11.11.2020.", formatter), LocalDate.parse("11.11.2021.", formatter));

        List<DayOfWeek> appliesTo3 = new ArrayList<DayOfWeek>();
        appliesTo3.add(DayOfWeek.MONDAY);
        appliesTo3.add(DayOfWeek.TUESDAY);
        appliesTo3.add(DayOfWeek.WEDNESDAY);
        appliesTo3.add(DayOfWeek.FRIDAY);

        List<LocalTime> routeA3 = new ArrayList<LocalTime>();
        routeA3.add(LocalTime.of(12,0));
        routeA3.add(LocalTime.of(12,10));
        routeA3.add(LocalTime.of(12,15));
        routeA3.add(LocalTime.of(13,0));
        routeA3.add(LocalTime.of(13,10));
        routeA3.add(LocalTime.of(13,15));

        List<LocalTime> routeB3 = new ArrayList<LocalTime>();
        routeB3.add(LocalTime.of(12,20));
        routeB3.add(LocalTime.of(12,25));
        routeB3.add(LocalTime.of(12,35));
        routeB3.add(LocalTime.of(13,12));
        routeB3.add(LocalTime.of(13,22));
        routeB3.add(LocalTime.of(13,32));

        Schedule schedule3 = new Schedule(appliesTo3,routeA3,routeB3,validityPeriod3);
        schedule3.setId(3L);
        mockedSchedules.put(schedule3.getId(),schedule3 );

        // schedule 4
        ValidityPeriod validityPeriod4 = new ValidityPeriod(LocalDate.parse("11.11.2021.", formatter), LocalDate.parse("11.11.2022.", formatter));

        List<DayOfWeek> appliesTo4 = new ArrayList<DayOfWeek>();
        appliesTo4.add(DayOfWeek.TUESDAY);
        appliesTo4.add(DayOfWeek.WEDNESDAY);
        appliesTo4.add(DayOfWeek.THURSDAY);
        appliesTo4.add(DayOfWeek.FRIDAY);

        List<LocalTime> routeA4 = new ArrayList<LocalTime>();
        routeA4.add(LocalTime.of(12,0));
        routeA4.add(LocalTime.of(12,10));
        routeA4.add(LocalTime.of(12,15));
        routeA4.add(LocalTime.of(13,0));
        routeA4.add(LocalTime.of(13,10));
        routeA4.add(LocalTime.of(13,15));

        List<LocalTime> routeB4 = new ArrayList<LocalTime>();
        routeB4.add(LocalTime.of(12,20));
        routeB4.add(LocalTime.of(12,25));
        routeB4.add(LocalTime.of(12,35));
        routeB4.add(LocalTime.of(13,12));
        routeB4.add(LocalTime.of(13,22));
        routeB4.add(LocalTime.of(13,32));

        Schedule schedule4 = new Schedule(appliesTo4,routeA4,routeB4,validityPeriod4);
        schedule4.setId(4L);
        mockedSchedules.put(schedule4.getId(),schedule4 );




        Zone zone = new Zone("Zone 1");
        Station station = new Station("Station 1", zone, 30, 30);
        station.setId(1L);
        mockedStations.put(1L, station);

        Station station2 = new Station("Station 2", zone, 30, 30);
        station2.setId(2L);
        mockedStations.put(2L, station2);

        Station station3 = new Station("Station 3", zone, 30, 30);
        station3.setId(3L);
        mockedStations.put(3L, station3);

        Line line1 = new Line("Line 1", TransportationType.BUS, "");
        line1.setId(1L);
        line1.getStations().add(station);
        line1.getSchedules().add(schedule1);
        line1.getSchedules().add(schedule2);
        line1.getSchedules().add(schedule3);
        mockedLines.put(1L, line1);

        Line line2 = new Line("Line 2", TransportationType.METRO, "");
        line2.setId(2L);
        mockedLines.put(2L, line2);

        Line line3 = new Line("Line 3", TransportationType.TRAM, "");
        line3.setId(3L);
        line3.getStations().add(station);
        mockedLines.put(3L, line3);

        Line line4 = new Line("Line 4", TransportationType.BUS, "");
        line4.setId(4L);
        line4.getStations().add(station);
        line4.getStations().add(station2);
        line4.getStations().add(station3);
        mockedLines.put(4L, line4);



        when(scheduleRepository.findById(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            Schedule l = mockedSchedules.getOrDefault(key, null);
            return Optional.ofNullable(l);
        });

        when(scheduleRepository.findAll()).thenReturn(new LinkedList<>(mockedSchedules.values()));

//        when(scheduleRepository.findAllByStations_Id(anyLong())).thenAnswer(
//                invocation -> {
//                    long key = invocation.getArgument(0);
//                    return mockedLines.values()
//                            .stream()
//                            .filter(line -> line.getStations()
//                                    .stream()
//                                    .filter(s -> key == s.getId())
//                                    .count() == 1)
//                            .collect(Collectors.toList());
//                });

        when(scheduleRepository.save(any(Schedule.class))).thenAnswer(invocation -> {
            Schedule l = invocation.getArgument(0);
            System.out.println(l);
            if (l.getId() == null) {
                long key = mockedSchedules.keySet().size();
                l.setId(++key);
                mockedSchedules.put(++key, l);
            } else {
                mockedSchedules.put(l.getId(), l);
            }

            return l;
        });

        when(scheduleRepository.existsById(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            return mockedSchedules.containsKey(key);
        });

        when(lineRepository.getOne(anyLong())).thenAnswer(invocation -> {
            long key = invocation.getArgument(0);
            return mockedLines.get(key);
        });

        when(lineRepository.save(any(Line.class))).thenAnswer(invocation -> {
            Line l = invocation.getArgument(0);

            if (l.getId() == null) {
                long key = mockedLines.keySet().size();
                l.setId(++key);
                mockedLines.put(++key, l);
            } else {
                mockedLines.put(l.getId(), l);
            }

            return l;
        });

    }

    @Test
    public void createSuccess() throws InvalidAttributeValueException {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");

        ValidityPeriod validityPeriod4 = new ValidityPeriod(LocalDate.parse("11.11.2021.", formatter), LocalDate.parse("11.11.2022.", formatter));
        List<DayOfWeek> appliesTo = new ArrayList<DayOfWeek>();

        appliesTo.add(DayOfWeek.TUESDAY);
        appliesTo.add(DayOfWeek.WEDNESDAY);
        appliesTo.add(DayOfWeek.FRIDAY);
        LocalDate startDate = LocalDate.parse("11.11.2021.", formatter );
        LocalDate endDate = LocalDate.parse("11.11.2022.", formatter );

        List<LocalTime> routeA = new ArrayList<LocalTime>();
        routeA.add(LocalTime.of(12,0));
        routeA.add(LocalTime.of(12,10));
        routeA.add(LocalTime.of(12,15));
        routeA.add(LocalTime.of(13,0));
        routeA.add(LocalTime.of(13,10));
        routeA.add(LocalTime.of(13,15));

        List<LocalTime> routeB = new ArrayList<LocalTime>();
        routeB.add(LocalTime.of(12,20));
        routeB.add(LocalTime.of(12,25));
        routeB.add(LocalTime.of(12,35));
        routeB.add(LocalTime.of(13,22));
        routeB.add(LocalTime.of(13,32));
        List<Schedule> schedules = this.scheduleService.save(new ScheduleForm(
                routeA,
                routeB,
                appliesTo,
                startDate,
                endDate,
                mockedLines.get(1L).getId()));
        Schedule created = schedules.get(schedules.size()-1);
        assertNotNull(created);
        verify(lineRepository, times(1)).save(any(Line.class));


    }


//

    @Test
    public void getSuccess() throws ScheduleNotFound {
        Schedule schedule = this.scheduleService.findById(1L);
        assertNotNull(schedule);
        assertEquals(1L, (long) schedule.getId());

        verify(scheduleRepository, times(1)).findById(anyLong());
        verifyNoMoreInteractions(scheduleRepository);

        schedule = this.scheduleService.findById(3L);
        assertNotNull(schedule);
        assertEquals(3L, (long) schedule.getId());
    }

    @Test(expected = ScheduleNotFound.class)
    public void getScheduleNotFound() throws ScheduleNotFound  {
        this.scheduleService.findById(99L);
    }

    @Test
    public void getAllSuccess() {
        List<Schedule> schedules = this.scheduleService.findAll();
        assertNotNull(schedules);
        assertEquals(4, schedules.size());

        verify(scheduleRepository, times(1)).findAll();
        verifyNoMoreInteractions(scheduleRepository);

    }

    @Test
    public void deleteSuccess() throws ScheduleNotFound {
        Schedule schedule = this.scheduleService.deleteById(1L);
        assertNotNull(schedule);
        assertFalse(schedule.getActive());

        verify(scheduleRepository, times(1)).findById(anyLong());
        verify(scheduleRepository, times(1)).save(any(Schedule.class));
        verifyNoMoreInteractions(scheduleRepository);
    }

    @Test(expected = ScheduleNotFound.class)
    public void deleteScheduleNotFound() throws ScheduleNotFound {
        this.scheduleService.deleteById(99L);
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void validityPeriodStartDateFailed() throws InvalidAttributeValueException{
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");

        List<DayOfWeek> appliesTo = new ArrayList<DayOfWeek>();

        appliesTo.add(DayOfWeek.TUESDAY);
        appliesTo.add(DayOfWeek.WEDNESDAY);
        appliesTo.add(DayOfWeek.FRIDAY);
        LocalDate startDate = LocalDate.parse("11.11.2001.", formatter );
        LocalDate endDate = LocalDate.parse("11.11.2022.", formatter );

        List<LocalTime> routeA = new ArrayList<LocalTime>();
        routeA.add(LocalTime.of(12,0));
        routeA.add(LocalTime.of(12,10));
        routeA.add(LocalTime.of(12,15));
        routeA.add(LocalTime.of(13,0));
        routeA.add(LocalTime.of(13,10));
        routeA.add(LocalTime.of(13,15));

        List<LocalTime> routeB = new ArrayList<LocalTime>();
        routeB.add(LocalTime.of(12,20));
        routeB.add(LocalTime.of(12,25));
        routeB.add(LocalTime.of(12,35));
        routeB.add(LocalTime.of(13,22));
        routeB.add(LocalTime.of(13,32));
        List<Schedule> schedules = this.scheduleService.save(new ScheduleForm(
                routeA,
                routeB,
                appliesTo,
                startDate,
                endDate,
                mockedLines.get(1L).getId()));
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void validityPeriodEndDateFailed() throws InvalidAttributeValueException{
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");

        List<DayOfWeek> appliesTo = new ArrayList<DayOfWeek>();

        appliesTo.add(DayOfWeek.TUESDAY);
        appliesTo.add(DayOfWeek.WEDNESDAY);
        appliesTo.add(DayOfWeek.FRIDAY);
        LocalDate startDate = LocalDate.parse("11.11.2021.", formatter );
        LocalDate endDate = LocalDate.parse("11.11.2002.", formatter );

        List<LocalTime> routeA = new ArrayList<LocalTime>();
        routeA.add(LocalTime.of(12,0));
        routeA.add(LocalTime.of(12,10));
        routeA.add(LocalTime.of(12,15));
        routeA.add(LocalTime.of(13,0));
        routeA.add(LocalTime.of(13,10));
        routeA.add(LocalTime.of(13,15));

        List<LocalTime> routeB = new ArrayList<LocalTime>();
        routeB.add(LocalTime.of(12,20));
        routeB.add(LocalTime.of(12,25));
        routeB.add(LocalTime.of(12,35));
        routeB.add(LocalTime.of(13,22));
        routeB.add(LocalTime.of(13,32));
        List<Schedule> schedules = this.scheduleService.save(new ScheduleForm(
                routeA,
                routeB,
                appliesTo,
                startDate,
                endDate,
                mockedLines.get(1L).getId()));
    }

    @Test
    public void validityPeriodValidityPeriodSuccess() throws InvalidAttributeValueException{
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");

        List<DayOfWeek> appliesTo = new ArrayList<DayOfWeek>();

        appliesTo.add(DayOfWeek.TUESDAY);
        appliesTo.add(DayOfWeek.WEDNESDAY);
        appliesTo.add(DayOfWeek.FRIDAY);
        LocalDate startDate = LocalDate.parse("11.11.2021.", formatter );
        LocalDate endDate = LocalDate.parse("11.11.2022.", formatter );

        List<LocalTime> routeA = new ArrayList<LocalTime>();
        routeA.add(LocalTime.of(12,0));
        routeA.add(LocalTime.of(12,10));
        routeA.add(LocalTime.of(12,15));
        routeA.add(LocalTime.of(13,0));
        routeA.add(LocalTime.of(13,10));
        routeA.add(LocalTime.of(13,15));

        List<LocalTime> routeB = new ArrayList<LocalTime>();
        routeB.add(LocalTime.of(12,20));
        routeB.add(LocalTime.of(12,25));
        routeB.add(LocalTime.of(12,35));
        routeB.add(LocalTime.of(13,22));
        routeB.add(LocalTime.of(13,32));
        List<Schedule> schedules = this.scheduleService.save(new ScheduleForm(
                routeA,
                routeB,
                appliesTo,
                startDate,
                endDate,
                mockedLines.get(1L).getId()));
    }
}




package com.metrolejbus.app.transportation.services;


import com.metrolejbus.app.ClearDatabaseRule;
import com.metrolejbus.app.common.models.ValidityPeriod;
import com.metrolejbus.app.transportation.exceptions.LineNotFound;
import com.metrolejbus.app.transportation.exceptions.ScheduleNotFound;
import com.metrolejbus.app.transportation.exceptions.StationNotFound;
import com.metrolejbus.app.transportation.forms.ScheduleForm;
import com.metrolejbus.app.transportation.models.*;
import com.metrolejbus.app.transportation.repositories.LineRepository;
import com.metrolejbus.app.transportation.repositories.ScheduleRepository;
import com.metrolejbus.app.transportation.repositories.StationRepository;
import com.metrolejbus.app.transportation.repositories.ZoneRepository;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.management.InvalidAttributeValueException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;
import static org.junit.Assert.assertFalse;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class ScheduleServiceIntegrationTest {
    @Rule
    @Autowired
    public ClearDatabaseRule clearDatabaseRule;

    @Autowired
    public ScheduleService scheduleService;



    @Autowired
    public LineRepository lineRepository;

    @Autowired
    public ScheduleRepository scheduleRepository;

    @Autowired
    public ZoneRepository zoneRepository;

    @Autowired
    public StationRepository stationRepository;

    @Before
    public void setUp() throws InvalidAttributeValueException {
        Zone zone = zoneRepository.saveAndFlush(new Zone("Zone 1"));

        Station station = stationRepository.save(
                new Station("Station 1", zone, 30, 30)
        );
        Station station2 = stationRepository.save(
                new Station("Station 2", zone, 30, 30)
        );
        Station station3 = stationRepository.save(
                new Station("Station 3", zone, 30, 30)
        );

        Line line1 = lineRepository.save(
                new Line("Line 1", TransportationType.BUS, "")
        );
        line1.getStations().add(station);
        lineRepository.save(line1);

        lineRepository.save(
                new Line("Line 2", TransportationType.METRO, "")
        );

        Line line3 = lineRepository.save(
                new Line("Line 3", TransportationType.TRAM, "")
        );
        line3.getStations().add(station);
        lineRepository.save(line3);


        Line line4 = lineRepository.save(
                new Line("Line 4", TransportationType.TRAM, "")
        );
        line4.getStations().add(station);
        line4.getStations().add(station2);
        line4.getStations().add(station3);
        lineRepository.saveAndFlush(line4);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
        // schedule 1
        ValidityPeriod validityPeriod = new ValidityPeriod(LocalDate.parse("11.11.2029.", formatter), LocalDate.parse("11.11.2030.", formatter));

        List<DayOfWeek> appliesTo = new ArrayList<DayOfWeek>();

        appliesTo.add(DayOfWeek.FRIDAY);

        List<LocalTime> routeA = new ArrayList<LocalTime>();
        routeA.add(LocalTime.of(12,0));
        routeA.add(LocalTime.of(12,10));
        routeA.add(LocalTime.of(12,15));
        routeA.add(LocalTime.of(13,0));
        routeA.add(LocalTime.of(13,10));
        routeA.add(LocalTime.of(13,15));

        List<LocalTime> routeB = new ArrayList<LocalTime>();
        routeA.add(LocalTime.of(12,20));
        routeA.add(LocalTime.of(12,25));
        routeA.add(LocalTime.of(12,35));
        routeA.add(LocalTime.of(13,12));
        routeA.add(LocalTime.of(13,22));
        routeA.add(LocalTime.of(13,32));

        Schedule schedule1 = new Schedule(appliesTo,routeA,routeB,validityPeriod);
        scheduleRepository.save(schedule1);


        // schedule 2
        ValidityPeriod validityPeriod2 = new ValidityPeriod(LocalDate.parse("11.11.2019.", formatter), LocalDate.parse("11.11.2020.", formatter));

        List<DayOfWeek> appliesTo2 = new ArrayList<DayOfWeek>();
        appliesTo2.add(DayOfWeek.MONDAY);
        appliesTo2.add(DayOfWeek.TUESDAY);
        appliesTo2.add(DayOfWeek.WEDNESDAY);
        appliesTo2.add(DayOfWeek.THURSDAY);

        List<LocalTime> routeA2 = new ArrayList<LocalTime>();
        routeA2.add(LocalTime.of(12,0));
        routeA2.add(LocalTime.of(12,10));
        routeA2.add(LocalTime.of(12,15));
        routeA2.add(LocalTime.of(13,0));
        routeA2.add(LocalTime.of(13,10));
        routeA2.add(LocalTime.of(13,15));

        List<LocalTime> routeB2 = new ArrayList<LocalTime>();
        routeB2.add(LocalTime.of(12,20));
        routeB2.add(LocalTime.of(12,25));
        routeB2.add(LocalTime.of(12,35));
        routeB2.add(LocalTime.of(13,12));
        routeB2.add(LocalTime.of(13,22));
        routeB2.add(LocalTime.of(13,32));

        Schedule schedule2 = new Schedule(appliesTo2,routeA2,routeB2,validityPeriod2);
        scheduleRepository.save(schedule2);

        // schedule 3
        ValidityPeriod validityPeriod3 = new ValidityPeriod(LocalDate.parse("11.11.2020.", formatter), LocalDate.parse("11.11.2021.", formatter));

        List<DayOfWeek> appliesTo3 = new ArrayList<DayOfWeek>();
        appliesTo3.add(DayOfWeek.MONDAY);
        appliesTo3.add(DayOfWeek.TUESDAY);
        appliesTo3.add(DayOfWeek.WEDNESDAY);
        appliesTo3.add(DayOfWeek.FRIDAY);

        List<LocalTime> routeA3 = new ArrayList<LocalTime>();
        routeA3.add(LocalTime.of(12,0));
        routeA3.add(LocalTime.of(12,10));
        routeA3.add(LocalTime.of(12,15));
        routeA3.add(LocalTime.of(13,0));
        routeA3.add(LocalTime.of(13,10));
        routeA3.add(LocalTime.of(13,15));

        List<LocalTime> routeB3 = new ArrayList<LocalTime>();
        routeB3.add(LocalTime.of(12,20));
        routeB3.add(LocalTime.of(12,25));
        routeB3.add(LocalTime.of(12,35));
        routeB3.add(LocalTime.of(13,12));
        routeB3.add(LocalTime.of(13,22));
        routeB3.add(LocalTime.of(13,32));

        Schedule schedule3 = new Schedule(appliesTo3,routeA3,routeB3,validityPeriod3);
        scheduleRepository.save(schedule3);

        // schedule 4
        ValidityPeriod validityPeriod4 = new ValidityPeriod(LocalDate.parse("11.11.2021.", formatter), LocalDate.parse("11.11.2022.", formatter));

        List<DayOfWeek> appliesTo4 = new ArrayList<DayOfWeek>();
        appliesTo4.add(DayOfWeek.TUESDAY);
        appliesTo4.add(DayOfWeek.WEDNESDAY);
        appliesTo4.add(DayOfWeek.THURSDAY);
        appliesTo4.add(DayOfWeek.FRIDAY);

        List<LocalTime> routeA4 = new ArrayList<LocalTime>();
        routeA4.add(LocalTime.of(12,0));
        routeA4.add(LocalTime.of(12,10));
        routeA4.add(LocalTime.of(12,15));
        routeA4.add(LocalTime.of(13,0));
        routeA4.add(LocalTime.of(13,10));
        routeA4.add(LocalTime.of(13,15));

        List<LocalTime> routeB4 = new ArrayList<LocalTime>();
        routeB4.add(LocalTime.of(12,20));
        routeB4.add(LocalTime.of(12,25));
        routeB4.add(LocalTime.of(12,35));
        routeB4.add(LocalTime.of(13,12));
        routeB4.add(LocalTime.of(13,22));
        routeB4.add(LocalTime.of(13,32));

        Schedule schedule4 = new Schedule(appliesTo4,routeA4,routeB4,validityPeriod4);
        scheduleRepository.save(schedule4);



    }

//    @Test
//    public void createSuccess() throws InvalidAttributeValueException {
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
//
//        ValidityPeriod validityPeriod4 = new ValidityPeriod(LocalDate.parse("11.11.2021.", formatter), LocalDate.parse("11.11.2022.", formatter));
//        List<DayOfWeek> appliesTo = new ArrayList<DayOfWeek>();
//
//        appliesTo.add(DayOfWeek.TUESDAY);
//        appliesTo.add(DayOfWeek.WEDNESDAY);
//        appliesTo.add(DayOfWeek.FRIDAY);
//        LocalDate startDate = LocalDate.parse("11.11.2021.", formatter );
//        LocalDate endDate = LocalDate.parse("11.11.2022.", formatter );
//
//        List<LocalTime> routeA = new ArrayList<LocalTime>();
//        routeA.add(LocalTime.of(12,0));
//        routeA.add(LocalTime.of(12,10));
//        routeA.add(LocalTime.of(12,15));
//        routeA.add(LocalTime.of(13,0));
//        routeA.add(LocalTime.of(13,10));
//        routeA.add(LocalTime.of(13,15));
//
//        List<LocalTime> routeB = new ArrayList<LocalTime>();
//        routeB.add(LocalTime.of(12,20));
//        routeB.add(LocalTime.of(12,25));
//        routeB.add(LocalTime.of(12,35));
//        routeB.add(LocalTime.of(13,22));
//        routeB.add(LocalTime.of(13,32));
//        List<Schedule> schedules = this.scheduleService.save(new ScheduleForm(
//                routeA,
//                routeB,
//                appliesTo,
//                startDate,
//                endDate,
//                lineRepository.getOne(1L).getId()));
//
//
//        Schedule schedule = schedules.get(schedules.size()-1);
//        assertNotNull(schedules);
//        assertNotNull(schedule);
//        assertEquals(5L, (long) schedule.getId());
//    }







    @Test
    public void getSuccess() throws ScheduleNotFound {
        Schedule schedule = this.scheduleService.findById(1L);
        assertNotNull(schedule);
        assertEquals(1L, (long) schedule.getId());

        schedule = this.scheduleService.findById(3L);
        assertNotNull(schedule);
        assertEquals(3L, (long) schedule.getId());
    }

    @Test(expected = ScheduleNotFound.class)
    public void getScheduleNotFound() throws ScheduleNotFound {
        this.scheduleService.findById(99L);
    }

    @Test
    public void getAllSuccess() {
        List<Schedule> schedules = this.scheduleService.findAll();
        assertNotNull(schedules);
        assertEquals(4, schedules.size());
    }

    @Test
    public void deleteSuccess() throws ScheduleNotFound {
        Schedule s = this.scheduleService.deleteById(1L);
        assertNotNull(s);
        assertFalse(s.getActive());
    }

    @Test(expected = ScheduleNotFound.class)
    public void deleteScheduleNotFound() throws ScheduleNotFound {
        this.scheduleService.deleteById(99L);
    }


    @Test(expected = InvalidAttributeValueException.class)
    public void validityPeriodStartDateFailed() throws InvalidAttributeValueException{
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");

        List<DayOfWeek> appliesTo = new ArrayList<DayOfWeek>();

        appliesTo.add(DayOfWeek.TUESDAY);
        appliesTo.add(DayOfWeek.WEDNESDAY);
        appliesTo.add(DayOfWeek.FRIDAY);
        LocalDate startDate = LocalDate.parse("11.11.2001.", formatter );
        LocalDate endDate = LocalDate.parse("11.11.2022.", formatter );

        List<LocalTime> routeA = new ArrayList<LocalTime>();
        routeA.add(LocalTime.of(12,0));
        routeA.add(LocalTime.of(12,10));
        routeA.add(LocalTime.of(12,15));
        routeA.add(LocalTime.of(13,0));
        routeA.add(LocalTime.of(13,10));
        routeA.add(LocalTime.of(13,15));

        List<LocalTime> routeB = new ArrayList<LocalTime>();
        routeB.add(LocalTime.of(12,20));
        routeB.add(LocalTime.of(12,25));
        routeB.add(LocalTime.of(12,35));
        routeB.add(LocalTime.of(13,22));
        routeB.add(LocalTime.of(13,32));
        List<Schedule> schedules = this.scheduleService.save(new ScheduleForm(
                routeA,
                routeB,
                appliesTo,
                startDate,
                endDate,
                lineRepository.getOne(1L).getId()));
    }

    @Test(expected = InvalidAttributeValueException.class)
    public void validityPeriodEndDateFailed() throws InvalidAttributeValueException{
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");

        List<DayOfWeek> appliesTo = new ArrayList<DayOfWeek>();

        appliesTo.add(DayOfWeek.TUESDAY);
        appliesTo.add(DayOfWeek.WEDNESDAY);
        appliesTo.add(DayOfWeek.FRIDAY);
        LocalDate startDate = LocalDate.parse("11.11.2021.", formatter );
        LocalDate endDate = LocalDate.parse("11.11.2002.", formatter );

        List<LocalTime> routeA = new ArrayList<LocalTime>();
        routeA.add(LocalTime.of(12,0));
        routeA.add(LocalTime.of(12,10));
        routeA.add(LocalTime.of(12,15));
        routeA.add(LocalTime.of(13,0));
        routeA.add(LocalTime.of(13,10));
        routeA.add(LocalTime.of(13,15));

        List<LocalTime> routeB = new ArrayList<LocalTime>();
        routeB.add(LocalTime.of(12,20));
        routeB.add(LocalTime.of(12,25));
        routeB.add(LocalTime.of(12,35));
        routeB.add(LocalTime.of(13,22));
        routeB.add(LocalTime.of(13,32));
        List<Schedule> schedules = this.scheduleService.save(new ScheduleForm(
                routeA,
                routeB,
                appliesTo,
                startDate,
                endDate,
                lineRepository.getOne(1L).getId()));
    }

//    @Test
//    public void validityPeriodValidityPeriodSuccess() throws InvalidAttributeValueException{
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
//
//        List<DayOfWeek> appliesTo = new ArrayList<DayOfWeek>();
//
//        appliesTo.add(DayOfWeek.TUESDAY);
//        appliesTo.add(DayOfWeek.WEDNESDAY);
//        appliesTo.add(DayOfWeek.FRIDAY);
//        LocalDate startDate = LocalDate.parse("11.11.2021.", formatter );
//        LocalDate endDate = LocalDate.parse("11.11.2022.", formatter );
//
//        List<LocalTime> routeA = new ArrayList<LocalTime>();
//        routeA.add(LocalTime.of(12,0));
//        routeA.add(LocalTime.of(12,10));
//        routeA.add(LocalTime.of(12,15));
//        routeA.add(LocalTime.of(13,0));
//        routeA.add(LocalTime.of(13,10));
//        routeA.add(LocalTime.of(13,15));
//
//        List<LocalTime> routeB = new ArrayList<LocalTime>();
//        routeB.add(LocalTime.of(12,20));
//        routeB.add(LocalTime.of(12,25));
//        routeB.add(LocalTime.of(12,35));
//        routeB.add(LocalTime.of(13,22));
//        routeB.add(LocalTime.of(13,32));
//        List<Schedule> schedules = this.scheduleService.save(new ScheduleForm(
//                routeA,
//                routeB,
//                appliesTo,
//                startDate,
//                endDate,
//                lineRepository.getOne(1L).getId()));
//    }
}

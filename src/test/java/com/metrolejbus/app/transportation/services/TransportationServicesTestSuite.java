package com.metrolejbus.app.transportation.services;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
    JourneyServiceIntegrationTest.class,
    JourneyServiceTest.class,
    LineServiceIntegrationTest.class,
    LineServiceTest.class,
    StationServiceIntegrationTest.class,
    StationServiceTest.class,
    ZoneServiceIntegrationTest.class,
    ZoneServiceTest.class,
    ScheduleServiceTest.class,
    ScheduleServiceIntegrationTest.class
})
public class TransportationServicesTestSuite {}

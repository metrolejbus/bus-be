package com.metrolejbus.app.transportation;

import com.metrolejbus.app.transportation.controllers.TransportationControllersTestSuite;
import com.metrolejbus.app.transportation.services.TransportationServicesTestSuite;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
    TransportationControllersTestSuite.class,
    TransportationServicesTestSuite.class
})
public class TransportationTestSuite {}

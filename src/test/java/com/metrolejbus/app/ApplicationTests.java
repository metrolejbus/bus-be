package com.metrolejbus.app;

import com.metrolejbus.app.customers.CustomersTestSuite;
import com.metrolejbus.app.ticketing.TicketingTestSuite;
import com.metrolejbus.app.transportation.TransportationTestSuite;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        TicketingTestSuite.class,
        TransportationTestSuite.class,
        CustomersTestSuite.class
})
public class ApplicationTests {

}

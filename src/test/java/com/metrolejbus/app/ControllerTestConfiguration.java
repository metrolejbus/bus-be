package com.metrolejbus.app;

import com.metrolejbus.app.authentication.SecurityFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.util.Base64Utils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Collection;


@Configuration
@EnableWebSecurity
@Profile({"controller-test"})
public class ControllerTestConfiguration {

    @Bean
    public UserDetailsService userDetailsService() {
        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        manager.createUser(User
            .withUsername("user")
            .password("password")
            .authorities("USER").build());
        manager.createUser(User
            .withUsername("admin")
            .password("password")
            .authorities("ADMIN").build());
        manager.createUser(User
            .withUsername("system")
            .password("password")
            .authorities("SYSTEM").build());
        manager.createUser(User
            .withUsername("customer")
            .password("password")
            .authorities("CUSTOMER").build());
        return manager;
    }

    @Bean
    public SecurityFilter securityFilter() {
        return new SecurityFilter() {
            @Override
            public void doFilter(
                ServletRequest request,
                ServletResponse response,
                FilterChain chain
            ) throws IOException, ServletException
            {
                String token =
                    ((HttpServletRequest)request).getHeader("Authorization");
                if (token != null) {
                    String encodedCredentials = token.substring(6);
                    String decodedCredentials = new String(
                        Base64Utils.decodeFromString(encodedCredentials)
                    );
                    String[] credentials = decodedCredentials.split(":");

                    String username = credentials[0];
                    String password = credentials[1];

                    UserDetails userDetails =
                        userDetailsService().loadUserByUsername(username);

                    if (userDetails.getPassword().equals(password)) {
                        Collection roles = userDetails.getAuthorities();
                        Authentication auth =
                            new UsernamePasswordAuthenticationToken(
                                username, "", roles
                            );
                        SecurityContextHolder.getContext()
                            .setAuthentication(auth);
                    }
                }
                chain.doFilter(request, response);
            }
        };
    }
}

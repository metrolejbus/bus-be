package com.metrolejbus.app.transportation.exceptions;

public class StationNotFound extends Exception {

    public StationNotFound(long id) {
        super("Station [" + id + "] doesn't exist");
    }

}

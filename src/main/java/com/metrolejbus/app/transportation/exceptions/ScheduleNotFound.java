package com.metrolejbus.app.transportation.exceptions;

public class ScheduleNotFound extends Exception {

    public ScheduleNotFound(long id) {
        super("Schedule [" + id + "] doesn't exist");
    }
}

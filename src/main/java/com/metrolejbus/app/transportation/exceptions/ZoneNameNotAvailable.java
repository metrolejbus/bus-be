package com.metrolejbus.app.transportation.exceptions;

public class ZoneNameNotAvailable extends Exception {

    public ZoneNameNotAvailable(String name) {
        super("Zone name \"" + name + "\" is not available");
    }

}

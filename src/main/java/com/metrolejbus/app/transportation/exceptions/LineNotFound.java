package com.metrolejbus.app.transportation.exceptions;

public class LineNotFound extends Exception {

    public LineNotFound(long id) {
        super("Line [" + id + "] doesn't exist");
    }

}

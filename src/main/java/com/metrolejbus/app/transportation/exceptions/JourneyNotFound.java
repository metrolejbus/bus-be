package com.metrolejbus.app.transportation.exceptions;

public class JourneyNotFound extends Exception {

    public JourneyNotFound(long journeyId) {
        super("Journey [" + journeyId + "] doesn't exist");
    }

}

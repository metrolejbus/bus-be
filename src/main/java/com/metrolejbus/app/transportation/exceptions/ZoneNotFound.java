package com.metrolejbus.app.transportation.exceptions;

public class ZoneNotFound extends Exception {

    public ZoneNotFound(long id) {
        super("Zone [" + id + "] doesn't exist");
    }

}

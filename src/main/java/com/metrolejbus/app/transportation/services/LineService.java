package com.metrolejbus.app.transportation.services;

import com.metrolejbus.app.transportation.exceptions.LineNotFound;
import com.metrolejbus.app.transportation.exceptions.StationNotFound;
import com.metrolejbus.app.transportation.models.Line;
import com.metrolejbus.app.transportation.models.Schedule;
import com.metrolejbus.app.transportation.models.Station;
import com.metrolejbus.app.transportation.models.TransportationType;
import com.metrolejbus.app.transportation.repositories.LineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;


@Service
public class LineService {

    private final LineRepository lineRepository;
    private final StationService stationService;

    @Autowired
    public LineService(
        LineRepository lineRepository,
        StationService stationService
    )
    {
        this.lineRepository = lineRepository;
        this.stationService = stationService;
    }

    public Line create(String name, TransportationType type, String route) {
        return this.lineRepository.save(new Line(name, type, route));
    }

    public boolean exists(long id) { return this.lineRepository.existsById(id); }

    public Line get(long id) throws LineNotFound {
        return this.lineRepository.findById(id)
            .orElseThrow(() -> new LineNotFound(id));
    }

    public List<Line> getAll() {
        return this.lineRepository.findAll();
    }

    public List<Line> getAll(long stationId) {
        return this.lineRepository.findAllByStations_Id(stationId);
    }

    public Line delete(long id) throws LineNotFound {
        Line line = this.get(id);
        line.setActive(false);
        return this.lineRepository.save(line);
    }

    public Line update(long id, String name) throws LineNotFound {
        Line line = this.get(id);
        line.setName(name);
        return this.lineRepository.save(line);
    }

    public Line update(long id, String name, String route) throws LineNotFound{
        Line line = this.get(id);
        line.setName(name);
        line.setRoute(route);
        return this.lineRepository.save(line);
    }

    public Line addStation(long id, long stationId)
        throws LineNotFound, StationNotFound
    {
        Line line = this.get(id);
        Station station = this.stationService.get(stationId);

        line.getStations().add(station);
        return this.lineRepository.save(line);
    }

    @Transactional(rollbackOn = { StationNotFound.class })
    public Line addStations(long id, List<Long> stationIds)
        throws LineNotFound, StationNotFound
    {
        Line line = this.get(id);
        Set<Station> stations = line.getStations();

        for(Long stationId: stationIds) {
            stations.add(this.stationService.get(stationId));
        }

        return this.lineRepository.save(line);
    }

    public Line removeStation(long id, long stationId)
        throws LineNotFound, StationNotFound
    {
        Line line = this.get(id);
        Station station = this.stationService.get(stationId);

        line.getStations().remove(station);
        return this.lineRepository.save(line);
    }

    @Transactional(rollbackOn = { StationNotFound.class })
    public Line removeStations(long id, List<Long> stationIds)
        throws LineNotFound, StationNotFound
    {
        Line line = this.get(id);
        Set<Station> stations = line.getStations();

        for(Long stationId: stationIds) {
            stations.remove(this.stationService.get(stationId));
        }

        return this.lineRepository.save(line);
    }

    public Set<Station> getStations(long lineId) throws LineNotFound {
        Line line = this.get(lineId);
        return line.getStations();
    }

    public List<Schedule> getSchedules(long lineId) throws LineNotFound {
        Line line = this.get(lineId);
        return line.getSchedules();
    }
}

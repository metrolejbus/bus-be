package com.metrolejbus.app.transportation.services;

import com.metrolejbus.app.transportation.forms.TimedLocationForm;
import com.metrolejbus.app.transportation.models.Location;
import com.metrolejbus.app.transportation.models.TimedLocation;
import com.metrolejbus.app.transportation.repositories.TimedLocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import javax.management.InvalidAttributeValueException;
import java.util.List;

@Service
public class TimedLocationService {

    @Autowired
    TimedLocationRepository timedLocationRepository;

    public TimedLocation save(TimedLocation timedLocation){
        return timedLocationRepository.saveAndFlush(timedLocation);
    }
    public TimedLocation findById(Long id) {
        return timedLocationRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    public List<TimedLocation> findAll(){
        return timedLocationRepository.findAll();
    }

    public TimedLocation update(TimedLocationForm data, Long id) throws InvalidAttributeValueException {
        TimedLocation timedLocation = findById(id);
        timedLocation.setLocation(new Location(data.getLat(), data.getLng()));
        timedLocation.setTimestamp(data.getTimestamp());
        return timedLocationRepository.saveAndFlush(timedLocation);
    }

    public TimedLocation deleteById(long id) {
        TimedLocation timedLocation = findById(id);
        timedLocation.setActive(false);
        return timedLocationRepository.save(timedLocation);
    }

}

package com.metrolejbus.app.transportation.services;

import com.metrolejbus.app.transportation.models.TimedLocation;
import com.metrolejbus.app.transportation.repositories.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;


@Service
public class LocationService {

    private final LocationRepository locations;

    @Autowired
    public LocationService(LocationRepository locations) {
        this.locations = locations;
    }

    public void update(long journeyId, float lat, float lng, String timestamp) {
        LocalDateTime ts = LocalDateTime.parse(timestamp);
        this.locations.save(journeyId, new TimedLocation(lat, lng, ts));
    }

    public TimedLocation get(long journeyId) {
        return this.locations.get(journeyId);
    }

}

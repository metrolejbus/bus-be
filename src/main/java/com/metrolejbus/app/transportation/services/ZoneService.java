package com.metrolejbus.app.transportation.services;

import com.metrolejbus.app.transportation.exceptions.ZoneNameNotAvailable;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.repositories.ZoneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ZoneService {

    private final ZoneRepository zones;

    @Autowired
    public ZoneService(ZoneRepository zones) {
        this.zones = zones;
    }

    public Zone create(String name) throws ZoneNameNotAvailable {
        try {
            return this.zones.save(new Zone(name));
        } catch (DataIntegrityViolationException e) {
            throw new ZoneNameNotAvailable(name);
        }
    }

    public Zone delete(long id) throws ZoneNotFound {
        Zone zone = this.get(id);
        zone.setActive(false);
        return this.zones.save(zone);
    }

    public Zone get(long id) throws ZoneNotFound {
        return this.zones.findById(id).orElseThrow(() -> new ZoneNotFound(id));
    }

    public List<Zone> getAll() {
        return this.zones.findAll();
    }

    public Zone update(long id, String name)
        throws ZoneNameNotAvailable, ZoneNotFound
    {
        Zone zone = this.get(id);
        zone.setName(name);
        try {
            return this.zones.save(zone);
        } catch (DataIntegrityViolationException e) {
            throw new ZoneNameNotAvailable(name);
        }
    }

}

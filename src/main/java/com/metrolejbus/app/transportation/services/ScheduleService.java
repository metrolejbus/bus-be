package com.metrolejbus.app.transportation.services;

import com.metrolejbus.app.common.models.ValidityPeriod;
import com.metrolejbus.app.transportation.exceptions.ScheduleNotFound;
import com.metrolejbus.app.transportation.forms.ScheduleForm;
import com.metrolejbus.app.transportation.models.Line;
import com.metrolejbus.app.transportation.models.Schedule;
import com.metrolejbus.app.transportation.repositories.LineRepository;
import com.metrolejbus.app.transportation.repositories.ScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import javax.management.InvalidAttributeValueException;
import java.time.LocalDate;
import java.util.List;

@Service
public class ScheduleService {

    @Autowired
    ScheduleRepository scheduleRepository;

    @Autowired
    LineRepository lineRepository;



    public List<Schedule> save(ScheduleForm scheduleForm) throws InvalidAttributeValueException {
        Schedule schedule = convertToSchedule(scheduleForm);
        Long lineId = scheduleForm.getLine();
        Line line = lineRepository.getOne(lineId);
        line.getSchedules().add(schedule);
        Line l = lineRepository.save(line);
        return l.getSchedules();
    }

    public Schedule findById(Long id) throws ScheduleNotFound {
        return scheduleRepository.findById(id).orElseThrow(() -> new ScheduleNotFound(id));

    }

    public List<Schedule> findAll(){
        return scheduleRepository.findAll();
    }

    public Schedule update(ScheduleForm data, Long id) throws InvalidAttributeValueException, ScheduleNotFound {
        Schedule schedule = findById(id);
        if (data.getStartDate().isBefore(LocalDate.now()) || data.getStartDate().isAfter(data.getEndDate()) || data.getEndDate().isBefore(LocalDate.now())) {
            throw new InvalidAttributeValueException(
                "Daily ticket can't be bought with a date in the past"
            );
        }
        schedule.getValidityPeriod().setStartDate(data.getStartDate());
        schedule.getValidityPeriod().setEndDate(data.getEndDate());
        schedule.setAppliesTo(data.getDays());
        schedule.setRouteA(data.getRouteA());
        schedule.setRouteB(data.getRouteB());

        return scheduleRepository.saveAndFlush(schedule);
    }

    public Schedule deleteById(long id) throws ScheduleNotFound {
        Schedule schedule = findById(id);
        schedule.setActive(false);
        return scheduleRepository.save(schedule);
    }

    public Schedule convertToSchedule(ScheduleForm scheduleForm) throws InvalidAttributeValueException {
        Schedule schedule = new Schedule();
        schedule.setActive(true);
        schedule.setRouteA(scheduleForm.getRouteA());
        schedule.setRouteB(scheduleForm.getRouteB());
        schedule.setValidityPeriod(new ValidityPeriod(scheduleForm.getStartDate(), scheduleForm.getEndDate()));
        schedule.setAppliesTo(scheduleForm.getDays());
        return schedule;
    }



}

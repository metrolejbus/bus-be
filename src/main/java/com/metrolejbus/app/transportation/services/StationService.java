package com.metrolejbus.app.transportation.services;

import com.metrolejbus.app.transportation.exceptions.StationNotFound;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Station;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.repositories.StationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class StationService {

    private final StationRepository stations;
    private final ZoneService zoneService;

    @Autowired
    public StationService(StationRepository stations, ZoneService zoneService) {
        this.stations = stations;
        this.zoneService = zoneService;
    }

    public Station create(String name, long zoneId, float lat, float lng)
        throws ZoneNotFound
    {
        Zone zone = this.zoneService.get(zoneId);
        return this.stations.save(new Station(name, zone, lat, lng));
    }

    public Station delete(long id) throws StationNotFound {
        Station station = this.get(id);
        station.setActive(false);
        return this.stations.save(station);
    }

    public Station get(long id) throws StationNotFound {
        return this.stations.findById(id)
            .orElseThrow(() -> new StationNotFound(id));
    }

    public List<Station> getAll() {
        return this.stations.findAll();
    }

    public Station update(long id, String name) throws StationNotFound {
        Station station = this.get(id);
        station.setName(name);
        return this.stations.save(station);
    }

    public Station update(long id, long zoneId)
        throws StationNotFound, ZoneNotFound
    {
        Station station = this.get(id);
        station.setZone(this.zoneService.get(zoneId));
        return this.stations.save(station);
    }


    public Station update(long id, String name, long zoneId)
        throws StationNotFound, ZoneNotFound
    {
        Station station = this.get(id);
        station.setName(name);
        station.setZone(this.zoneService.get(zoneId));
        return this.stations.save(station);
    }

}

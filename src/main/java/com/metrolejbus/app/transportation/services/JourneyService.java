package com.metrolejbus.app.transportation.services;

import com.metrolejbus.app.transportation.exceptions.JourneyNotFound;
import com.metrolejbus.app.transportation.exceptions.LineNotFound;
import com.metrolejbus.app.transportation.models.Journey;
import com.metrolejbus.app.transportation.models.Line;
import com.metrolejbus.app.transportation.repositories.JourneyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;


@Service
public class JourneyService {

    private final JourneyRepository journeys;
    private final LineService lineService;

    @Autowired
    public JourneyService(JourneyRepository journeys, LineService lineService) {
        this.journeys = journeys;
        this.lineService = lineService;
    }

    public Journey create(long lineId, String departureTime)
        throws LineNotFound
    {
        Line line = this.lineService.get(lineId);
        LocalDateTime dt = LocalDateTime.parse(
            departureTime,
            DateTimeFormatter.ISO_LOCAL_DATE_TIME
        );

        return journeys.save(new Journey(line, dt));
    }

    public Journey get(long id) throws JourneyNotFound {
        return this.journeys.findById(id)
            .orElseThrow(() -> new JourneyNotFound(id));
    }

    public Journey update(long id, String arrivalTime) throws JourneyNotFound {
        Journey journey = this.get(id);
        LocalDateTime dt = LocalDateTime.parse(
            arrivalTime,
            DateTimeFormatter.ISO_LOCAL_DATE_TIME
        );

        journey.setArrivalTime(dt);

        return journeys.save(journey);
    }

    public List<Journey> getAll() {
        return this.journeys.findAll();
    }

    public List<Journey> getAll(long lineId) {
        return this.journeys.findAllByLine_Id(lineId);
    }

}

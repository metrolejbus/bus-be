package com.metrolejbus.app.transportation.forms;

import javax.validation.constraints.*;


public class CreateStation {
    @NotBlank(message = "Name must be provided and can't be empty")
    public String name;

    @NotNull(message = "Zone ID must be provided")
    @Positive(message = "Zone ID can't be a positive integer")
    public Long zoneId;

    @NotNull(message = "Latitude must be provided")
    @DecimalMin(value = "-90.0", message = "Latitude value can't be less then" +
        " -90.0")
    @DecimalMax(value = "90.0", message = "Latitude value can't be greater " +
        "then 90.0")
    public Float lat;

    @NotNull(message = "Longitude must be provided")
    @DecimalMin(value = "-180.0", message = "Longitude value can't be less " +
        "then -180.0")
    @DecimalMax(value = "180.0", message = "Longitude value can't be greater " +
        "then 180.0")
    public Float lng;

}

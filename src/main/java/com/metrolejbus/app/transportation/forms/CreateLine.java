package com.metrolejbus.app.transportation.forms;

import com.metrolejbus.app.transportation.models.TransportationType;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


public class CreateLine {
    @NotBlank(message = "Name must be provided and can't be empty")
    public String name;
    @NotNull(message = "Line type must be provided")
    public TransportationType type;
    @NotBlank(message = "Line must have a defined route")
    public String route;
}

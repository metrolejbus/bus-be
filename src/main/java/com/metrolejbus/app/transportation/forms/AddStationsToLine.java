package com.metrolejbus.app.transportation.forms;


import javax.validation.constraints.NotEmpty;
import java.util.List;


public class AddStationsToLine {
    @NotEmpty(message = "Must include at least one station")
    public List<Long> stationIds;
}

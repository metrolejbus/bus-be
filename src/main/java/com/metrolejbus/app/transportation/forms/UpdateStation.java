package com.metrolejbus.app.transportation.forms;

import javax.validation.constraints.Positive;


public class UpdateStation {
    public String name;

    @Positive(message = "Zone ID must be a positive integer")
    public Long zoneId;

}

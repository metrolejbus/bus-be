package com.metrolejbus.app.transportation.forms;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;


public class UpdateJourney {
    @NotNull(message = "Arrival time must be provided")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    public String arrivalTime;
}

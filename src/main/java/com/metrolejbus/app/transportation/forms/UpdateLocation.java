package com.metrolejbus.app.transportation.forms;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


public class UpdateLocation {
    @NotNull(message = "Line ID must be provided")
    public Long lineId;

    @DecimalMin(value = "-90.0", message = "Latitude value can't be less then" +
        " -90.0")
    @DecimalMax(value = "90.0", message = "Latitude value can't be greater " +
        "then 90.0")
    public Float lat;

    @DecimalMin(value = "-180.0", message = "Longitude value can't be less " +
        "then -180.0")
    @DecimalMax(value = "180.0", message = "Longitude value can't be greater " +
        "then 180.0")
    public Float lng;

    @NotBlank(message = "Timestamp must be provided and can't be empty")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    public String timestamp;

}

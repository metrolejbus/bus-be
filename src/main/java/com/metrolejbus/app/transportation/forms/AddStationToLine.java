package com.metrolejbus.app.transportation.forms;

import javax.validation.constraints.NotNull;


public class AddStationToLine {
    @NotNull(message = "Station ID must be provided")
    public Long stationId;

}

package com.metrolejbus.app.transportation.forms;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;


public class CreateJourney {

    @NotNull(message = "Line ID must be provided")
    @Positive(message = "Line ID can't be a positive integer")
    public Long lineId;

    @NotNull(message = "Departure time must be provided")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    public String departureTime;

}

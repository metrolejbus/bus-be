package com.metrolejbus.app.transportation.forms;

import javax.validation.constraints.NotBlank;


public class UpdateZone {
    @NotBlank(message = "Name must be provided and can't be empty")
    public String name;
}

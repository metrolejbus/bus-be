package com.metrolejbus.app.transportation.forms;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.metrolejbus.app.transportation.models.Line;

import javax.validation.constraints.NotNull;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;


public class ScheduleForm {

    @NotNull
    @JsonFormat(pattern = "HH:mm")
    private List<LocalTime> routeA;

    @NotNull
    @JsonFormat(pattern = "HH:mm")
    private List<LocalTime> routeB;

    @NotNull
    private List<DayOfWeek> days;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @NotNull
    private Long line;

    public ScheduleForm(){}

    public ScheduleForm(List<LocalTime> routeA, List<LocalTime> routeB, List<DayOfWeek> days, LocalDate startDate, LocalDate endDate, Long line) {
        this.routeA = routeA;
        this.routeB = routeB;
        this.days = days;
        this.startDate = startDate;
        this.endDate = endDate;
        this.line = line;
    }

    public List<LocalTime> getRouteA() {
        return routeA;
    }

    public void setRouteA(List<LocalTime> routeA) {
        this.routeA = routeA;
    }

    public List<LocalTime> getRouteB() {
        return routeB;
    }

    public void setRouteB(List<LocalTime> routeB) {
        this.routeB = routeB;
    }

    public List<DayOfWeek> getDays() {
        return days;
    }

    public void setDays(List<DayOfWeek> days) {
        this.days = days;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Long getLine() {
        return line;
    }

    public void setLine(Long line) {
        this.line = line;
    }
}
package com.metrolejbus.app.transportation.forms;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotNull;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

public class TimedLocationForm {
    @NotNull
    private Long lat;

    @NotNull
    private Long lng;

    @NotNull
    @JsonFormat(pattern = "dd:MM:yyyy HH:mm")
    private LocalDateTime timestamp;

    public TimedLocationForm(){}

    public TimedLocationForm(@NotNull Long lat, @NotNull Long lng, @NotNull LocalDateTime timestamp) {
        this.lat = lat;
        this.lng = lng;
        this.timestamp = timestamp;
    }

    public Long getLat() {
        return lat;
    }

    public void setLat(Long lat) {
        this.lat = lat;
    }

    public Long getLng() {
        return lng;
    }

    public void setLng(Long lng) {
        this.lng = lng;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }
}

package com.metrolejbus.app.transportation.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;


@Entity
@Table(name = "journeys")
@Where(clause = "active=1")
public class Journey {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "journey_id", updatable = false, nullable = false)
    private Long id;

    private LocalDateTime departureTime;
    private LocalDateTime arrivalTime;

    @ManyToOne(optional = false, targetEntity = Line.class)
    @JoinColumn(name = "line_id", referencedColumnName = "line_id")
    private Line line;

    @Transient
    private List<TimedLocation> locations;

    @JsonIgnore
    private boolean active;

    public Journey() {

    }

    public Journey(Line line, LocalDateTime departureTime) {
        if (line == null) {
            throw new NullPointerException("Line can't be null");
        }

        if (departureTime == null) {
            throw new NullPointerException("Departure time can't be null");
        }

        this.line = line;
        this.departureTime = departureTime;
        this.arrivalTime = null;
        this.locations = new LinkedList<>();
        this.active = true;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getDepartureTime() {
        return departureTime;
    }

    public LocalDateTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(LocalDateTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Line getLine() {
        return line;
    }

    public List<TimedLocation> getLocations() {
        return locations;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        Journey journey = (Journey) o;
        return Objects.equals(id, journey.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Journey{" +
            "id=" + id +
            ", departureTime=" + departureTime +
            ", arrivalTime=" + arrivalTime +
            ", line=" + line +
            ", active=" + active + '}';
    }

}

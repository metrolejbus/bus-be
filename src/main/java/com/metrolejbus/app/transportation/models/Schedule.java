package com.metrolejbus.app.transportation.models;

import com.metrolejbus.app.common.models.ValidityPeriod;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;


@Entity
@Table(name = "schedules")
@Where(clause ="active = 1")
public class Schedule {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "schedule_id", updatable = false, nullable = false)
    private Long id;

    @ElementCollection
    @CollectionTable(name = "schedule_applies_to", joinColumns =
    @JoinColumn(name = "schedule_id"))
    @Column(name = "applies_to", nullable = false)
    private List<DayOfWeek> appliesTo;

    @ElementCollection
    @CollectionTable(name = "schedule_route_a", joinColumns =
    @JoinColumn(name = "schedule_id"))
    @Column(name = "route_a")
    private List<LocalTime> routeA;

    @ElementCollection
    @CollectionTable(name = "schedule_route_b", joinColumns =
    @JoinColumn(name = "schedule_id"))
    @Column(name = "route_b")
    private List<LocalTime> routeB;

    @Embedded
    private ValidityPeriod validityPeriod;

    @NotNull
    private Boolean active;

    public Schedule() {

    }

    public Schedule(
        List<DayOfWeek> appliesTo,
        List<LocalTime> routeA,
        List<LocalTime> routeB,
        ValidityPeriod validityPeriod
    ) {
        if (validityPeriod == null) {
            throw new NullPointerException("Validity period can't be null");
        }

        this.appliesTo = appliesTo != null ? appliesTo : new LinkedList<>();
        this.routeA = routeA != null ? routeA : new LinkedList<>();
        this.routeB = routeB != null ? routeB : new LinkedList<>();
        this.active = true;
        this.validityPeriod = validityPeriod;
    }


    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Long getId() {
        return id;
    }

    public List<DayOfWeek> getAppliesTo() {
        return appliesTo;
    }

    public List<LocalTime> getRouteA() {
        return routeA;
    }

    public List<LocalTime> getRouteB() {
        return routeB;
    }

    public ValidityPeriod getValidityPeriod() {
        return validityPeriod;
    }

    public void setValidityPeriod(ValidityPeriod validityPeriod) {
        if (validityPeriod == null) {
            throw new NullPointerException("Validity period can't be null");
        }
        this.validityPeriod = validityPeriod;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAppliesTo(List<DayOfWeek> appliesTo) {
        this.appliesTo = appliesTo;
    }

    public void setRouteA(List<LocalTime> routeA) {
        this.routeA = routeA;
    }

    public void setRouteB(List<LocalTime> routeB) {
        this.routeB = routeB;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        Schedule schedule = (Schedule) o;
        return Objects.equals(id, schedule.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Schedule{" +
            "id=" + id +
            ", appliesTo=" + appliesTo +
            ", routeA=" + routeA +
            ", routeB=" + routeB +
            ", validityPeriod=" + validityPeriod + '}';
    }

}

package com.metrolejbus.app.transportation.models;

import org.hibernate.annotations.Where;

import javax.management.InvalidAttributeValueException;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "timed_locations")
@Where(clause ="active = 1")
public class TimedLocation {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "timed_location_id", updatable = false, nullable = false)
    private Long id;

    private Location location;

    @Column(nullable = false)
    private LocalDateTime timestamp;

    @NotNull
    private Boolean active;

    public TimedLocation() {

    }

    public TimedLocation(float lat, float lng, LocalDateTime timestamp)
        throws IllegalArgumentException
    {
        this.location = new Location(lat, lng);
        this.timestamp = timestamp;
    }

    public Location getLocation() {
        return location;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        TimedLocation that = (TimedLocation) o;
        return Objects.equals(location, that.location) && Objects.equals(
            timestamp,
            that.timestamp
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(location, timestamp);
    }

    @Override
    public String toString() {
        return "TimedLocation{" +
            "location=" + location +
            ", timestamp=" + timestamp + '}';
    }

}

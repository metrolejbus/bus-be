package com.metrolejbus.app.transportation.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.*;


@Entity
@Table(name = "trans_lines")
@Where(clause = "active=1")
public class Line {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "line_id", updatable = false, nullable = false)
    private Long id;

    @Column(nullable = false)
    private String name;

    @JsonProperty(value = "type")
    @Column(name = "trans_type", nullable = false)
    @Enumerated(EnumType.STRING)
    private TransportationType transportationType;

    @ManyToMany(
        targetEntity = Station.class,
        cascade = { CascadeType.PERSIST, CascadeType.MERGE },
        fetch = FetchType.EAGER
    )
    @JoinTable(
        name = "line_stops",
        joinColumns = @JoinColumn(name = "line_id"),
        inverseJoinColumns = @JoinColumn(name = "station_id")
    )
    private Set<Station> stations;

    @OneToMany(targetEntity = Schedule.class, orphanRemoval = true,  cascade = CascadeType.ALL)
    @JoinColumn(name = "line_id")
    private List<Schedule> schedules;

    @Column(length = 32768)
    private String route;

    @JsonIgnore
    private boolean active;

    public Line() {

    }

    public Line(String name, TransportationType type, String route) {
        if (name == null) {
            throw new NullPointerException("Name can't be null");
        }

        if (type == null) {
            throw new NullPointerException("Transportation type can't be null");
        }

        if (route == null) {
            throw new NullPointerException("Route can't be null");
        }

        this.name = name;
        this.transportationType = type;
        this.route = route;
        this.stations = new HashSet<>();
        this.schedules = new LinkedList<>();
        this.active = true;
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null) {
            throw new NullPointerException("Name can't be null");
        }
        this.name = name;
    }

    public TransportationType getTransportationType() {
        return transportationType;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        if (route == null) {
            throw new NullPointerException("Route can't be null");
        }
        this.route = route;
    }

    public Set<Station> getStations() {
        return stations;
    }

    public List<Schedule> getSchedules() {
        return schedules;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        Line line = (Line) o;
        return Objects.equals(id, line.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Line{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", transportationType=" + transportationType +
            ", active=" + active + '}';
    }

}

package com.metrolejbus.app.transportation.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name = "stations")
@Where(clause = "active=1")
public class Station {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "station_id", updatable = false, nullable = false)
    private Long id;

    private String name;

    @ManyToOne(optional = false, targetEntity = Zone.class)
    @JoinColumn(name = "zone_id", referencedColumnName = "zone_id")
    private Zone zone;

    @Embedded
    private Location location;

    @JsonIgnore
    private boolean active;

    public Station() {

    }

    public Station(String name, Zone zone, float lat, float lng) {
        if (name == null) {
            throw new NullPointerException("Name can't be null");
        }

        if (zone == null) {
            throw new NullPointerException("Zone can't be null");
        }

        this.name = name;
        this.zone = zone;
        this.active = true;
        this.location = new Location(lat, lng);
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null) {
            throw new NullPointerException("Name can't be null");
        }
        this.name = name;
    }

    public Zone getZone() {
        return zone;
    }

    public void setZone(Zone zone) {
        if (zone == null) {
            throw new NullPointerException("Zone can't be null");
        }
        this.zone = zone;
    }

    public Location getLocation() {
        return location;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        Station station = (Station) o;
        return Objects.equals(id, station.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Station{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", zone=" + zone +
            ", location=" + location +
            ", active=" + active + '}';
    }

}

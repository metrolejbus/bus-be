package com.metrolejbus.app.transportation.models;

import javax.persistence.Embeddable;
import java.util.Objects;


@Embeddable
public class Location {
    private float lat;
    private float lng;

    public Location() {

    }

    public Location(float lat, float lng) throws IllegalArgumentException {
        if (this.isInvalidLatitude(lat)) {
            throw new IllegalArgumentException(
                "Latitude has to be between -90.0 and 90.0");
        }

        if (this.isInvalidLongitude(lng)) {
            throw new IllegalArgumentException(
                "Longitude has to be between -180.0 and 180.0");
        }

        this.lat = lat;
        this.lng = lng;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) throws IllegalArgumentException {
        if (this.isInvalidLatitude(lat)) {
            throw new IllegalArgumentException(
                "Latitude has to be between -90.0 and 90.0");
        }

        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) throws IllegalArgumentException {
        if (this.isInvalidLongitude(lng)) {
            throw new IllegalArgumentException(
                "Longitude has to be between -180.0 and 180.0");
        }

        this.lng = lng;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) { return true; }
        if (o == null || getClass() != o.getClass()) { return false; }
        Location location = (Location) o;
        return Float.compare(
            location.lat,
            lat
        ) == 0 && Float.compare(location.lng, lng) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(lat, lng);
    }

    @Override
    public String toString() {
        return "Location{" + "lat=" + lat + ", lng=" + lng + '}';
    }

    private boolean isInvalidLatitude(float value) {
        return value < -90 || value > 90;
    }

    private boolean isInvalidLongitude(float value) {
        return value < -180 || value > 180;
    }

}

package com.metrolejbus.app.transportation.models;

public enum TransportationType {
    BUS, METRO, TRAM
}

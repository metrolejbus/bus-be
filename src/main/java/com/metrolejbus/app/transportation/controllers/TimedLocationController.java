package com.metrolejbus.app.transportation.controllers;


import com.metrolejbus.app.common.models.ValidityPeriod;
import com.metrolejbus.app.transportation.forms.ScheduleForm;
import com.metrolejbus.app.transportation.forms.TimedLocationForm;
import com.metrolejbus.app.transportation.models.Location;
import com.metrolejbus.app.transportation.models.Schedule;
import com.metrolejbus.app.transportation.models.TimedLocation;
import com.metrolejbus.app.transportation.services.ScheduleService;
import com.metrolejbus.app.transportation.services.TimedLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.management.InvalidAttributeValueException;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/timed-locations")
public class TimedLocationController {


    private TimedLocationService timedLocationService;

    public TimedLocationController(@Autowired TimedLocationService timedLocationService) {
        this.timedLocationService = timedLocationService;
    }

    @PostMapping
    public ResponseEntity<TimedLocation> create(@Valid @RequestBody TimedLocationForm timedLocationForm ){
        try {
            TimedLocation timedLocation = convertToTimedLocation(timedLocationForm);
            timedLocationService.save(timedLocation);
            return new ResponseEntity<>(timedLocation, HttpStatus.OK);
        }
        catch (InvalidAttributeValueException e){
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @GetMapping
    public ResponseEntity<List<TimedLocation>> findAll() {
        return ResponseEntity.ok(timedLocationService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<TimedLocation> findById(@PathVariable Long id) {
        TimedLocation timedLocation;
            timedLocation = timedLocationService.findById(id);
        return ResponseEntity.ok(timedLocation);
    }

    @PutMapping("/{id}")
    public ResponseEntity<TimedLocation> update(@PathVariable Long id, @Valid @RequestBody TimedLocationForm data) {
        TimedLocation timedLocation;
        try {
            timedLocation = timedLocationService.update(data, id);
        } catch (InvalidAttributeValueException e) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(timedLocation);
    }

    public TimedLocation convertToTimedLocation(TimedLocationForm timedLocationForm) throws InvalidAttributeValueException {
        TimedLocation timedLocation = new TimedLocation();
        timedLocation.setActive(true);
        timedLocation.setLocation(new Location(timedLocationForm.getLat(), timedLocationForm.getLng()));
        timedLocation.setTimestamp(timedLocationForm.getTimestamp());

        return timedLocation;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<TimedLocation> deleteById(@PathVariable long id) {
        TimedLocation timedLocation;
        try {
            timedLocation = timedLocationService.deleteById(id);
        }catch (ResourceNotFoundException e){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(timedLocation);
    }


}

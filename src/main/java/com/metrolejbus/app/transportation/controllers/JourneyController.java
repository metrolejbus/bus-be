package com.metrolejbus.app.transportation.controllers;

import com.metrolejbus.app.common.exceptions.BadRequest;
import com.metrolejbus.app.common.exceptions.NotFound;
import com.metrolejbus.app.common.services.Sockets;
import com.metrolejbus.app.transportation.exceptions.JourneyNotFound;
import com.metrolejbus.app.transportation.exceptions.LineNotFound;
import com.metrolejbus.app.transportation.forms.CreateJourney;
import com.metrolejbus.app.transportation.forms.UpdateJourney;
import com.metrolejbus.app.transportation.forms.UpdateLocation;
import com.metrolejbus.app.transportation.models.Journey;
import com.metrolejbus.app.transportation.services.JourneyService;
import jdk.nashorn.internal.parser.JSONParser;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.format.DateTimeParseException;
import java.util.List;


@RestController
@RequestMapping("/journeys")
public class JourneyController {

    private JourneyService journeyService;

    public JourneyController(@Autowired JourneyService journeyService) {
        this.journeyService = journeyService;
    }

    @GetMapping
    public List<Journey> getAll() {
        return this.journeyService.getAll();
    }

    @PostMapping
    @PreAuthorize("hasAuthority('SYSTEM')")
    @ResponseStatus(value = HttpStatus.CREATED)
    public Journey create(@Valid @RequestBody CreateJourney form)
        throws NotFound, BadRequest
    {
        try {
            return this.journeyService.create(form.lineId, form.departureTime);
        } catch (LineNotFound e) {
            throw new NotFound(e.getMessage());
        } catch (DateTimeParseException e) {
            throw new BadRequest(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    @PreAuthorize("permitAll()")
    public Journey get(@PathVariable long id) throws NotFound {
        try {
            return this.journeyService.get(id);
        } catch (JourneyNotFound e) {
            throw new NotFound(e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('SYSTEM')")
    public Journey update(
        @PathVariable long id,
        @Valid @RequestBody UpdateJourney form
    ) throws NotFound, BadRequest
    {
        try {
            Journey journey = this.journeyService.update(id, form.arrivalTime);

            JSONObject data = new JSONObject();
            data.put("journeyId", journey.getId());
            System.out.println(data);

            JSONObject message = new JSONObject();
            message.put("name", "journey-end");
            message.put("data", data);

            Sockets.getSocket().emit("event", message);

            return journey;

        } catch (JourneyNotFound e) {
            throw new NotFound(e.getMessage());
        } catch (DateTimeParseException | JSONException e) {
            throw new BadRequest(e.getMessage());
        }
    }

    @PutMapping("/{id}/location")
    @PreAuthorize("hasAuthority('SYSTEM')")
    public void reportLocation(
        @PathVariable long id, @RequestBody UpdateLocation form
    ) throws JSONException
    {
        JSONObject data = new JSONObject();
        data.put("lineId", form.lineId);
        data.put("journeyId", id);
        data.put("lat", form.lat);
        data.put("lng", form.lng);
        data.put("timestamp", form.timestamp);

        System.out.println(data);

        JSONObject message = new JSONObject();
        message.put("name", "journey-location");
        message.put("data", data);

        Sockets.getSocket().emit("event", message);
    }
}

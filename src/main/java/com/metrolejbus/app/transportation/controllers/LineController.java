package com.metrolejbus.app.transportation.controllers;

import com.metrolejbus.app.common.exceptions.BadRequest;
import com.metrolejbus.app.common.exceptions.NotFound;
import com.metrolejbus.app.transportation.exceptions.LineNotFound;
import com.metrolejbus.app.transportation.exceptions.StationNotFound;
import com.metrolejbus.app.transportation.forms.*;
import com.metrolejbus.app.transportation.models.Journey;
import com.metrolejbus.app.transportation.models.Line;
import com.metrolejbus.app.transportation.models.Schedule;
import com.metrolejbus.app.transportation.models.Station;
import com.metrolejbus.app.transportation.services.JourneyService;
import com.metrolejbus.app.transportation.services.LineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;


@RestController
@RequestMapping("/lines")
public class LineController {

    private final LineService lineService;
    private final JourneyService journeyService;


    @Autowired
    public LineController(
        LineService lineService,
        JourneyService journeyService
    ) {
        this.lineService = lineService;
        this.journeyService = journeyService;
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    @ResponseStatus(code = HttpStatus.CREATED)
    public Line create(@Valid @RequestBody CreateLine form) {
        return this.lineService.create(form.name, form.type, form.route);
    }

    @GetMapping("/{id}")
    @PreAuthorize("permitAll()")
    public Line get(@PathVariable long id) throws NotFound {
        try {
            return this.lineService.get(id);
        } catch (LineNotFound e) {
            throw new NotFound(e.getMessage());
        }
    }

    @GetMapping
    @PreAuthorize("permitAll()")
    public List<Line> getAll(@RequestParam(name = "station",
        required = false) Long stationId) {
        if (stationId != null) {
            return this.lineService.getAll(stationId);
        } else {
            return this.lineService.getAll();
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Line delete(@PathVariable long id) throws NotFound {
        try {
            return this.lineService.delete(id);
        } catch (LineNotFound e) {
            throw new NotFound(e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Line update(
        @PathVariable long id,
        @Valid @RequestBody UpdateLine form
    ) throws NotFound, BadRequest {
        try {
            if (form.name == null) {
                return this.lineService.get(id);
            }

            if (form.name.length() == 0) {
                throw new BadRequest("Line name can't be blank");
            }

            if (form.route == null) {
                return this.lineService.update(id, form.name);
            }

            if (form.route.length() == 0) {
                throw new BadRequest("Route can't be blank");
            }

            return this.lineService.update(id, form.name, form.route);

        } catch (LineNotFound e) {
            throw new NotFound(e.getMessage());
        }
    }

    @PostMapping("/{id}/stations")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Line addStation(
        @PathVariable long id,
        @Valid @RequestBody AddStationToLine form
    ) throws NotFound {
        try {
            return this.lineService.addStation(id, form.stationId);
        } catch (LineNotFound | StationNotFound e) {
            throw new NotFound(e.getMessage());
        }
    }

    @PostMapping("/{id}/stations/bulk")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Line addStations(
        @PathVariable long id,
        @Valid @RequestBody AddStationsToLine form
    ) throws NotFound {
        try {
            return this.lineService.addStations(id, form.stationIds);
        } catch (LineNotFound | StationNotFound e) {
            throw new NotFound(e.getMessage());
        }
    }

    @GetMapping("/{id}/stations")
    @PreAuthorize("permitAll()")
    public Set<Station> getStations(@PathVariable long id) throws NotFound {
        try {
            return this.lineService.getStations(id);
        } catch (LineNotFound e) {
            throw new NotFound(e.getMessage());
        }
    }

    @DeleteMapping("/{id}/stations/{stationId}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Line removeStation(
        @PathVariable long id,
        @PathVariable long stationId
    ) throws NotFound {
        try {
            return this.lineService.removeStation(id, stationId);
        } catch (LineNotFound | StationNotFound e) {
            throw new NotFound(e.getMessage());
        }
    }

    @DeleteMapping("/{id}/stations/bulk")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Line removeStations(
        @PathVariable long id,
        @Valid @RequestBody RemoveStationsFromLine form
    ) throws NotFound {
        try {
            return this.lineService.removeStations(id, form.stationIds);
        } catch (LineNotFound | StationNotFound e) {
            throw new NotFound(e.getMessage());
        }
    }

    @GetMapping("/{id}/journeys")
    @PreAuthorize("permitAll()")
    public List<Journey> getJourneys(@PathVariable long id) throws NotFound {
        if (!this.lineService.exists(id)) {
            throw new NotFound("Line [" + id + "] doesn't exist");
        }
        return this.journeyService.getAll(id);
    }

    @GetMapping("/{id}/schedules")
    @PreAuthorize("permitAll()")
    public List<Schedule> getSchedules(@PathVariable long id) throws NotFound {
        try {
            return this.lineService.getSchedules(id);
        } catch (LineNotFound e) {
            throw new NotFound(e.getMessage());
        }
    }




}

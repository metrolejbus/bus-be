package com.metrolejbus.app.transportation.controllers;

import com.metrolejbus.app.common.exceptions.BadRequest;
import com.metrolejbus.app.common.exceptions.NotFound;
import com.metrolejbus.app.transportation.exceptions.ZoneNameNotAvailable;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.forms.CreateZone;
import com.metrolejbus.app.transportation.forms.UpdateZone;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.services.ZoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/zones")
public class ZoneController {

    private final ZoneService service;

    @Autowired
    public ZoneController(ZoneService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    @PreAuthorize("permitAll()")
    public Zone get(@PathVariable long id) throws NotFound {
        try {
            return this.service.get(id);
        } catch (ZoneNotFound e) {
            throw new NotFound(e.getMessage());
        }
    }

    @GetMapping
    @PreAuthorize("permitAll()")
    public List<Zone> getAll() {
        return this.service.getAll();
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Zone delete(@PathVariable long id) throws NotFound {
        try {
            return this.service.delete(id);
        } catch (ZoneNotFound e) {
            throw new NotFound(e.getMessage());
        }
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    @ResponseStatus(code = HttpStatus.CREATED)
    public Zone create(@Valid @RequestBody CreateZone form) throws BadRequest {
        try {
            return this.service.create(form.name);
        } catch (ZoneNameNotAvailable e) {
            throw new BadRequest(e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Zone update(
        @PathVariable long id, @Valid @RequestBody UpdateZone form
    ) throws NotFound, BadRequest {
        try {
            return this.service.update(id, form.name);
        } catch (ZoneNotFound e) {
            throw new NotFound(e.getMessage());
        } catch (ZoneNameNotAvailable e) {
            throw new BadRequest(e.getMessage());
        }
    }

}

package com.metrolejbus.app.transportation.controllers;

import com.metrolejbus.app.common.exceptions.BadRequest;
import com.metrolejbus.app.common.exceptions.NotFound;
import com.metrolejbus.app.transportation.exceptions.StationNotFound;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.forms.CreateStation;
import com.metrolejbus.app.transportation.forms.UpdateStation;
import com.metrolejbus.app.transportation.models.Station;
import com.metrolejbus.app.transportation.services.StationService;
import com.metrolejbus.app.transportation.services.ZoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/stations")
public class StationController {

    @Autowired
    private StationService stationService;
    @Autowired
    private ZoneService zoneService;

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Station delete(@PathVariable long id) throws NotFound {
        try {
            return this.stationService.delete(id);
        } catch (StationNotFound e) {
            throw new NotFound(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    @PreAuthorize("permitAll()")
    public Station get(@PathVariable long id) throws NotFound {
        try {
            return this.stationService.get(id);
        } catch (StationNotFound e) {
            throw new NotFound(e.getMessage());
        }
    }

    @GetMapping
    @PreAuthorize("permitAll()")
    public List<Station> getAll() {
        return this.stationService.getAll();
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    @ResponseStatus(code = HttpStatus.CREATED)
    public Station create(@Valid @RequestBody CreateStation form)
        throws NotFound
    {
        try {
            return this.stationService.create(
                form.name,
                form.zoneId,
                form.lat,
                form.lng
            );
        } catch (ZoneNotFound e) {
            throw new NotFound(e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public Station update(
        @PathVariable long id, @Valid @RequestBody UpdateStation form
    ) throws NotFound, BadRequest {
        try {
            if (form.name != null && form.zoneId != null) {
                if (form.name.length() == 0) {
                    throw new BadRequest("Station name can't be blank");
                }

                return this.stationService.update(id, form.name, form.zoneId);
            } else if (form.name != null) {
                if (form.name.length() == 0) {
                    throw new BadRequest("Station name can't be blank");
                }

                return this.stationService.update(id, form.name);
            } else if (form.zoneId != null) {
                return this.stationService.update(id, form.zoneId);
            } else {
                return this.stationService.get(id);
            }
        } catch (StationNotFound | ZoneNotFound e) {
            throw new NotFound(e.getMessage());
        }
    }

}

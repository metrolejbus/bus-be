package com.metrolejbus.app.transportation.controllers;

import com.metrolejbus.app.common.models.ValidityPeriod;
import com.metrolejbus.app.transportation.exceptions.ScheduleNotFound;
import com.metrolejbus.app.transportation.forms.ScheduleForm;
import com.metrolejbus.app.transportation.models.Schedule;
import com.metrolejbus.app.transportation.services.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.management.InvalidAttributeValueException;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/schedules")
public class ScheduleController {

    private ScheduleService scheduleService;


    public ScheduleController(@Autowired ScheduleService scheduleService) {
        this.scheduleService = scheduleService;
    }


    @PostMapping
    public ResponseEntity<List<Schedule>> create(@Valid @RequestBody ScheduleForm scheduleForm ){
        try {
            List<Schedule> schedules = scheduleService.save(scheduleForm);
            return new ResponseEntity<>(schedules, HttpStatus.OK);
        }
        catch (InvalidAttributeValueException e){
            return new ResponseEntity<>(HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @GetMapping
    public ResponseEntity<List<Schedule>> findAll() {
        List<Schedule> allSchedules = scheduleService.findAll();

        return ResponseEntity.ok(allSchedules);
        }

    @GetMapping("/{id}")
    public ResponseEntity<Schedule> findById(@PathVariable Long id) {
            Schedule schedule;
            try {
                schedule = scheduleService.findById(id);
            } catch (ResourceNotFoundException e){
                return ResponseEntity.notFound().build();
            } catch (ScheduleNotFound e){
                return ResponseEntity.notFound().build();
            }
            return ResponseEntity.ok(schedule);
        }

    @PutMapping("/{id}")
    public ResponseEntity<Schedule> update(@PathVariable Long id, @Valid @RequestBody ScheduleForm data) {
        Schedule schedule;
        try {
            schedule = scheduleService.update(data, id);
        } catch (ScheduleNotFound e) {
            return ResponseEntity.badRequest().build();
        }catch (InvalidAttributeValueException e) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(schedule);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Schedule> deleteById(@PathVariable long id) {
        Schedule schedule;
        try {
            schedule = scheduleService.deleteById(id);
        } catch (ScheduleNotFound e) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(schedule);
    }
    
}

package com.metrolejbus.app.transportation.repositories;

import com.metrolejbus.app.transportation.models.Zone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZoneRepository extends JpaRepository<Zone, Long> {

    Zone findByName(String name);
}

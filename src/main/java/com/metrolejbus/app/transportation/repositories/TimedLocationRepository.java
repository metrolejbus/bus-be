package com.metrolejbus.app.transportation.repositories;

import com.metrolejbus.app.transportation.models.TimedLocation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TimedLocationRepository extends JpaRepository<TimedLocation, Long> {}


package com.metrolejbus.app.transportation.repositories;

import com.metrolejbus.app.transportation.models.TimedLocation;
import org.springframework.stereotype.Repository;


@Repository
public class LocationRepository {

    public void save(long journeyId, TimedLocation location) {
        //TODO: save to redis store
    }

    public TimedLocation get(long journeyId) {
        //TODO: get from redis store
        return null;
    }

}

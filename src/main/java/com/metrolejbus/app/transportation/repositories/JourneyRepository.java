package com.metrolejbus.app.transportation.repositories;

import com.metrolejbus.app.transportation.models.Journey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface JourneyRepository extends JpaRepository<Journey, Long> {
    List<Journey> findAllByLine_Id(long lineId);

}

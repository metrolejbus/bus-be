package com.metrolejbus.app.transportation.repositories;

import com.metrolejbus.app.transportation.models.Station;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface StationRepository extends JpaRepository<Station, Long> {}

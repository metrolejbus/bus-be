package com.metrolejbus.app.transportation.repositories;

import com.metrolejbus.app.transportation.models.Line;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface LineRepository extends JpaRepository<Line, Long> {
    List<Line> findAllByStations_Id(long stationId);
}

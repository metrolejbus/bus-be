package com.metrolejbus.app.customers.repositories;

import com.metrolejbus.app.authentication.models.User;
import com.metrolejbus.app.customers.models.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomersRepository extends JpaRepository<Customer, Long> {
    Customer findByUser(User user);
}

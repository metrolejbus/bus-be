package com.metrolejbus.app.customers.controllers;

import com.metrolejbus.app.authentication.models.User;
import com.metrolejbus.app.authentication.services.AuthService;
import com.metrolejbus.app.common.exceptions.BadRequest;
import com.metrolejbus.app.customers.forms.UpdateCustomerForm;
import com.metrolejbus.app.customers.models.Customer;
import com.metrolejbus.app.customers.services.CustomersService;
import com.metrolejbus.app.storage.FileSystemStorageService;
import com.metrolejbus.app.ticketing.models.DailyTicket;
import com.metrolejbus.app.ticketing.models.PeriodicTicket;
import com.metrolejbus.app.ticketing.models.SingleUseTicket;
import com.metrolejbus.app.ticketing.services.DailyTicketService;
import com.metrolejbus.app.ticketing.services.PeriodicTicketService;
import com.metrolejbus.app.ticketing.services.SingleUseTicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomersController {

    @Autowired
    private DailyTicketService dailyTicketService;

    @Autowired
    private PeriodicTicketService periodicTicketService;

    @Autowired
    private SingleUseTicketService singleUseTicketService;

    @Autowired
    private CustomersService customersService;

    @Autowired
    private AuthService authService;

    @Autowired
    private FileSystemStorageService storageService;

    @GetMapping("/tickets")
    public ResponseEntity<HashMap<String, Object>> getCustomersTickets() {

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentUserName = authentication.getName();
        User user = authService.findByUsername(currentUserName);

        List<SingleUseTicket> singleUseTickets = this.singleUseTicketService.findAllByCustomerId(user.getId());
        List<DailyTicket> dailyTickets = this.dailyTicketService.findAllByCustomerId(user.getId());
        List<PeriodicTicket> periodicTickets = this.periodicTicketService.findAllByCustomerId(user.getId());

        HashMap<String, Object> resp = new HashMap<>();
        resp.put("singleUseTickets", singleUseTickets);
        resp.put("dailyTickets", dailyTickets);
        resp.put("periodicTickets", periodicTickets);

        return ResponseEntity.ok(resp);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Customer> getCustomersTickets(@PathVariable long id) {
        Customer customer = this.customersService.findById(id);
        return ResponseEntity.ok(customer);
    }

    @PutMapping("/{id}")
    public Customer updateCustomer(
            @PathVariable Integer id,
            @Valid
            @RequestBody
                    UpdateCustomerForm data
    ) throws BadRequest {
        return this.customersService.updateCustomer(new Long(id), data.firstName, data.lastName, data.age);
    }

    @PostMapping("/personal-verification")
    public Customer uploadPersonalInfo(
            @RequestParam("file") MultipartFile file,
            Principal principal) {

        String filename = storageService.store(file);
        User user = authService.findByUsername(principal.getName());
        Customer customer = customersService.findByUser(user);
        customer.setTicketVerificationFileName(filename);
        customer = customersService.updateCustomer(customer);

        return customer;
    }
}

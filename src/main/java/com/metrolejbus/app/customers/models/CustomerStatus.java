package com.metrolejbus.app.customers.models;

public enum CustomerStatus {
    STUDENT, ADULT, SENIOR
}

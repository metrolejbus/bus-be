package com.metrolejbus.app.customers.models;

import com.metrolejbus.app.authentication.models.User;

import javax.management.InvalidAttributeValueException;
import javax.persistence.*;
import java.io.Serializable;
import java.net.URL;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "customers")
public class Customer implements Serializable {

    @Id
    @Column(name = "user_id", nullable = false)
    private Long userId;

    @OneToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    @MapsId
    private User user;


    private String firstName;
    private String middleName;
    private String lastName;
    private int age;
    private URL photoUrl;
    private String ticketVerificationFileName;
    private LocalDate approvedUntil;

    @Enumerated(EnumType.STRING)
    private CustomerStatus status;

    public Customer() {

    }

    public Customer(long id) {
        this.userId = id;
    }

    public Customer(
            User user,
            String firstName,
            String middleName,
            String lastName,
            int age,
            CustomerStatus status
    ) throws InvalidAttributeValueException {
        if (user == null) {
            throw new NullPointerException("User can't be null");
        }

        if (firstName == null) {
            throw new NullPointerException("First name can't be null");
        }

        if (lastName == null) {
            throw new NullPointerException("Last name can't be null");
        }

        if (status == null) {
            throw new NullPointerException("Customer status can't be null");
        }

        if (!this.isValidAge(age)) {
            throw new InvalidAttributeValueException(
                    "Not a valid age"
            );
        }

        this.user = user;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.age = age;
        this.status = status;
        this.photoUrl = null;
        this.ticketVerificationFileName = null;
        this.approvedUntil = null;
//        this.tickets = new LinkedList<>();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if (firstName == null) {
            throw new NullPointerException("First name can't be null");
        }
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        if (lastName == null) {
            throw new NullPointerException("Last name can't be null");
        }
        this.lastName = lastName;
    }

    public Long getId() {
        return userId;
    }

    public void setId(Long userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }


    public LocalDate getApprovedUntil() {
        return approvedUntil;
    }

    public void setApprovedUntil(LocalDate approvedUntil) {
        this.approvedUntil = approvedUntil;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws InvalidAttributeValueException {
        if (!this.isValidAge(age)) {
            throw new InvalidAttributeValueException(
                    "Not a valid age"
            );
        }
        this.age = age;
    }

    public URL getPhotoUrl() {
        return photoUrl;
    }

    public String getTicketVerificationFileName() {
        return ticketVerificationFileName;
    }

    public void setTicketVerificationFileName(String ticketVerificationFileName) {
        this.ticketVerificationFileName = ticketVerificationFileName;
    }

    public void setPhotoUrl(URL photoUrl) {
        this.photoUrl = photoUrl;
    }

    public CustomerStatus getStatus() {
        return status;
    }

    public void setStatus(CustomerStatus status) {
        if (status == null) {
            throw new NullPointerException("Customer status can't be null");
        }
        this.status = status;
    }


    public void setUser(User user) {
        this.user = user;
    }

    //    public List<Ticket> getTickets() {
//        return tickets;
//    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(userId, customer.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id="+userId +
                "user="+user.toString()+
                "firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }

    private boolean isValidAge(int age) {
        return age > 0 && age < 200;
    }
}

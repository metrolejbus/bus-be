package com.metrolejbus.app.customers.forms;

import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Component
public class UpdateCustomerForm {
    @Size(min = 1)
    public String firstName;

    @Size(min = 1)
    public String lastName;

    @Positive
    public Integer age;
}

package com.metrolejbus.app.customers.services;

import com.metrolejbus.app.authentication.models.User;
import com.metrolejbus.app.common.exceptions.BadRequest;
import com.metrolejbus.app.customers.models.Customer;
import com.metrolejbus.app.customers.models.CustomerStatus;
import com.metrolejbus.app.customers.repositories.CustomersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;


@Service
public class CustomersService {
    @Autowired
    CustomersRepository customersRepository;

    public Customer findById(long id) {
        return customersRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    public Customer save(Customer customer) {
        return customersRepository.save(customer);
    }

    public Customer updateCustomer(
            Long id,
            String firstName,
            String lastName,
            Integer age
    ) throws BadRequest {
        Customer customer = customersRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
        try {
            customer.setAge(age);
            customer.setFirstName(firstName);
            customer.setLastName(lastName);
        } catch (Exception e) {
            throw new BadRequest(e.getMessage());
        }

        return customersRepository.saveAndFlush(customer);
    }

    public Customer updateCustomer(Customer c) {
        return customersRepository.saveAndFlush(c);
    }

    public Customer createCustomer(
            String firstName,
            String lastName,
            String middleName,
            Integer age,
            User user
    ) throws BadRequest {
        try {
            return this.customersRepository.saveAndFlush(new Customer(
                    user,
                    firstName,
                    middleName,
                    lastName,
                    age,
                    CustomerStatus.ADULT
            ));
        } catch (Exception e) {
            throw new BadRequest(e.getMessage());
        }
    }

    public Customer findByUser(User user) {
        return customersRepository.findByUser(user);
    }
}

package com.metrolejbus.app.ticketing.forms;

import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

@Component
public class UpdateTicketPriceForm {

    @NotBlank(message = "Customer status must be provided.")
    public String customerStatus;

    @NotBlank(message = "Ticket duration must be provided.")
    public String ticketDuration;

    @Positive(message = "Zone ID must be positive.")
    public Long zoneId;

    @Positive(message = "Zone ID must be positive.")
    public Float value;

    public String startDate;

    public String endDate;
}

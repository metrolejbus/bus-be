package com.metrolejbus.app.ticketing.forms;

import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Component
public class CreatePeriodicTicketForm {

    @NotNull(message = "Zone ID must be provided.")
    @Positive(message = "Zone ID must be positive.")
    public Long zoneId;

    @NotBlank(message = "Valid from must be provided.")
    public String validFrom;

    @NotBlank(message = "Duration must be provided.")
    public String duration;
}

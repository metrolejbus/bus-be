package com.metrolejbus.app.ticketing.forms;

import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Component
public class CreateSingleUseTicketForm {

    @NotNull(message = "Zone ID must be provided.")
    @Positive(message = "Zone ID must be positive.")
    public Long zoneId;
}

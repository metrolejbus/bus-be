package com.metrolejbus.app.ticketing.forms;

import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Component
public class CreateTicketPriceForm {

    @NotBlank(message = "Customer status must be provided.")
    public String customerStatus;

    @NotBlank(message = "Ticket duration must be provided.")
    public String ticketDuration;

    @NotNull(message = "Zone ID must be provided.")
    @Positive(message = "Zone ID must be positive.")
    public Long zoneId;

    @NotNull(message = "Value must be provided.")
    @Positive(message = "Zone ID must be positive.")
    public Float value;

    @NotBlank(message = "Start date must be provided.")
    public String startDate;

    @NotBlank(message = "End date must be provided.")
    public String endDate;
}

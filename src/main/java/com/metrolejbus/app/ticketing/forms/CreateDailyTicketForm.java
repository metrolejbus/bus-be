package com.metrolejbus.app.ticketing.forms;

import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Component
public class CreateDailyTicketForm {

    @NotNull(message = "Zone ID must be provided.")
    @Positive(message = "Zone ID must be positive.")
    public Long zoneId;

    @NotBlank(message = "Date must be provided.")
    public String date;
}

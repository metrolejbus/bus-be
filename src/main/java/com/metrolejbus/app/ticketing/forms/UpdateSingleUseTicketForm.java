package com.metrolejbus.app.ticketing.forms;

import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Component
public class UpdateSingleUseTicketForm {

    @NotNull(message = "Ticket ID must be provided.")
    public Long id;

    @Positive(message = "Zone ID must be positive.")
    public Long zoneId;
}

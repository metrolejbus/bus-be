package com.metrolejbus.app.ticketing.forms;

import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Component
public class UpdatePeriodicTicketForm {

    @NotNull(message = "Ticket ID must be provided.")
    public long id;

    @Positive(message = "Zone ID must be positive.")
    public Long zoneId;

    public String validFrom;

    public String duration;
}

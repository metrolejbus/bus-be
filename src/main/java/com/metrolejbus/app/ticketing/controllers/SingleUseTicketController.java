package com.metrolejbus.app.ticketing.controllers;

import com.metrolejbus.app.ticketing.forms.CreateSingleUseTicketForm;
import com.metrolejbus.app.ticketing.forms.UpdateSingleUseTicketForm;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import com.metrolejbus.app.ticketing.models.SingleUseTicket;
import com.metrolejbus.app.ticketing.services.SingleUseTicketService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/single-use-tickets")
public class SingleUseTicketController {

    private SingleUseTicketService singleUseTicketService;

    public SingleUseTicketController(@Autowired SingleUseTicketService singleUseTicketService) {
        this.singleUseTicketService = singleUseTicketService;
    }

    @GetMapping
    @PreAuthorize("permitAll()")
    public ResponseEntity<List<SingleUseTicket>> findAll() {
        return ResponseEntity.ok(singleUseTicketService.findAll());
    }

    @GetMapping("/{id}")
    @PreAuthorize("permitAll()")
    public ResponseEntity<SingleUseTicket> findById(@PathVariable long id) {
        SingleUseTicket ticket;
        try {
            ticket = singleUseTicketService.findById(id);
        }catch (ResourceNotFoundException e){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(ticket);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('CUSTOMER')")
    public ResponseEntity<SingleUseTicket> create(@Valid @RequestBody CreateSingleUseTicketForm data){
        SingleUseTicket ticket;
        try {
            ticket = singleUseTicketService.create(data);
        } catch (ResourceNotFoundException | ZoneNotFound e) {
            return ResponseEntity.notFound().build();
        }
        return new ResponseEntity<>(ticket, HttpStatus.CREATED);
    }

    @PutMapping
    @PreAuthorize("hasAuthority('CUSTOMER')")
    public ResponseEntity<SingleUseTicket> update(@Valid @RequestBody UpdateSingleUseTicketForm data) {
        SingleUseTicket ticket;
        try {
            ticket = singleUseTicketService.update(data);
        } catch (ResourceNotFoundException | ZoneNotFound e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(ticket);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('CUSTOMER')")
    public ResponseEntity<SingleUseTicket> deleteById(@PathVariable long id) {
        SingleUseTicket ticket;
        try {
            ticket = singleUseTicketService.deleteById(id);
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(ticket);
    }

}

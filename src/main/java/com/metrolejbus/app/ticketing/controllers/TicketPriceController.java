package com.metrolejbus.app.ticketing.controllers;

import com.metrolejbus.app.common.exceptions.NotFound;
import com.metrolejbus.app.ticketing.forms.CreateTicketPriceForm;
import com.metrolejbus.app.ticketing.forms.UpdateTicketPriceForm;
import com.metrolejbus.app.ticketing.models.TicketPrice;
import com.metrolejbus.app.ticketing.services.TicketPriceService;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.management.InvalidAttributeValueException;
import javax.validation.Valid;
import java.time.format.DateTimeParseException;
import java.util.List;

@RestController
@RequestMapping("/ticket-prices")
public class TicketPriceController {

    private TicketPriceService ticketPriceService;

    public TicketPriceController(@Autowired TicketPriceService ticketPriceService) {
        this.ticketPriceService = ticketPriceService;
    }

    @GetMapping
    @PreAuthorize("permitAll()")
    public ResponseEntity<List<TicketPrice>> findAll() {
        return ResponseEntity.ok(ticketPriceService.findAll());
    }

    @GetMapping("/{zoneId}/{duration}/{customer}")
    @PreAuthorize("permitAll()")
    public ResponseEntity<TicketPrice> findById(
            @PathVariable long zoneId,
            @PathVariable String duration,
            @PathVariable String customer) {
        TicketPrice ticketPrice;
        try {
            ticketPrice = ticketPriceService.findById(zoneId, duration, customer);
        } catch (ResourceNotFoundException | ZoneNotFound e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(ticketPrice);
    }

    @GetMapping("/{duration}")
    @PreAuthorize("permitAll()")
    public ResponseEntity<List<TicketPrice>> findAllByDuration(@PathVariable String duration) {
        List<TicketPrice> ticketPrices;
        try {
            ticketPrices = ticketPriceService.findAllByDuration(duration);
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(ticketPrices);
    }

    @GetMapping("/{zoneId}/{duration}")
    @PreAuthorize("permitAll()")
    public ResponseEntity<List<TicketPrice>> findAllByZoneAndDuration(
            @PathVariable long zoneId,
            @PathVariable String duration) {
        List<TicketPrice> ticketPrices;
        try {
            ticketPrices = ticketPriceService.findAllByZoneAndDuration(zoneId, duration);
        } catch (ResourceNotFoundException | ZoneNotFound e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(ticketPrices);
    }


    @PostMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<TicketPrice> create(@Valid @RequestBody CreateTicketPriceForm data) throws NotFound {
        TicketPrice ticketPrice;
        try {
            ticketPrice = ticketPriceService.create(data);
        } catch (DateTimeParseException | InvalidAttributeValueException | IllegalArgumentException e) {
            return ResponseEntity.badRequest().build();
        } catch (ResourceNotFoundException | ZoneNotFound e) {
            return ResponseEntity.notFound().build();
        }
        return new ResponseEntity<>(ticketPrice, HttpStatus.CREATED);
    }

    @PutMapping
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<TicketPrice> update(@Valid @RequestBody UpdateTicketPriceForm data) {
        TicketPrice ticketPrice;
        try {
            ticketPrice = ticketPriceService.update(data);
        } catch (ResourceNotFoundException | ZoneNotFound e) {
            return ResponseEntity.notFound().build();
        } catch (InvalidAttributeValueException e) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(ticketPrice);
    }

    @DeleteMapping("/{zoneId}/{duration}/{customer}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<TicketPrice> deleteById(
            @PathVariable long zoneId,
            @PathVariable String duration,
            @PathVariable String customer) {
        TicketPrice ticketPrice;
        try {
            ticketPrice = ticketPriceService.deleteById(zoneId, duration, customer);
        } catch (ResourceNotFoundException | ZoneNotFound e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(ticketPrice);
    }

}

package com.metrolejbus.app.ticketing.controllers;

import com.metrolejbus.app.ticketing.forms.CreateDailyTicketForm;
import com.metrolejbus.app.ticketing.forms.UpdateDailyTicketForm;
import com.metrolejbus.app.ticketing.models.DailyTicket;
import com.metrolejbus.app.ticketing.services.DailyTicketService;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.management.InvalidAttributeValueException;
import javax.validation.Valid;
import java.time.format.DateTimeParseException;
import java.util.List;

@RestController
@RequestMapping("/daily-tickets")
public class DailyTicketController {

    private DailyTicketService dailyTicketService;

    public DailyTicketController(@Autowired DailyTicketService dailyTicketService) {
        this.dailyTicketService = dailyTicketService;
    }

    @GetMapping
    @PreAuthorize("permitAll()")
    public ResponseEntity<List<DailyTicket>> findAll() {
        return ResponseEntity.ok(dailyTicketService.findAll());
    }

    @GetMapping("/{id}")
    @PreAuthorize("permitAll()")
    public ResponseEntity<DailyTicket> findById(@PathVariable long id) {
        DailyTicket ticket;
        try {
            ticket = dailyTicketService.findById(id);
        }catch (ResourceNotFoundException e){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(ticket);
    }

    @PostMapping
    @PreAuthorize("hasAuthority('CUSTOMER')")
    public ResponseEntity<DailyTicket> create(@RequestBody CreateDailyTicketForm data) {
        DailyTicket ticket;
        try {
            ticket = dailyTicketService.create(data);
        } catch (DateTimeParseException | InvalidAttributeValueException e) {
            return ResponseEntity.badRequest().build();
        } catch (ResourceNotFoundException|ZoneNotFound e) {
            return ResponseEntity.notFound().build();
        }
        return new ResponseEntity<>(ticket, HttpStatus.CREATED);
    }

    @PutMapping
    @PreAuthorize("hasAuthority('CUSTOMER')")
    public ResponseEntity<DailyTicket> update(@Valid @RequestBody UpdateDailyTicketForm data) {
        DailyTicket ticket;
        try {
            ticket = dailyTicketService.update(data);
        } catch (ResourceNotFoundException|ZoneNotFound e) {
            return ResponseEntity.notFound().build();
        } catch (InvalidAttributeValueException e) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(ticket);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('CUSTOMER')")
    public ResponseEntity<DailyTicket> deleteById(@PathVariable long id) {
        DailyTicket ticket;
        try {
            ticket = dailyTicketService.deleteById(id);
        }catch (ResourceNotFoundException e){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(ticket);
    }

}

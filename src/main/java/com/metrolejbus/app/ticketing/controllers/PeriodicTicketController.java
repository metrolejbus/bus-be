package com.metrolejbus.app.ticketing.controllers;

import com.metrolejbus.app.authentication.models.User;
import com.metrolejbus.app.authentication.services.AuthService;
import com.metrolejbus.app.customers.models.Customer;
import com.metrolejbus.app.customers.services.CustomersService;
import com.metrolejbus.app.ticketing.forms.CreatePeriodicTicketForm;
import com.metrolejbus.app.ticketing.forms.UpdatePeriodicTicketForm;
import com.metrolejbus.app.ticketing.models.PeriodicTicket;
import com.metrolejbus.app.ticketing.models.PeriodicTicketState;
import com.metrolejbus.app.ticketing.models.TicketDuration;
import com.metrolejbus.app.ticketing.services.PeriodicTicketService;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;

import javax.management.InvalidAttributeValueException;
import javax.validation.Valid;
import java.security.Principal;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.List;

@RestController
@RequestMapping("/periodic-tickets")
public class PeriodicTicketController {

    private PeriodicTicketService periodicTicketService;

    private CustomersService customersService;

    private AuthService authService;

    public PeriodicTicketController(@Autowired PeriodicTicketService periodicTicketService,
                                    @Autowired CustomersService customersService,
                                    @Autowired AuthService authService) {
        this.periodicTicketService = periodicTicketService;
        this.customersService = customersService;
        this.authService = authService;
    }

    @GetMapping
    @PreAuthorize("permitAll()")
    public ResponseEntity<List<PeriodicTicket>> findAll() {
        return ResponseEntity.ok(periodicTicketService.findAll());
    }

    @GetMapping("/unprocessed")
    public List<PeriodicTicket> getUnprocessed() {
        return this.periodicTicketService.findAllByState(PeriodicTicketState.CREATED);
    }

    @GetMapping("/{id}")
    @PreAuthorize("permitAll()")
    public ResponseEntity<PeriodicTicket> findById(@PathVariable long id) {
        PeriodicTicket ticket;
        try {
            ticket = periodicTicketService.findById(id);
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(ticket);
    }

    @PostMapping("/{id}/approve")
    public ResponseEntity<PeriodicTicket> approveTicket(@PathVariable int id) {
        PeriodicTicket ticket = periodicTicketService.findById(id);
        ticket.setState(PeriodicTicketState.APPROVED);
        periodicTicketService.save(ticket);

        Customer customer = ticket.getCustomer();
        ChronoUnit interval = ticket.getDuration() == TicketDuration.MONTH ? ChronoUnit.MONTHS : ChronoUnit.YEARS;
        LocalDate approvedUntil = LocalDate.now().plus(1, interval);

        customer.setApprovedUntil(approvedUntil);
        customersService.save(customer);

        return ResponseEntity.ok(ticket);
    }

    @PostMapping("/{id}/reject")
    public ResponseEntity<PeriodicTicket> rejectTicket(@PathVariable int id) {
        PeriodicTicket ticket = periodicTicketService.findById(id);
        ticket.setState(PeriodicTicketState.REJECTED);
        periodicTicketService.save(ticket);

        return ResponseEntity.ok(ticket);
    }

    @PostMapping
//    public ResponseEntity<Object> create(@Valid @RequestBody CreatePeriodicTicketForm data, Principal principal) {
    @PreAuthorize("hasAuthority('CUSTOMER')")
    public ResponseEntity<Object> create(@Valid @RequestBody CreatePeriodicTicketForm data, Principal principal) {
        PeriodicTicket ticket;
        try {
            ticket = periodicTicketService.create(data);
            User user =  authService.findByUsername(principal.getName());
            Customer customer = customersService.findByUser(user);

            ticket.setCustomer(customer);
            periodicTicketService.save(ticket);
        } catch (DateTimeParseException | IllegalArgumentException | InvalidAttributeValueException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (ResourceNotFoundException | ZoneNotFound e) {
            return ResponseEntity.notFound().build();
        }
        return new ResponseEntity<>(ticket, HttpStatus.CREATED);
    }

    //FIXME(simona): shouldn't put mapping require an ID?
    @PutMapping
    @PreAuthorize("hasAuthority('CUSTOMER')")
    public ResponseEntity<PeriodicTicket> update(@Valid @RequestBody UpdatePeriodicTicketForm data) {
        PeriodicTicket ticket;
        try {
            ticket = periodicTicketService.update(data);
        } catch (ResourceNotFoundException | ZoneNotFound e) {
            return ResponseEntity.notFound().build();
        } catch (InvalidAttributeValueException e) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(ticket);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('CUSTOMER')")
    public ResponseEntity<PeriodicTicket> deleteById(@PathVariable long id) {
        PeriodicTicket ticket;
        try {
            ticket = periodicTicketService.deleteById(id);
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(ticket);
    }

}

package com.metrolejbus.app.ticketing.models;

public enum PeriodicTicketState {
    CREATED, APPROVED, REJECTED, EXPIRED
}

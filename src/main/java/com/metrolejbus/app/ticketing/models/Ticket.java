package com.metrolejbus.app.ticketing.models;

import com.metrolejbus.app.customers.models.Customer;
import com.metrolejbus.app.transportation.models.Zone;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@MappedSuperclass
public abstract class Ticket {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ticket_id", updatable = false, nullable = false)
    private Long id;

    @ManyToOne(optional = true, targetEntity = Customer.class)
    @JoinColumn(name = "customer_id", referencedColumnName = "user_id")
    private Customer customer;

    @ManyToOne(optional = false)
    @JoinColumn(name = "zone_id", referencedColumnName = "zone_id")
    private Zone zone;

    @Column(name = "issued_on")
    private LocalDate issuedOn;

    private boolean active;

    protected Ticket(Zone zone) {
        this.zone = zone;
        this.issuedOn = LocalDate.now();
    }

    public abstract TicketDuration getDuration();

    public Long getId() {
        return id;
    }

    public void setId(long id) { this.id = id; }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        return customer;
    }

    public Zone getZone() {
        return zone;
    }

    public void setZone(Zone zone) {this.zone = zone;}

    public LocalDate getIssuedOn() {
        return issuedOn;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ticket ticket = (Ticket) o;
        return Objects.equals(id, ticket.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", customer=" + customer +
                ", zone=" + zone +
                ", issuedOn=" + issuedOn +
                ", active=" + active +
                '}';
    }
}

package com.metrolejbus.app.ticketing.models;

import com.metrolejbus.app.transportation.models.Zone;
import org.hibernate.annotations.Where;

import javax.management.InvalidAttributeValueException;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "daily_tickets")
@Where(clause="active=1")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class DailyTicket extends Ticket {

    private LocalDate date;

    public DailyTicket() {
        super(null);
    }

    public DailyTicket(Zone zone, LocalDate date) throws InvalidAttributeValueException {
        super(zone);
        if (zone == null) {
            throw new NullPointerException("Zone can't be null");
        }

        if (date == null) {
            throw new NullPointerException("Date can't be null");
        }

        if (date.isBefore(LocalDate.now())) {
            throw new InvalidAttributeValueException(
                    "Daily ticket can't be bought for a date in the past"
            );
        }

        this.date = date;
    }

    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        if (date == null) {
            throw new NullPointerException("Date can't be null");
        }
        this.date = date;
    }

    @Override
    public TicketDuration getDuration() {
        return TicketDuration.DAY;
    }

    @Override
    public String toString() {
        return super.toString() + ", date=" + date + '}';
    }
}

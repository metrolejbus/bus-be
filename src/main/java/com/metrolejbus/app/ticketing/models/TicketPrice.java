package com.metrolejbus.app.ticketing.models;

import com.metrolejbus.app.common.models.ValidityPeriod;
import com.metrolejbus.app.customers.models.CustomerStatus;
import com.metrolejbus.app.transportation.models.Zone;
import org.hibernate.annotations.Where;

import javax.management.InvalidAttributeValueException;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "ticket_prices")
@Where(clause="active=1")
public class TicketPrice {

    @EmbeddedId
    private TicketPriceKey id;

    private float value;

    private ValidityPeriod validityPeriod;

    private boolean active;

    public TicketPrice() {

    }

    public  TicketPrice(
        CustomerStatus customerStatus,
        TicketDuration ticketDuration,
        Zone zone,
        float value,
        ValidityPeriod validityPeriod
    ) throws InvalidAttributeValueException {
        if (customerStatus == null) {
            throw new NullPointerException("Customer status can't be null");
        }

        if (ticketDuration == null) {
            throw new NullPointerException("Ticket duration can't be null");
        }

        if (zone == null) {
            throw new NullPointerException("Zone can't be null");
        }

        if (validityPeriod == null) {
            throw new NullPointerException("Validity period can't be null");
        }

        if (value < 0) {
            throw new InvalidAttributeValueException(
                    "Ticket price can't be a negative value"
            );
        }
        this.id = new TicketPriceKey();
        this.id.ticketDuration = ticketDuration;
        this.id.customerStatus = customerStatus;
        this.id.zone = zone;

        this.value = value;
        this.validityPeriod = validityPeriod;
    }

    public  TicketPrice(
        CustomerStatus customerStatus,
        TicketDuration ticketDuration,
        Zone zone
    ) {
        if (customerStatus == null) {
            throw new NullPointerException("Customer status can't be null");
        }

        if (ticketDuration == null) {
            throw new NullPointerException("Ticket duration can't be null");
        }

        if (zone == null) {
            throw new NullPointerException("Zone can't be null");
        }

        this.id = new TicketPriceKey();
        this.id.ticketDuration = ticketDuration;
        this.id.customerStatus = customerStatus;
        this.id.zone = zone;
    }


    public TicketPriceKey getId() {
        return id;
    }

    public CustomerStatus getCustomerStatus() {
        return this.id.customerStatus;
    }

    public void setCustomerStatus(CustomerStatus status) {
        if (status == null) {
            throw new NullPointerException("Customer status can't be null");
        }
        this.id.customerStatus = status; }

    public TicketDuration getTicketDuration() {
        return this.id.ticketDuration;
    }

    public Zone getZone() {
        return this.id.zone;
    }

    public void setZone(Zone zone) {
        if (zone == null) {
            throw new NullPointerException("Zone can't be null");
        }
        this.id.zone = zone;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) throws InvalidAttributeValueException {
        if (value < 0) {
            throw new InvalidAttributeValueException(
                    "Ticket price can't be a negative value"
            );
        }
        this.value = value;
    }

    public ValidityPeriod getValidityPeriod() {
        return validityPeriod;
    }

    public void setValidityPeriod(ValidityPeriod validityPeriod) {
        if (validityPeriod == null) {
            throw new NullPointerException("Validity period can't be null");
        }
        this.validityPeriod = validityPeriod;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setId(TicketPriceKey id) {
        this.id = id;
    }

    public void setDuration(TicketDuration duration) { this.id.ticketDuration = duration; }

    @Embeddable
    public static class TicketPriceKey implements Serializable {
        @Column(name = "customer_status", nullable = false)
        public CustomerStatus customerStatus;
        @Column(name = "ticket_duration", nullable = false)
        public TicketDuration ticketDuration;

        @ManyToOne(targetEntity = Zone.class, optional = false)
        @JoinColumn(name = "zone_id", referencedColumnName = "zone_id")
        public Zone zone;

        public TicketPriceKey(){}

        public TicketPriceKey(CustomerStatus customerStatus, TicketDuration ticketDuration, Zone zone){
            this.customerStatus = customerStatus;
            this.ticketDuration = ticketDuration;
            this.zone = zone;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TicketPriceKey that = (TicketPriceKey) o;
            return customerStatus == that.customerStatus &&
                    ticketDuration == that.ticketDuration &&
                    Objects.equals(zone, that.zone);
        }

        @Override
        public int hashCode() {
            return Objects.hash(customerStatus, ticketDuration, zone);
        }

        @Override
        public String toString() {
            return "TicketPriceKey{" +
                    "customerStatus=" + customerStatus +
                    ", ticketDuration=" + ticketDuration +
                    ", zone=" + zone +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "TicketPrice{" +
                "id=" + id +
                ", value=" + value +
                ", validityPeriod=" + validityPeriod +
                ", active=" + active +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TicketPrice ticketPrice = (TicketPrice) o;
        return Objects.equals(id, ticketPrice.id);
    }
}

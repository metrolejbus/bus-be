package com.metrolejbus.app.ticketing.models;

public enum TicketDuration {
    SINGLE_USE, DAY, MONTH, YEAR
}

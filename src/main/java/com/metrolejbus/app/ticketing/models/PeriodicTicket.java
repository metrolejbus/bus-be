package com.metrolejbus.app.ticketing.models;

import com.metrolejbus.app.common.models.ValidityPeriod;
import com.metrolejbus.app.transportation.models.Zone;
import org.hibernate.annotations.Where;

import javax.management.InvalidAttributeValueException;
import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "periodic_tickets")
@Where(clause="active=1")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class PeriodicTicket extends Ticket {

    @Enumerated(EnumType.STRING)
    private TicketDuration duration;
    @Enumerated(EnumType.STRING)
    private PeriodicTicketState state;

    @Embedded
    private ValidityPeriod validityPeriod;

    @Column(name = "rejection_reason")
    private String rejectionReason;

    public PeriodicTicket() {
        super(null);
    }

    public PeriodicTicket(
            Zone zone,
            TicketDuration duration,
            LocalDate validFrom
    ) throws InvalidAttributeValueException {
        super(zone);
        if (zone == null) {
            throw new NullPointerException("Zone can't be null");
        }

        if (duration == null) {
            throw new NullPointerException("Ticket duration can't be null");
        }

        if (validFrom == null) {
            throw new NullPointerException("Valid from date can't be null");
        }

        if (duration != TicketDuration.MONTH && duration != TicketDuration.YEAR) {
            throw new InvalidAttributeValueException(
                    "Periodic ticket can be bought for either a month or a year"
            );
        }

        this.duration = duration;
        this.state = PeriodicTicketState.CREATED;
        this.rejectionReason = null;
        this.setValidFrom(validFrom);
    }

    @Override
    public TicketDuration getDuration() {
        return duration;
    }

    public void setDuration(TicketDuration duration) throws InvalidAttributeValueException {
        if (duration != TicketDuration.MONTH && duration != TicketDuration.YEAR) {
            throw new InvalidAttributeValueException(
                    "Periodic ticket can be bought for either a month or a year"
            );
        }
        this.duration = duration;
    }

    public PeriodicTicketState getState() {
        return state;
    }

    public void setState(PeriodicTicketState state) {
        this.state = state;
    }

    public LocalDate getValidFrom() {
        return validityPeriod.getStartDate();
    }

    public void setValidFrom(LocalDate validFrom)
            throws IllegalStateException, InvalidAttributeValueException {
        if (validFrom == null) {
            throw new NullPointerException("Valid from date can't be null");
        }

        if (this.state == PeriodicTicketState.EXPIRED ||
                this.state == PeriodicTicketState.REJECTED) {
            throw new IllegalStateException(
                    "Ticket has either been rejected or has expired"
            );
        }

        LocalDate validTo;
        if (duration == TicketDuration.MONTH) {
            validTo = validFrom.plusMonths(1);
        } else {
            validTo = validFrom.plusYears(1);
        }

        this.validityPeriod = new ValidityPeriod(validFrom, validTo);
    }

    public LocalDate getValidTo() {
        return this.validityPeriod.getEndDate();
    }

    public String getRejectionReason() {
        return rejectionReason;
    }

    public void setRejectionReason(String rejectionReason) {
        this.rejectionReason = rejectionReason;
    }
}

package com.metrolejbus.app.ticketing.models;

import com.metrolejbus.app.transportation.models.Journey;
import com.metrolejbus.app.transportation.models.Zone;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "single_use_tickets")
@Where(clause="active=1")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class SingleUseTicket extends Ticket {

    @ManyToOne
    @JoinColumn(name = "journey_id", referencedColumnName = "journey_id")
    private Journey journey;

    public SingleUseTicket() {
        super(null);
    }

    public SingleUseTicket(Zone zone) {
        super(zone);
        if (zone == null) {
            throw new NullPointerException("Zone can't be null");
        }

        this.journey = null;
    }

    @Override
    public TicketDuration getDuration() {
        return TicketDuration.SINGLE_USE;
    }

    public Journey getJourney() {
        return journey;
    }

    public void setJourney(Journey journey) {
        this.journey = journey;
    }

}

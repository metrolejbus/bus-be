package com.metrolejbus.app.ticketing.services;

import com.metrolejbus.app.authentication.models.User;
import com.metrolejbus.app.authentication.services.AuthService;
import com.metrolejbus.app.customers.models.Customer;
import com.metrolejbus.app.customers.services.CustomersService;
import com.metrolejbus.app.ticketing.forms.CreateDailyTicketForm;
import com.metrolejbus.app.ticketing.forms.UpdateDailyTicketForm;
import com.metrolejbus.app.ticketing.models.DailyTicket;
import com.metrolejbus.app.ticketing.repositories.DailyTicketRepository;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.services.ZoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.management.InvalidAttributeValueException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

@Service
public class DailyTicketService {

    private DailyTicketRepository dailyTicketRepository;
    private ZoneService zoneService;
    private AuthService authService;
    private CustomersService customersService;

    public DailyTicketService(
            @Autowired DailyTicketRepository dailyTicketRepository,
            @Autowired ZoneService zoneService,
            @Autowired AuthService authService,
            @Autowired CustomersService customersService
            ) {
        this.dailyTicketRepository = dailyTicketRepository;
        this.zoneService = zoneService;
        this.authService = authService;
        this.customersService = customersService;
    }

    public List<DailyTicket> findAll() { return dailyTicketRepository.findAll(); }

    public DailyTicket findById(long id){
        return dailyTicketRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    public List<DailyTicket> findAllByCustomerId(long id) {
        List<DailyTicket> tickets = dailyTicketRepository.findAllByCustomerUserIdEquals(id);
        LocalDate today = LocalDate.now();
        for (DailyTicket t : tickets) {
            if (t.getDate().isBefore(today)) {
                tickets.remove(t);
            }
        }
        return tickets;
    }

    public DailyTicket create(CreateDailyTicketForm data)
        throws InvalidAttributeValueException, ZoneNotFound
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
        LocalDate date;
        try {
            date = LocalDate.parse(data.date, formatter);
            Zone zone;
            try {
                zone = zoneService.get(data.zoneId);
                DailyTicket ticket = new DailyTicket(zone, date);
                ticket.setActive(true);

                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                String currentUserName = authentication.getName();
                User user = authService.findByUsername(currentUserName);
                Customer customer = customersService.findById(user.getId());
                ticket.setCustomer(customer);

                return dailyTicketRepository.save(ticket);
            } catch (ResourceNotFoundException e) {
                throw new ResourceNotFoundException();
            }
        } catch (DateTimeParseException e) {
            throw new DateTimeParseException("Date cannot be parsed ", data.date, 0);
        }
    }

    public DailyTicket update(UpdateDailyTicketForm data)
        throws InvalidAttributeValueException, ZoneNotFound
    {
        DailyTicket ticket = findById(data.id);
        if (data.zoneId != null){
            Zone zone = zoneService.get(data.zoneId);
            ticket.setZone(zone);
        }

        if(data.date != null && !data.date.equals("")){
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
            LocalDate date;
            try {
                date = LocalDate.parse(data.date, formatter);
                if (date.isBefore(LocalDate.now())) {
                    throw new InvalidAttributeValueException(
                            "Daily ticket can't be bought for a date in the past"
                    );
                }
            } catch (DateTimeParseException e) {
                throw new DateTimeParseException("Date cannot be parsed ", data.date, 0);
            }
            ticket.setDate(date);
        }

        return dailyTicketRepository.save(ticket);
    }

    public DailyTicket deleteById(long id) {
        DailyTicket ticket = findById(id);
        ticket.setActive(false);
        return dailyTicketRepository.save(ticket);
    }

}

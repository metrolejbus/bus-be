package com.metrolejbus.app.ticketing.services;

import com.metrolejbus.app.common.exceptions.NotFound;
import com.metrolejbus.app.common.models.ValidityPeriod;
import com.metrolejbus.app.customers.models.CustomerStatus;
import com.metrolejbus.app.ticketing.forms.CreateTicketPriceForm;
import com.metrolejbus.app.ticketing.forms.UpdateTicketPriceForm;
import com.metrolejbus.app.ticketing.models.TicketDuration;
import com.metrolejbus.app.ticketing.models.TicketPrice;
import com.metrolejbus.app.ticketing.repositories.TicketPriceRepository;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.services.ZoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import javax.management.InvalidAttributeValueException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

@Service
public class TicketPriceService {

    private TicketPriceRepository ticketPriceRepository;
    private ZoneService zoneService;

    public TicketPriceService(
            @Autowired TicketPriceRepository ticketPriceRepository,
            @Autowired ZoneService zoneService
    ) {
         this.ticketPriceRepository = ticketPriceRepository;
         this.zoneService = zoneService;
    }

    public List<TicketPrice> findAll(){
        return ticketPriceRepository.findAll();
    }

    public TicketPrice findById(Long zoneId, String duration, String customer)
        throws ZoneNotFound
    {
        try {
            CustomerStatus customerStatus = CustomerStatus.valueOf(customer.toUpperCase());
            try {
                TicketDuration ticketDuration = TicketDuration.valueOf(duration.toUpperCase());
                try {
                    Zone zone = zoneService.get(zoneId);
                    TicketPrice.TicketPriceKey id = new TicketPrice.TicketPriceKey(
                            customerStatus,
                            ticketDuration,
                            zone
                    );
                    return ticketPriceRepository.findTicketPriceByIdEquals(id);
//                    if (ticketPrice == null) {
//                        throw new TicketPriceNotFound(zoneId, duration, customer);
//                    }
                } catch (ResourceNotFoundException e) {
                    throw new ResourceNotFoundException();
                }
            } catch (IllegalArgumentException e) {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException();
        }
    }

    public List<TicketPrice> findAllByDuration(String duration) {
        try {
            TicketDuration ticketDuration = TicketDuration.valueOf(duration.toUpperCase());
            List<TicketPrice> ticketPrices = ticketPriceRepository.findAllById_TicketDuration(ticketDuration);
            List<TicketPrice> currentTicketPrices = new ArrayList<>();
            LocalDate today = LocalDate.now();
            LocalDate startDate;
            LocalDate endDate;
            for (TicketPrice tp : ticketPrices) {
                startDate = tp.getValidityPeriod().getStartDate();
                endDate = tp.getValidityPeriod().getEndDate();
                if (today.isEqual(startDate) || today.isEqual(endDate) || (today.isAfter(startDate) && today.isBefore(endDate))) {
                    currentTicketPrices.add(tp);
                }
            }
            return currentTicketPrices;
        } catch (ResourceNotFoundException e) {
            throw new ResourceNotFoundException();
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException();
        }
    }

    public List<TicketPrice> findAllByZoneAndDuration(Long zoneId, String duration)
            throws ZoneNotFound
    {
        try {
            TicketDuration ticketDuration = TicketDuration.valueOf(duration.toUpperCase());
            Zone zone = zoneService.get(zoneId);
            List<TicketPrice> ticketPrices = ticketPriceRepository.findAllById_ZoneAndId_TicketDuration(zone, ticketDuration);
            List<TicketPrice> currentTicketPrices = new ArrayList<>();
            LocalDate today = LocalDate.now();
            LocalDate startDate;
            LocalDate endDate;
            for (TicketPrice tp : ticketPrices) {
                startDate = tp.getValidityPeriod().getStartDate();
                endDate = tp.getValidityPeriod().getEndDate();
                if (today.isEqual(startDate) || today.isEqual(endDate) || (today.isAfter(startDate) && today.isBefore(endDate))) {
                    currentTicketPrices.add(tp);
                }
            }
            return currentTicketPrices;
        } catch (ResourceNotFoundException e) {
            throw new ResourceNotFoundException();
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException();
        }
    }

    private boolean checkIfExists(CreateTicketPriceForm data) throws ZoneNotFound {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
        TicketPrice ticketP = this.findById(data.zoneId, data.ticketDuration, data.customerStatus);
        if (ticketP != null) {
            LocalDate date1 = LocalDate.parse(data.startDate, formatter);
            LocalDate startD = ticketP.getValidityPeriod().getStartDate();
            LocalDate endD = ticketP.getValidityPeriod().getEndDate();
            if (date1.isEqual(startD) || date1.isEqual(endD) || (date1.isAfter(startD) && (date1.isBefore(endD)))
            ) {
                return true;
            }
            LocalDate date2 = LocalDate.parse(data.endDate, formatter);
            return (date2.isEqual(startD) || date2.isEqual(endD) || (date2.isAfter(startD) && date2.isBefore(endD)));
        }
        return false;
    }

    public TicketPrice create(CreateTicketPriceForm data)
        throws InvalidAttributeValueException, ZoneNotFound, NotFound
    {
        if (this.checkIfExists(data)) {
            throw new InvalidAttributeValueException();
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
        Zone zone = zoneService.get(data.zoneId);
        try {
            LocalDate startDate = LocalDate.parse(data.startDate, formatter);
            try {
                LocalDate endDate = LocalDate.parse(data.endDate, formatter);
                ValidityPeriod validityPeriod = new ValidityPeriod(startDate, endDate);

                try {
                    CustomerStatus customerStatus = CustomerStatus.valueOf(data.customerStatus.toUpperCase());
                    TicketDuration ticketDuration = TicketDuration.valueOf(data.ticketDuration.toUpperCase());
                    TicketPrice ticketPrice = new TicketPrice(
                            customerStatus,
                            ticketDuration,
                            zone,
                            data.value,
                            validityPeriod
                    );
                    ticketPrice.setActive(true);
                    return ticketPriceRepository.save(ticketPrice);
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException();
                }
            } catch (DateTimeParseException e) {
                throw new DateTimeParseException("Date cannot be parsed ", data.endDate, 0);
            }
        } catch (DateTimeParseException e) {
            throw new DateTimeParseException("Date cannot be parsed ", data.startDate, 0);
        }
    }

    public TicketPrice update(UpdateTicketPriceForm data)
        throws InvalidAttributeValueException, ZoneNotFound
    {
        if (data.zoneId == null) {
            throw new InvalidAttributeValueException();
        }

        TicketPrice ticketPrice = findById(data.zoneId, data.ticketDuration, data.customerStatus);

        if (data.value != null) {
            ticketPrice.setValue(data.value);
        }


        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
        if (data.startDate != null && !data.startDate.equals("")) {
            try {
                LocalDate startDate = LocalDate.parse(data.startDate, formatter);
                if (!startDate.isEqual(ticketPrice.getValidityPeriod().getStartDate())) {
                    ticketPrice.getValidityPeriod().setStartDate(startDate);
                }
            } catch (DateTimeParseException e) {
                throw new DateTimeParseException("Date cannot be parsed ", data.startDate, 0);
            }
        }

        if (data.endDate != null && !data.endDate.equals("")) {
            try {
                LocalDate endDate = LocalDate.parse(data.endDate, formatter);
                if (!endDate.isEqual(ticketPrice.getValidityPeriod().getEndDate())) {
                    ticketPrice.getValidityPeriod().setEndDate(endDate);
                }
            } catch (DateTimeParseException e) {
                throw new DateTimeParseException("Date cannot be parsed ", data.endDate, 0);
            }
        }

        return ticketPriceRepository.save(ticketPrice);
    }

    public TicketPrice deleteById(Long zoneId, String duration, String customer)
        throws ZoneNotFound
    {
        TicketPrice ticketPrice = findById(zoneId, duration, customer);
        ticketPrice.setActive(false);
        return ticketPriceRepository.save(ticketPrice);
    }
}

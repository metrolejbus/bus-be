package com.metrolejbus.app.ticketing.services;

import com.metrolejbus.app.authentication.models.User;
import com.metrolejbus.app.authentication.services.AuthService;
import com.metrolejbus.app.customers.models.Customer;
import com.metrolejbus.app.customers.services.CustomersService;
import com.metrolejbus.app.ticketing.forms.CreatePeriodicTicketForm;
import com.metrolejbus.app.ticketing.forms.UpdatePeriodicTicketForm;
import com.metrolejbus.app.ticketing.models.PeriodicTicketState;
import com.metrolejbus.app.ticketing.models.PeriodicTicket;
import com.metrolejbus.app.ticketing.models.TicketDuration;
import com.metrolejbus.app.ticketing.repositories.PeriodicTicketRepository;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.services.ZoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.management.InvalidAttributeValueException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;

@Service
public class PeriodicTicketService {

    private PeriodicTicketRepository periodicTicketRepository;
    private ZoneService zoneService;
    private AuthService authService;
    private CustomersService customersService;

    public PeriodicTicketService(
            @Autowired PeriodicTicketRepository periodicTicketRepository,
            @Autowired ZoneService zoneService,
            @Autowired AuthService authService,
            @Autowired CustomersService customersService
    ) {
        this.periodicTicketRepository = periodicTicketRepository;
        this.zoneService = zoneService;
        this.authService = authService;
        this.customersService = customersService;
    }

    public List<PeriodicTicket> findAll() { return periodicTicketRepository.findAll(); }

    public PeriodicTicket findById(long id) {
        return periodicTicketRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    public PeriodicTicket save(PeriodicTicket periodicTicket) {
        return periodicTicketRepository.save(periodicTicket);
    }

    public List<PeriodicTicket> findAllByCustomerId(long id) {
        return periodicTicketRepository.findAllByCustomerUserIdEquals(id);
    }

    public List<PeriodicTicket> findAllByState(PeriodicTicketState state) {
        return periodicTicketRepository.findAllByState(state);
    }

    public PeriodicTicket create(CreatePeriodicTicketForm data)
        throws InvalidAttributeValueException, ZoneNotFound
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
        try {
            LocalDate validFrom = LocalDate.parse(data.validFrom, formatter);
            Zone zone = zoneService.get(data.zoneId);

            try {
                PeriodicTicket ticket = new PeriodicTicket(zone, TicketDuration.valueOf(data.duration.toUpperCase()), validFrom);
                ticket.setActive(true);

                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                String currentUserName = authentication.getName();
                User user = authService.findByUsername(currentUserName);
                Customer customer = customersService.findById(user.getId());
                ticket.setCustomer(customer);

                return periodicTicketRepository.save(ticket);
            } catch (IllegalArgumentException e) {
                throw new IllegalArgumentException();
            }
        } catch (DateTimeParseException e) {
            throw new DateTimeParseException("Date cannot be parsed ", data.validFrom, 0);
        }
    }

    public PeriodicTicket update(UpdatePeriodicTicketForm data)
        throws InvalidAttributeValueException, ZoneNotFound
    {
        PeriodicTicket ticket = findById(data.id);
        if (data.zoneId != null){
            Zone zone = zoneService.get(data.zoneId);
            ticket.setZone(zone);
        }

        if(data.validFrom != null && !data.validFrom.equals("")){
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy.");
            try {
                LocalDate validFrom = LocalDate.parse(data.validFrom, formatter);
                ticket.setValidFrom(validFrom);
            } catch (DateTimeParseException e) {
                throw new DateTimeParseException("Date cannot be parsed ", data.validFrom, 0);
            }
        }

        if (data.duration != null && !data.duration.equals("")){
            try {
                ticket.setDuration(TicketDuration.valueOf(data.duration.toUpperCase()));
            } catch (IllegalArgumentException e) {
                throw new IllegalArgumentException();
            }
        }

        return periodicTicketRepository.save(ticket);
    }

    public PeriodicTicket deleteById(long id) {
        PeriodicTicket ticket = findById(id);
        ticket.setActive(false);
        return periodicTicketRepository.save(ticket);
    }

}

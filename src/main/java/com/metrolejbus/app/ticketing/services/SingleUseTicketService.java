package com.metrolejbus.app.ticketing.services;

import com.metrolejbus.app.authentication.models.User;
import com.metrolejbus.app.authentication.services.AuthService;
import com.metrolejbus.app.customers.models.Customer;
import com.metrolejbus.app.customers.services.CustomersService;
import com.metrolejbus.app.ticketing.forms.CreateSingleUseTicketForm;
import com.metrolejbus.app.ticketing.forms.UpdateSingleUseTicketForm;
import com.metrolejbus.app.transportation.exceptions.ZoneNotFound;
import com.metrolejbus.app.transportation.models.Zone;
import com.metrolejbus.app.transportation.services.ZoneService;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import com.metrolejbus.app.ticketing.repositories.SingleUseTicketRepository;
import com.metrolejbus.app.ticketing.models.SingleUseTicket;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Service
public class SingleUseTicketService {

    private SingleUseTicketRepository singleUseTicketRepository;
    private ZoneService zoneService;
    private AuthService authService;
    private CustomersService customersService;

    public SingleUseTicketService(
            @Autowired SingleUseTicketRepository singleUseTicketRepository,
            @Autowired ZoneService zoneService,
            @Autowired AuthService authService,
            @Autowired CustomersService customersService
    ) {
        this.singleUseTicketRepository = singleUseTicketRepository;
        this.zoneService = zoneService;
        this.authService = authService;
        this.customersService = customersService;
    }

    public List<SingleUseTicket> findAll() {
        return singleUseTicketRepository.findAll();
    }

    public SingleUseTicket findById(long id){
        return singleUseTicketRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
    }

    public List<SingleUseTicket> findAllByCustomerId(long id) {
        return singleUseTicketRepository.findAllByCustomerUserIdEquals(id);
    }

    public SingleUseTicket create(CreateSingleUseTicketForm data)
        throws ZoneNotFound
    {
        Zone zone = zoneService.get(data.zoneId);

        SingleUseTicket ticket = new SingleUseTicket(zone);
        ticket.setActive(true);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentUserName = authentication.getName();
        User user = authService.findByUsername(currentUserName);
        Customer customer = customersService.findById(user.getId());
        ticket.setCustomer(customer);

        return singleUseTicketRepository.save(ticket);
    }

    public SingleUseTicket update(UpdateSingleUseTicketForm data)
        throws ZoneNotFound
    {
        SingleUseTicket ticket = findById(data.id);

        if (data.zoneId != null) {
            Zone zone = zoneService.get(data.zoneId);
            ticket.setZone(zone);
        }

        return singleUseTicketRepository.save(ticket);
    }

    public SingleUseTicket deleteById(long id) {
        SingleUseTicket ticket = findById(id);
        ticket.setActive(false);
        return singleUseTicketRepository.save(ticket);
    }

}

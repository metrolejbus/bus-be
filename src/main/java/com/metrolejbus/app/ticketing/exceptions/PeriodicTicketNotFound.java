package com.metrolejbus.app.ticketing.exceptions;

import com.metrolejbus.app.common.exceptions.NotFound;

public class PeriodicTicketNotFound extends NotFound {

    public PeriodicTicketNotFound(Long id){
        super("Periodic ticket [" + id + "] doesn't exist.");
    }
}

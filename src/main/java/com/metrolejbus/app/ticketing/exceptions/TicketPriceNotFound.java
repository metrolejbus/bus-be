package com.metrolejbus.app.ticketing.exceptions;

import com.metrolejbus.app.common.exceptions.NotFound;

public class TicketPriceNotFound extends NotFound {

    public TicketPriceNotFound(Long zoneId, String duration, String customer){
        super("Ticket price [zone " + zoneId + "," + duration + "," + customer + "] doesn't exist.");
    }
}

package com.metrolejbus.app.ticketing.exceptions;

import com.metrolejbus.app.common.exceptions.NotFound;

public class DailyTicketNotFound extends NotFound {

    public DailyTicketNotFound(Long id){
        super("Daily ticket [" + id + "] doesn't exist.");
    }
}

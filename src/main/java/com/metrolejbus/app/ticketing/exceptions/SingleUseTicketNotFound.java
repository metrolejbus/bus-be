package com.metrolejbus.app.ticketing.exceptions;

import com.metrolejbus.app.common.exceptions.NotFound;

public class SingleUseTicketNotFound extends NotFound {

    public SingleUseTicketNotFound(Long id){
        super("Single use ticket [" + id + "] doesn't exist.");
    }
}

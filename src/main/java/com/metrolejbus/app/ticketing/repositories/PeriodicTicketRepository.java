package com.metrolejbus.app.ticketing.repositories;

import com.metrolejbus.app.ticketing.models.PeriodicTicket;
import com.metrolejbus.app.ticketing.models.PeriodicTicketState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PeriodicTicketRepository extends JpaRepository<PeriodicTicket, Long> {
    List<PeriodicTicket> findAllByCustomerUserIdEquals(long customerId);
    List <PeriodicTicket> findAllByState(PeriodicTicketState state);
}
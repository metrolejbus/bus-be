package com.metrolejbus.app.ticketing.repositories;

import com.metrolejbus.app.ticketing.models.TicketDuration;
import com.metrolejbus.app.ticketing.models.TicketPrice;
import com.metrolejbus.app.transportation.models.Zone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketPriceRepository extends JpaRepository<TicketPrice, Integer> {

    TicketPrice findTicketPriceByIdEquals(TicketPrice.TicketPriceKey id);
    List<TicketPrice> findAllById_ZoneAndId_TicketDuration(Zone zone, TicketDuration duration);
    List<TicketPrice> findAllById_TicketDuration(TicketDuration duration);
}

package com.metrolejbus.app.ticketing.repositories;

import com.metrolejbus.app.ticketing.models.SingleUseTicket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SingleUseTicketRepository extends JpaRepository<SingleUseTicket, Long> {
    List<SingleUseTicket> findAllByCustomerUserIdEquals(long customerId);
}
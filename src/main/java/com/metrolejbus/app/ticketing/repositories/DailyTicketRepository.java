package com.metrolejbus.app.ticketing.repositories;

import com.metrolejbus.app.ticketing.models.DailyTicket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DailyTicketRepository extends JpaRepository<DailyTicket, Long> {
    List<DailyTicket> findAllByCustomerUserIdEquals(long customerId);
}
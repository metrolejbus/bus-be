package com.metrolejbus.app.storage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/storage")
public class StorageController {

    @Autowired
    StorageService storageService;

    @GetMapping(path = "/{name}", produces = "application/octet-stream")
    public ResponseEntity<Object> getFiles(@PathVariable String name) {
        return ResponseEntity.ok(storageService.loadAsResource(name));
    }
}

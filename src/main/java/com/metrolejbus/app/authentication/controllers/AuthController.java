package com.metrolejbus.app.authentication.controllers;

import com.metrolejbus.app.authentication.exceptions.UsernameNotAvailable;
import com.metrolejbus.app.authentication.forms.SignInForm;
import com.metrolejbus.app.authentication.models.User;
import com.metrolejbus.app.authentication.services.AuthService;
import com.metrolejbus.app.authentication.forms.SignUpForm;
import com.metrolejbus.app.authentication.models.UserWithToken;
import com.metrolejbus.app.authentication.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.management.InvalidAttributeValueException;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthService service;

    @PostMapping("/sign-in")
    public UserWithToken signIn(@Valid @RequestBody SignInForm data) {
        return this.service.signIn(data.username, data.password);
    }

    @PostMapping("/sign-up")
    public UserWithToken signUp(@Valid @RequestBody SignUpForm data)
            throws UsernameNotAvailable, InvalidAttributeValueException {
        return service.signUp(
                data.username,
                data.password,
                data.email,
                data.firstName,
                data.lastName,
                data.middleName,
                data.age,
                data.status
        );
    }
}

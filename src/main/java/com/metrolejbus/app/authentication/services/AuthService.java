package com.metrolejbus.app.authentication.services;

import com.metrolejbus.app.authentication.JwtTokenProvider;
import com.metrolejbus.app.authentication.exceptions.UsernameNotAvailable;
import com.metrolejbus.app.authentication.models.Role;
import com.metrolejbus.app.authentication.models.User;
import com.metrolejbus.app.authentication.models.UserWithToken;
import com.metrolejbus.app.authentication.repositories.UserRepository;
import com.metrolejbus.app.customers.models.Customer;
import com.metrolejbus.app.customers.models.CustomerStatus;
import com.metrolejbus.app.customers.repositories.CustomersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.management.InvalidAttributeValueException;

@Service
public class AuthService {

    @Autowired
    private UserRepository usersRepository;
    @Autowired
    private CustomersRepository customersRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private JwtTokenProvider tokenProvider;
    @Autowired
    private AuthenticationManager authenticationManager;

    public User createUser(String username, String password, String email)
            throws UsernameNotAvailable {
        if (this.usersRepository.existsByUsername(username)) {
            throw new UsernameNotAvailable(username);
        }

        User user = new User();
        user.setUsername(username);
        user.setEmail(email);
        user.setPassword(this.passwordEncoder.encode(password));
        user.setRole(Role.CUSTOMER);

        return user;
    }

    public UserWithToken signUp(
            String username,
            String password,
            String email,
            String firstName,
            String lastName,
            String middleName,
            int age,
            CustomerStatus status
    )
            throws UsernameNotAvailable, InvalidAttributeValueException {
        User user = this.createUser(username, password, email);
        this.createCustomer(
                user,
                firstName,
                lastName,
                middleName,
                age,
                status
        );
        return this.signIn(username, password);
    }

    public Customer createCustomer(
            User user,
            String firstName,
            String lastName,
            String middleName,
            int age,
            CustomerStatus status
    ) throws InvalidAttributeValueException {
        Customer customer = new Customer(
                user,
                firstName,
                middleName,
                lastName,
                age,
                status);
        return customersRepository.save(customer);
    }

    public UserWithToken signIn(String username, String password)
            throws AuthenticationException {
        UsernamePasswordAuthenticationToken token =
            new UsernamePasswordAuthenticationToken(
                username, password
            );
        this.authenticationManager.authenticate(token);

        User user = this.usersRepository.findByUsername(username);
        String jwtToken = this.generateToken(user);

        return new UserWithToken(user, jwtToken);
    }

    public User findByUsername(String username) {
        return this.usersRepository.findByUsername(username);
    }

    private String generateToken(User user) {
        return this.tokenProvider.createToken(user);
    }

}

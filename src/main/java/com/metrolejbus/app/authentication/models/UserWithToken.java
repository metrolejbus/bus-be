package com.metrolejbus.app.authentication.models;

public class UserWithToken {
    public User user;
    public String token;

    public UserWithToken() {}

    public UserWithToken(User user, String token) {
        this.user = user;
        this.token = token;
    }
}

package com.metrolejbus.app.authentication.models;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    CUSTOMER, OPERATOR, ADMIN, SYSTEM;

    public String getAuthority() {
        return name();
    }
}

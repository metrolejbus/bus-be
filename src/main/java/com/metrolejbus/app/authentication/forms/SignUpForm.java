package com.metrolejbus.app.authentication.forms;

import com.metrolejbus.app.customers.models.CustomerStatus;
import org.springframework.stereotype.Component;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Component
public class SignUpForm {
    @NotBlank
    @Size(min=5)
    public String username;

    @NotBlank
    @Size(min=5)
    public String password;

    @NotBlank
    @Email
    public String email;

    @NotBlank
    @Size(min=1)
    public String firstName;

    @NotBlank
    @Size(min=1)
    public String middleName;

    @NotBlank
    @Size(min=1)
    public String lastName;

    @Positive
    public Integer age;

    public CustomerStatus status;
}

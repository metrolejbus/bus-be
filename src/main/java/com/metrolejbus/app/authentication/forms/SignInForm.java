package com.metrolejbus.app.authentication.forms;

import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Component
public class SignInForm {
    @NotBlank
    @Size(min=5)
    public String username;

    @NotBlank
    @Size(min=5)
    public String password;
}

package com.metrolejbus.app.authentication;

import com.metrolejbus.app.authentication.models.User;
import com.metrolejbus.app.authentication.services.CustomDetailsService;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.Collection;
import java.util.Date;

@Component
public class JwtTokenProvider {

    //TODO: use public/private key pair
    @Value("${security.jwt.token.secret-key}")
    private String secretKey;

    @Value("${security.jwt.token.expire-length:3600000}")
    private long validityInMilliseconds = 3600000;

    @Autowired
    private CustomDetailsService detailsService;

    @PostConstruct
    protected void init() {
        this.secretKey =
            Base64.getEncoder().encodeToString(this.secretKey.getBytes());
    }

    public String createToken(User user) {
        Claims claims = Jwts.claims();
        claims.setSubject(user.getUsername());
        claims.put("role", user.getRole());

        Date now = new Date();
        claims.setIssuedAt(now);
        claims.setExpiration(
            new Date(now.getTime() + this.validityInMilliseconds)
        );

        return Jwts
                .builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact();
    }

    public boolean isValid(String token) {
        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(this.secretKey).parseClaimsJws(token);

            if (claims.getBody().getExpiration().before(new Date())) {
                // Token expired
                return false;
            }

            return true;
        } catch (JwtException | IllegalArgumentException e) {
            return false;
        }
    }

    public String resolve(HttpServletRequest req) {
        String bearerToken = req.getHeader("Authorization");
        if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
        return null;
    }

    public Authentication getAuthentication(String token) {
        String username = this.getUsername(token);
        Collection roles = detailsService.loadUserByUsername(username).getAuthorities();
        return new UsernamePasswordAuthenticationToken(username, "", roles);
    }

    public String getUsername(String token) {
        return Jwts
                .parser()
                .setSigningKey(this.secretKey)
                .parseClaimsJws(token).getBody()
                .getSubject();
    }
}

package com.metrolejbus.app.authentication.exceptions;

import com.metrolejbus.app.common.exceptions.BadRequest;

public class UsernameNotAvailable extends BadRequest {
    public UsernameNotAvailable(String username) {
        super("Username \"" + username + "\" is not available");
    }
}

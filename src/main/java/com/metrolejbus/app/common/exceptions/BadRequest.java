package com.metrolejbus.app.common.exceptions;

import org.springframework.http.HttpStatus;

public class BadRequest extends HttpException {

    public BadRequest(String message) {
        super(message);
    }

    @Override
    public HttpStatus getStatus() {
        return HttpStatus.BAD_REQUEST;
    }
}

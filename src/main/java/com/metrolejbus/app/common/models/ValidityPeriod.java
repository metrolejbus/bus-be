package com.metrolejbus.app.common.models;

import javax.management.InvalidAttributeValueException;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.LocalDate;
import java.util.Objects;

@Embeddable
public class ValidityPeriod {
    @Column(name = "valid_from")
    private LocalDate startDate;
    @Column(name = "valid_to")
    private LocalDate endDate;

    public ValidityPeriod() {
    }

    public ValidityPeriod(LocalDate startDate, LocalDate endDate)
            throws InvalidAttributeValueException {
        if (startDate == null) {
            throw new NullPointerException("Start date cant't be null");
        }

        if (startDate.isBefore(LocalDate.now())) {
            throw new InvalidAttributeValueException(
                    "Start date for validity period can't be in the past"
            );
        }

        if (endDate != null && endDate.isBefore(startDate)) {
            throw new InvalidAttributeValueException(
                    "End date for validity period can't be before start date"
            );
        }

        this.startDate = startDate;
        this.endDate = endDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate date)
            throws InvalidAttributeValueException {
        if (date == null) {
            throw new NullPointerException("Start date cant't be null");
        }

        if (date.isBefore(LocalDate.now())) {
            throw new InvalidAttributeValueException(
                    "Start date for validity period can't be in the past"
            );
        }
        if(endDate == null) {
            this.startDate = date;
            return;
        }
        if (endDate.isBefore(date)) {
            throw new InvalidAttributeValueException(
                    "End date for validity period can't be before start date"
            );
        }

        this.startDate = date;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate date)
            throws InvalidAttributeValueException
    {
        if (date != null && date.isBefore(this.startDate)) {
            throw new InvalidAttributeValueException(
                    "End date for validity period can't be before start date"
            );
        }
        this.endDate = date;
    }

    public boolean isValid() {
        LocalDate now = LocalDate.now();
        if (this.endDate == null) {
            return now.isAfter(this.startDate);
        } else if (now.isEqual(this.startDate) || now.isEqual(this.endDate)) {
            return true;
        }
        return now.isAfter(this.startDate) && now.isBefore(this.endDate);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValidityPeriod that = (ValidityPeriod) o;
        return Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startDate, endDate);
    }

    @Override
    public String toString() {
        return "ValidityPeriod{" +
                "startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }

}

package com.metrolejbus.app.common.services;


import io.socket.client.IO;
import io.socket.client.Socket;

import java.net.URISyntaxException;

public class Sockets {

    private static Socket socket;

    private static void instantiateSocket() {
        try {
            socket = IO.socket("http://localhost:9292");
            socket.on(Socket.EVENT_DISCONNECT, args -> {
                // this is not tested !
                socket = null;
                // should reconnect, but can be done through config
            });
            socket.connect();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public static Socket getSocket() {
        if (socket == null) {
            instantiateSocket();
        }
        return socket;
    }
}

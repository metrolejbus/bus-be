const server = require('http').createServer();
const io = require('socket.io')(server);

clients = [];

io.on('connection', client => {
    console.log('CONNECTED');

    client.on('event', data => {
        console.log('EVENT RECEIVED');
        console.log(data);
        // data should contain channel (room)
        // here should emit to that channel
        if (!data.name) {
            return;
        }

        io.emit(data.name, data.data);
    });

    client.on('disconnect', () => {
        console.log('DISCONNECTED');
    });
});

server.listen(9292);
